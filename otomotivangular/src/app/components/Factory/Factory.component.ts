import { FactoryService } from './../../services/factory/factory.service';
import { FactoryFullOutPut } from './../../services/factory/dtos/FactoryFullOutPut';
import { Component, OnInit, Injector, Inject, ChangeDetectionStrategy } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { ModalManagerService } from '@app/services/common/modal-manager.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { DeleteFactoryInput } from '@app/services/factory/dtos/DeleteFactoryInput';
import { finalize } from 'rxjs/operators';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'app-factory',
  templateUrl: './factory.component.html',
  animations: [appModuleAnimation()],
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./factory.component.css']
})
export class FactoryComponent extends AppComponentBase implements OnInit {
  factories: FactoryFullOutPut[] = [];
  dataSource: any = {};

  constructor(injector: Injector,
    private _factoryService: FactoryService,
    private _modelManagerService: ModalManagerService,
    @Inject(HttpClient) httpClient: HttpClient
    ) {
    super(injector);
  }

  ngOnInit(): void {
    // this.dataSource = this.createDxDataSource();
  }

  createFactory(): void {
    this._modelManagerService.openCreateFactoryDialog().afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  editFactory(id: number): void {
    this._modelManagerService.openEditFactoryDialog(id).afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  deleteFactory(factory: DeleteFactoryInput): void {
    abp.message.confirm(
      this.l('FactoryDeleteWarningMessage', factory.name),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._factoryService
            .delete(factory)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => { });
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      // {
      //   location: 'before',
      //   template: 'formNameTemplate'
      // },
      {
        location: 'after',
        template: 'refreshButtonTemplate'
      });
  }

  // dxGrid Changed
  onOptionChanged(e){
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }
  
  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        load: (loadOptions: any) => {
          function isNotEmpty(value: any): boolean {
            return value !== undefined && value !== null && value !== "";
        }
          let params: HttpParams = new HttpParams();
          [
              "skip",
              "take",
              "requireTotalCount",
              "requireGroupCount",
              "sort",
              "filter",
              "totalSummary",
              "group",
              "groupSummary"
          ].forEach(function(i) {
              if (i in loadOptions && isNotEmpty(loadOptions[i]))
                  params = params.set(i, JSON.stringify(loadOptions[i]));
          });
          // console.log("params:",params);
          // console.log("loadOptions:",loadOptions);
          return new Promise((resolve, reject) => {
            this._factoryService
              .getList(params)
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: any[]) => {
                // this.factories = result.data;
                console.log("Returned Data: ", result);
                resolve(result);
              });
          });
        }
      })
    });
  }

}