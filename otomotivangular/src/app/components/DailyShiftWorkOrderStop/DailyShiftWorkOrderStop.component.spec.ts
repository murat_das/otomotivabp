/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DailyShiftWorkOrderStopComponent } from './dailyShiftWorkOrderStop.component';

describe('DailyShiftWorkOrderStopComponent', () => {
  let component: DailyShiftWorkOrderStopComponent;
  let fixture: ComponentFixture<DailyShiftWorkOrderStopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyShiftWorkOrderStopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyShiftWorkOrderStopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
