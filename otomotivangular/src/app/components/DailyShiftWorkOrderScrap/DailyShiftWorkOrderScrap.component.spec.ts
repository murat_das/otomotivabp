/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DailyShiftWorkOrderScrapComponent } from './dailyShiftWorkOrderScrap.component';

describe('DailyShiftWorkOrderScrapComponent', () => {
  let component: DailyShiftWorkOrderScrapComponent;
  let fixture: ComponentFixture<DailyShiftWorkOrderScrapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyShiftWorkOrderScrapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyShiftWorkOrderScrapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
