/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MoldComponent } from './mold.component';

describe('MoldComponent', () => {
  let component: MoldComponent;
  let fixture: ComponentFixture<MoldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
