import { NgModule } from '@angular/core';
import { DxButtonModule, DxDataGridModule, DxTooltipModule, DxSelectBoxModule, DxDropDownBoxModule, DxTextBoxModule } from 'devextreme-angular';

@NgModule({
    imports: [
        DxButtonModule,
        DxDataGridModule,
        DxTooltipModule
    ],
    exports: [
      DxButtonModule,
      DxDataGridModule,
      DxTooltipModule,
      DxDropDownBoxModule,
      DxTextBoxModule

    ]
})
export class DevextremeComponentModule { }
