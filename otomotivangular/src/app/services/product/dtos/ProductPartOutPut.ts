export class ProductPartOutPut implements IProductPartOutPut {
  id: number;
  name: string;
  barcode: string;
  stockCode: string;
}

export interface IProductPartOutPut {
  id: number;
  name: string;
  barcode: string;
  stockCode: string;
}
