export class UpdateProductInput implements IUpdateProductInput {
  id: number;
  name: string;
  barcode: string;
  stockCode: string;
}

export interface IUpdateProductInput {
  id: number;
  name: string;
  barcode: string;
  stockCode: string;
}
