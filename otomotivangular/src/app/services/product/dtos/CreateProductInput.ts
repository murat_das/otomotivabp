export class CreateProductInput implements ICreateProductInput {
  name: string;
  barcode: string;
  stockCode: string;
}

export interface ICreateProductInput {
  name: string;
  barcode: string;
  stockCode: string;
}
