
import { isObject } from "util";

export class ProductFullOutPut implements IProductFullOutPut {
  
    id:number;
    name: string;
    barcode: string;
    stockCode: string;

    constructor(data?: IProductFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.name = data.name;
                this.barcode = data.barcode;
                this.stockCode = data.stockCode;
            }
        }
    }

    static fromJS(data: any): ProductFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new ProductFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["barcode"] = this.barcode;
        data["stockCode"] = this.stockCode;
        return data; 
    }

    clone(): ProductFullOutPut {
        const json = this.toJSON();
        let result = new ProductFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IProductFullOutPut{
    id:number;
    name: string;
    barcode: string;
    stockCode: string;
}
