export class DeleteProductInput implements IDeleteProductInput {
  id: number;
  name: string;
}

export interface IDeleteProductInput {
  id: number;
  name: string;
}
