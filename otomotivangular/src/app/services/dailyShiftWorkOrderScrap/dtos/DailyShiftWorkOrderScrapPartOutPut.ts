import { DailyShiftWorkOrderPartOutPut } from "../../dailyShiftWorkOrder/dtos/DailyShiftWorkOrderPartOutPut";
import { ScrapPartOutPut } from "../../scrap/dtos/ScrapPartOutPut";

export class DailyShiftWorkOrderScrapPartOutPut implements IDailyShiftWorkOrderScrapPartOutPut {
    id:number;
    dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
    scrap: ScrapPartOutPut;
}


export interface IDailyShiftWorkOrderScrapPartOutPut{
    id:number;
    dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
    scrap: ScrapPartOutPut;
}