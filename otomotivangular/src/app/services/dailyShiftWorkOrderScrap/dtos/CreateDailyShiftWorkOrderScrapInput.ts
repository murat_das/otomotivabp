import { ScrapPartOutPut } from "./../../scrap/dtos/ScrapPartOutPut";
import { DailyShiftWorkOrderPartOutPut } from "./../../dailyShiftWorkOrder/dtos/DailyShiftWorkOrderPartOutPut";

export class CreateDailyShiftWorkOrderScrapInput
  implements ICreateDailyShiftWorkOrderScrapInput {
  dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
  scrap: ScrapPartOutPut;
}

export interface ICreateDailyShiftWorkOrderScrapInput {
  dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
  scrap: ScrapPartOutPut;
}
