
import { isObject } from "util";
import { DailyShiftWorkOrderPartOutPut } from "../../dailyShiftWorkOrder/dtos/DailyShiftWorkOrderPartOutPut";
import { ScrapPartOutPut } from "../../scrap/dtos/ScrapPartOutPut";

export class DailyShiftWorkOrderScrapFullOutPut implements IDailyShiftWorkOrderScrapFullOutPut {
  
    id:number;
    dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
    scrap: ScrapPartOutPut;

    constructor(data?: IDailyShiftWorkOrderScrapFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }
  

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.dailyShiftWorkOrder = data.dailyShiftWorkOrder;
                this.scrap = data.scrap;
            }
        }
    }

    static fromJS(data: any): DailyShiftWorkOrderScrapFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new DailyShiftWorkOrderScrapFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["dailyShiftWorkOrder"] = this.dailyShiftWorkOrder;
        data["scrap"] = this.scrap;
        return data; 
    }

    clone(): DailyShiftWorkOrderScrapFullOutPut {
        const json = this.toJSON();
        let result = new DailyShiftWorkOrderScrapFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IDailyShiftWorkOrderScrapFullOutPut{
    id:number;
    dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
    scrap: ScrapPartOutPut;
}
