import { DailyShiftWorkOrderPartOutPut } from "../../dailyShiftWorkOrder/dtos/DailyShiftWorkOrderPartOutPut";
import { ScrapPartOutPut } from "../../scrap/dtos/ScrapPartOutPut";

export class UpdateDailyShiftWorkOrderScrapInput
  implements IUpdateDailyShiftWorkOrderScrapInput {
  id: number;
  dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
  scrap: ScrapPartOutPut;
}

export interface IUpdateDailyShiftWorkOrderScrapInput {
  id: number;
  dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
  scrap: ScrapPartOutPut;
}
