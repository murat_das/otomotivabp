import { ScrapPartOutPut } from "../../scrap/dtos/ScrapPartOutPut";
import { DailyShiftWorkOrderPartOutPut } from "../../dailyShiftWorkOrder/dtos/DailyShiftWorkOrderPartOutPut";

export class DeleteDailyShiftWorkOrderScrapInput
  implements IDeleteDailyShiftWorkOrderScrapInput {
  id: number;
  dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
  scrap: ScrapPartOutPut;
}

export interface IDeleteDailyShiftWorkOrderScrapInput {
  id: number;
  dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
  scrap: ScrapPartOutPut;
}
