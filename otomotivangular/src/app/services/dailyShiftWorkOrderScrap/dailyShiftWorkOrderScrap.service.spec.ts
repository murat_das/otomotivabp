/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DailyShiftWorkOrderScrapService } from './dailyShiftWorkOrderScrap.service';

describe('Service: DailyShiftWorkOrderScrap', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DailyShiftWorkOrderScrapService]
    });
  });

  it('should ...', inject([DailyShiftWorkOrderScrapService], (service: DailyShiftWorkOrderScrapService) => {
    expect(service).toBeTruthy();
  }));
});
