export class DepartmentPartOutPut implements IDepartmentPartOutPut {
  id: number;
  name: string;
}

export interface IDepartmentPartOutPut {
  id: number;
  name: string;
}
