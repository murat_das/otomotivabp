import { FactoryPartOutPut } from './../../factory/dtos/FactoryPartOutPut';

export class CreateDepartmentInput
  implements ICreateDepartmentInput {
    name: string;
    factory: FactoryPartOutPut;
}

export interface ICreateDepartmentInput {
  name: string;
  factory: FactoryPartOutPut;
}
