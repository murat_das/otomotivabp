export class DeleteDepartmentInput implements IDeleteDepartmentInput {
  id: number;
  name: string;
}

export interface IDeleteDepartmentInput {
  id: number;
  name: string;
}
