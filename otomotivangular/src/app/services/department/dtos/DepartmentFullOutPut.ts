import { FactoryPartOutPut } from './../../factory/dtos/FactoryPartOutPut';

import { isObject } from "util";

export class DepartmentFullOutPut implements IDepartmentFullOutPut {
  
    id:number;
    name:string;
    factory:FactoryPartOutPut;

    constructor(data?: IDepartmentFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.name = data.name;
                this.factory = data.factory;
            }
        }
    }

    static fromJS(data: any): DepartmentFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new DepartmentFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["factory"] = this.factory;
        return data; 
    }

    clone(): DepartmentFullOutPut {
        const json = this.toJSON();
        let result = new DepartmentFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IDepartmentFullOutPut{
    id:number;
    name:string;
    factory:FactoryPartOutPut;
}
