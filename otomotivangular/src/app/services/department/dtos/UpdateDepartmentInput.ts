import { FactoryPartOutPut } from "./../../factory/dtos/FactoryPartOutPut";

export class UpdateDepartmentInput implements IUpdateDepartmentInput {
  id: number;
  name: string;
  factory: FactoryPartOutPut;
}

export interface IUpdateDepartmentInput {
  id: number;
  name: string;
  factory: FactoryPartOutPut;
}
