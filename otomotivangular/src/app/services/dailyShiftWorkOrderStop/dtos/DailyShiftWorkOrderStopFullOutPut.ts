
import { isObject } from "util";
import { DailyShiftWorkOrderPartOutPut } from "../../dailyShiftWorkOrder/dtos/DailyShiftWorkOrderPartOutPut";
import { StopPartOutPut } from "../../stop/dtos/StopPartOutPut";

export class DailyShiftWorkOrderStopFullOutPut implements IDailyShiftWorkOrderStopFullOutPut {
  
    id:number;
    dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
    stop: StopPartOutPut;

    constructor(data?: IDailyShiftWorkOrderStopFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }
  

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.dailyShiftWorkOrder = data.dailyShiftWorkOrder;
                this.stop = data.stop;
            }
        }
    }

    static fromJS(data: any): DailyShiftWorkOrderStopFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new DailyShiftWorkOrderStopFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["dailyShiftWorkOrder"] = this.dailyShiftWorkOrder;
        data["stop"] = this.stop;
        return data; 
    }

    clone(): DailyShiftWorkOrderStopFullOutPut {
        const json = this.toJSON();
        let result = new DailyShiftWorkOrderStopFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IDailyShiftWorkOrderStopFullOutPut{
    id:number;
    dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
    stop: StopPartOutPut;
}
