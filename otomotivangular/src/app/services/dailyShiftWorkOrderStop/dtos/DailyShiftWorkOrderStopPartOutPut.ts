import { DailyShiftWorkOrderPartOutPut } from "../../dailyShiftWorkOrder/dtos/DailyShiftWorkOrderPartOutPut";
import { StopPartOutPut } from "../../stop/dtos/StopPartOutPut";

export class DailyShiftWorkOrderStopPartOutPut implements IDailyShiftWorkOrderStopPartOutPut {
    id:number;
    dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
    stop: StopPartOutPut;
}


export interface IDailyShiftWorkOrderStopPartOutPut{
    id:number;
    dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
    stop: StopPartOutPut;
}