import { StopPartOutPut } from "../../stop/dtos/StopPartOutPut";
import { DailyShiftWorkOrderPartOutPut } from "../../dailyShiftWorkOrder/dtos/DailyShiftWorkOrderPartOutPut";

export class DeleteDailyShiftWorkOrderStopInput
  implements IDeleteDailyShiftWorkOrderStopInput {
  id: number;
  dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
  stop: StopPartOutPut;
}

export interface IDeleteDailyShiftWorkOrderStopInput {
  id: number;
  dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
  stop: StopPartOutPut;
}
