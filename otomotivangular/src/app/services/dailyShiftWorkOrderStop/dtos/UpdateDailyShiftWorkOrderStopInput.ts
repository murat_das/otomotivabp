import { DailyShiftWorkOrderPartOutPut } from "../../dailyShiftWorkOrder/dtos/DailyShiftWorkOrderPartOutPut";
import { StopPartOutPut } from "../../stop/dtos/StopPartOutPut";

export class UpdateDailyShiftWorkOrderStopInput
  implements IUpdateDailyShiftWorkOrderStopInput {
  id: number;
  dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
  stop: StopPartOutPut;
}

export interface IUpdateDailyShiftWorkOrderStopInput {
  id: number;
  dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
  stop: StopPartOutPut;
}
