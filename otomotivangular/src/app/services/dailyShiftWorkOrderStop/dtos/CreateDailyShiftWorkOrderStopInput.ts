import { StopPartOutPut } from "./../../stop/dtos/StopPartOutPut";
import { DailyShiftWorkOrderPartOutPut } from "./../../dailyShiftWorkOrder/dtos/DailyShiftWorkOrderPartOutPut";

export class CreateDailyShiftWorkOrderStopInput
  implements ICreateDailyShiftWorkOrderStopInput {
  dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
  stop: StopPartOutPut;
}

export interface ICreateDailyShiftWorkOrderStopInput {
  dailyShiftWorkOrder: DailyShiftWorkOrderPartOutPut;
  stop: StopPartOutPut;
}
