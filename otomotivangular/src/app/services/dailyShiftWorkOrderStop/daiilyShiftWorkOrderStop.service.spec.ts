/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DaiilyShiftWorkOrderStopService } from './daiilyShiftWorkOrderStop.service';

describe('Service: DaiilyShiftWorkOrderStop', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DaiilyShiftWorkOrderStopService]
    });
  });

  it('should ...', inject([DaiilyShiftWorkOrderStopService], (service: DaiilyShiftWorkOrderStopService) => {
    expect(service).toBeTruthy();
  }));
});
