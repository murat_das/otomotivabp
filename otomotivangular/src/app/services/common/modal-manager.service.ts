import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { GetFactoryInput } from '../../services/factory/dtos/GetFactoryInput';


@Injectable({
  providedIn: "root",
  
})
export class ModalManagerService {
  constructor(private _dialog: MatDialog) {}

  // Factories
  openCreateFactoryDialog(parameters?: any): any {
    let createFactoryDialog;
    // createFactoryDialog = this._dialog.open(CreateFactoryDialogComponent, {
    //   data: parameters
    // });

    return createFactoryDialog;
  }

  openEditFactoryDialog(id?: number): any {
    let editFactoryDialog;
    let getFactoryInput = new GetFactoryInput();
    getFactoryInput.id = id;

    // editFactoryDialog = this._dialog.open(EditFactoryDialogComponent, {
    //   data: getFactoryInput
    // });

    return editFactoryDialog;
  }

}
