import { ScrapPartOutPut } from "../../scrap/dtos/ScrapPartOutPut";
import { ProductPartOutPut } from "../../product/dtos/ProductPartOutPut";

export class DeleteProductScrapInput implements IDeleteProductScrapInput {
    id: number;
    product: ProductPartOutPut;
    scrap: ScrapPartOutPut;
  }
  
  export interface IDeleteProductScrapInput {
    id: number;
    product: ProductPartOutPut;
    scrap: ScrapPartOutPut;
  }
  