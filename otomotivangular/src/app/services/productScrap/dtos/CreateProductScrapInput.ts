import { ScrapPartOutPut } from "./../../scrap/dtos/ScrapPartOutPut";
import { ProductPartOutPut } from "./../../product/dtos/ProductPartOutPut";

export class CreateProductScrapInput implements ICreateProductScrapInput {
  product: ProductPartOutPut;
  scrap: ScrapPartOutPut;
}

export interface ICreateProductScrapInput {
  product: ProductPartOutPut;
  scrap: ScrapPartOutPut;
}
