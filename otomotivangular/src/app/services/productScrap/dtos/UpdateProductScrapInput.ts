import { ProductPartOutPut } from "../../product/dtos/ProductPartOutPut";
import { ScrapPartOutPut } from "../../scrap/dtos/ScrapPartOutPut";

export class UpdateProductScrapInput implements IUpdateProductScrapInput {
  id: number;
  product: ProductPartOutPut;
  scrap: ScrapPartOutPut;
}

export interface IUpdateProductScrapInput {
  id: number;
  product: ProductPartOutPut;
  scrap: ScrapPartOutPut;
}
