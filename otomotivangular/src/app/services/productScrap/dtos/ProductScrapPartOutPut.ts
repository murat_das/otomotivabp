import { ProductPartOutPut } from "../../product/dtos/ProductPartOutPut";
import { ScrapPartOutPut } from "../../scrap/dtos/ScrapPartOutPut";

export class ProductScrapPartOutPut implements IProductScrapPartOutPut {
  id: number;
  product: ProductPartOutPut;
  scrap: ScrapPartOutPut;
}

export interface IProductScrapPartOutPut {
  id: number;
  product: ProductPartOutPut;
  scrap: ScrapPartOutPut;
}
