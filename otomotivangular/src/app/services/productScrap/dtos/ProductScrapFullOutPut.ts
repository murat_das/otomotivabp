import { isObject } from "util";
import { ProductPartOutPut } from "../../product/dtos/ProductPartOutPut";
import { ScrapPartOutPut } from "../../scrap/dtos/ScrapPartOutPut";

export class ProductScrapFullOutPut implements IProductScrapFullOutPut {
  id: number;
  product: ProductPartOutPut;
  scrap: ScrapPartOutPut;

  constructor(data?: IProductScrapFullOutPut) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (isObject(data)) {
        this.id = data.id;
        this.product = data.product;
        this.scrap = data.scrap;
      }
    }
  }

  static fromJS(data: any): ProductScrapFullOutPut {
    data = typeof data === "object" ? data : {};
    let result = new ProductScrapFullOutPut();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["product"] = this.product;
    data["name"] = this.scrap;
    return data;
  }

  clone(): ProductScrapFullOutPut {
    const json = this.toJSON();
    let result = new ProductScrapFullOutPut();
    result.init(json);
    return result;
  }
}

export interface IProductScrapFullOutPut {
  id: number;
  product: ProductPartOutPut;
  scrap: ScrapPartOutPut;
}
