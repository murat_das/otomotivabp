export class ShiftPartOutPut implements IShiftPartOutPut {
    id: number;
    name: string;
    startTime:Date;
    endTime:Date;
  }
  
  export interface IShiftPartOutPut {
    id: number;
    name: string;
    startTime:Date;
    endTime:Date;
  }
  