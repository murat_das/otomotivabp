export class DeleteShiftInput implements IDeleteShiftInput {
    id: number;
    name: string;
    startTime:Date;
    endTime:Date;
  }
  
  export interface IDeleteShiftInput {
    id: number;
    name: string;
    startTime:Date;
    endTime:Date;
  }
  