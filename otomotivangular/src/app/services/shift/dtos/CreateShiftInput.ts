export class CreateShiftInput implements ICreateShiftInput {
    name: string;
    startTime:Date;
    endTime:Date;
  }
  
  export interface ICreateShiftInput {
    name: string;
    startTime:Date;
    endTime:Date;
  }
  