import { isObject } from "util";

export class ShiftFullOutPut implements IShiftFullOutPut {
  
    id:number;
    name: string;
    startTime:Date;
    endTime:Date;

    constructor(data?: IShiftFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.name = data.name;
                this.startTime = data.startTime;
                this.endTime = data.endTime;
            }
        }
    }

    static fromJS(data: any): ShiftFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new ShiftFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["startTime"] = this.startTime;
        data["endTime"] = this.endTime;
        return data; 
    }

    clone(): ShiftFullOutPut {
        const json = this.toJSON();
        let result = new ShiftFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IShiftFullOutPut{
    id:number;
    name: string;
    startTime:Date;
    endTime:Date;
}
