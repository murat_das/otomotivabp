export class UpdateShiftInput implements IUpdateShiftInput {
    id: number;
    name: string;
    startTime:Date;
    endTime:Date;
  }
  
  export interface IUpdateShiftInput {
    id: number;
    name: string;
    startTime:Date;
    endTime:Date;
  }
  