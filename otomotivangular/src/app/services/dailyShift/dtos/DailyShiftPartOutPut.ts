import { DevicePartOutPut } from './../../device/dtos/DevicePartOutPut';
import { ShiftPartOutPut } from './../../shift/dtos/ShiftPartOutPut';
export class DailyShiftPartOutPut implements IDailyShiftPartOutPut{
    device:DevicePartOutPut;
    shift:ShiftPartOutPut;
}

export interface IDailyShiftPartOutPut {
    device:DevicePartOutPut;
    shift:ShiftPartOutPut;
}
