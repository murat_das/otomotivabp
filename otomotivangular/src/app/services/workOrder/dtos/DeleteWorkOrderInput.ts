export class DeleteWorkOrderInput implements IDeleteWorkOrderInput {
  id: number;
  no: string;
}

export interface IDeleteWorkOrderInput {
  id: number;
  no: string;
}
