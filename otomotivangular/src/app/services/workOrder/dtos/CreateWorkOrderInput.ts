import { MoldPartOutPut } from "./../../mold/dtos/MoldPartOutPut";
import { ProductPartOutPut } from "./../../product/dtos/ProductPartOutPut";
import { DevicePartOutPut } from "./../../device/dtos/DevicePartOutPut";

export class CreateWorkOrderInput implements ICreateWorkOrderInput {
  no: string;
  status: number;
  targetAmount: number;
  type: number;
  device: DevicePartOutPut;
  product: ProductPartOutPut;
  mold: MoldPartOutPut;
}

export interface ICreateWorkOrderInput {
  no: string;
  status: number;
  targetAmount: number;
  type: number;
  device: DevicePartOutPut;
  product: ProductPartOutPut;
  mold: MoldPartOutPut;
}
