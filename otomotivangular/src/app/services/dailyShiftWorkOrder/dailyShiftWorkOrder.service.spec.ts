/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DailyShiftWorkOrderService } from './dailyShiftWorkOrder.service';

describe('Service: DailyShiftWorkOrder', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DailyShiftWorkOrderService]
    });
  });

  it('should ...', inject([DailyShiftWorkOrderService], (service: DailyShiftWorkOrderService) => {
    expect(service).toBeTruthy();
  }));
});
