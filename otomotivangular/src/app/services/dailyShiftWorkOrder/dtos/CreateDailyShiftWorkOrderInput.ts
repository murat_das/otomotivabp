import { WorkOrderPartOutPut } from "./../../workOrder/dtos/WorkOrderPartOutPut";
import { DailyShiftPartOutPut } from "../../dailyShift/dtos/DailyShiftPartOutPut";

export class CreateDailyShiftWorkOrderInput
  implements ICreateDailyShiftWorkOrderInput {
    amount: number;
    startDate: Date;
    endDate: Date;
    dailyShift: DailyShiftPartOutPut;
    workOrder: WorkOrderPartOutPut;
}

export interface ICreateDailyShiftWorkOrderInput {
  amount: number;
  startDate: Date;
  endDate: Date;
  dailyShift: DailyShiftPartOutPut;
  workOrder: WorkOrderPartOutPut;
}
