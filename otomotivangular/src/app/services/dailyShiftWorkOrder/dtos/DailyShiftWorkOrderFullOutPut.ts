import { isObject } from "util";
import { DailyShiftPartOutPut } from "../../dailyShift/dtos/DailyShiftPartOutPut";
import { WorkOrderPartOutPut } from "../../workOrder/dtos/WorkOrderPartOutPut";

export class DailyShiftWorkOrderFullOutPut implements IDailyShiftWorkOrderFullOutPut {
  
    id:number;
    amount: number;
    startDate: Date;
    endDate: Date;
    dailyShift: DailyShiftPartOutPut;
    workOrder: WorkOrderPartOutPut;

    constructor(data?: IDailyShiftWorkOrderFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }
  

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.workOrder = data.workOrder;
                this.dailyShift = data.dailyShift;
                this.amount = data.amount;
                this.startDate = data.startDate;
                this.endDate = data.endDate;
            }
        }
    }

    static fromJS(data: any): DailyShiftWorkOrderFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new DailyShiftWorkOrderFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["dailyShift"] = this.dailyShift;
        data["workOrder"] = this.workOrder;
        data["amount"] = this.amount;
        data["startDate"] = this.startDate;
        data["endDate"] = this.endDate;
        return data; 
    }

    clone(): DailyShiftWorkOrderFullOutPut {
        const json = this.toJSON();
        let result = new DailyShiftWorkOrderFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IDailyShiftWorkOrderFullOutPut{
    id:number;
    amount: number;
    startDate: Date;
    endDate: Date;
    dailyShift: DailyShiftPartOutPut;
    workOrder: WorkOrderPartOutPut;
}
