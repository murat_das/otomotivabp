import { DailyShiftPartOutPut } from "../../dailyShift/dtos/DailyShiftPartOutPut";
import { WorkOrderPartOutPut } from "../../workOrder/dtos/WorkOrderPartOutPut";

export class UpdateDailyShiftWorkOrderInput implements IUpdateDailyShiftWorkOrderInput {
    id: number;
    amount: number;
    startDate: Date;
    endDate: Date;
    dailyShift: DailyShiftPartOutPut;
    workOrder: WorkOrderPartOutPut;
}

export interface IUpdateDailyShiftWorkOrderInput {
    id:number;
    amount: number;
    startDate: Date;
    endDate: Date;
    dailyShift: DailyShiftPartOutPut;
    workOrder: WorkOrderPartOutPut;
}

