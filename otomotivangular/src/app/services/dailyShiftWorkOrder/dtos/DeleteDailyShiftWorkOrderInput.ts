import { WorkOrderPartOutPut } from "../../workOrder/dtos/WorkOrderPartOutPut";
import { DailyShiftPartOutPut } from "../../dailyShift/dtos/DailyShiftPartOutPut";

export class DeleteDailyShiftWorkOrderInput implements IDeleteDailyShiftWorkOrderInput {
    id:number;
    dailyShift:DailyShiftPartOutPut;
    workOrder:WorkOrderPartOutPut;
}

export interface IDeleteDailyShiftWorkOrderInput{
    id:number;
    dailyShift:DailyShiftPartOutPut;
    workOrder:WorkOrderPartOutPut;
}