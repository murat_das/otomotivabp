
export class DailyShiftWorkOrderPartOutPut implements IDailyShiftWorkOrderPartOutPut {
    id:number;
    amount: number;
    startDate: Date;
    endDate: Date;
}


export interface IDailyShiftWorkOrderPartOutPut{
    id:number;
    amount: number;
    startDate: Date;
    endDate: Date;
}