import { DailyShiftPartOutPut } from "../../dailyShift/dtos/DailyShiftPartOutPut";
import { UserPartOutPut } from "../../user/dtos/UserPartOutPut";

export class DailyShiftEmployeePartOutPut implements IDailyShiftEmployeePartOutPut {
    id:number;
    dailyShift:DailyShiftPartOutPut;
    user:UserPartOutPut;
}


export interface IDailyShiftEmployeePartOutPut{
    id:number;
    dailyShift:DailyShiftPartOutPut;
    user:UserPartOutPut;
}