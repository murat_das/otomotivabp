import { UserPartOutPut } from "../../user/dtos/UserPartOutPut";
import { DailyShiftPartOutPut } from "../../dailyShift/dtos/DailyShiftPartOutPut";


export class CreateDailyShiftEmployeeInput
  implements ICreateDailyShiftEmployeeInput {
  user: UserPartOutPut;
  dailyShift: DailyShiftPartOutPut;
}

export interface ICreateDailyShiftEmployeeInput {
  user: UserPartOutPut;
  dailyShift: DailyShiftPartOutPut;
}
