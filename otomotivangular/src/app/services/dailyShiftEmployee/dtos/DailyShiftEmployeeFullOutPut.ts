import { UserPartOutPut } from './../../user/dtos/UserPartOutPut';
import { DailyShiftPartOutPut } from './../../dailyShift/dtos/DailyShiftPartOutPut';
import { isObject } from "util";

export class DailyShiftEmployeeFullOutPut implements IDailyShiftEmployeeFullOutPut {
  
    id:number;
    dailyShift:DailyShiftPartOutPut;
    user:UserPartOutPut;

    constructor(data?: IDailyShiftEmployeeFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.dailyShift = data.dailyShift;
                this.user = data.user;
            }
        }
    }

    static fromJS(data: any): DailyShiftEmployeeFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new DailyShiftEmployeeFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["dailyShift"] = this.dailyShift;
        data["user"] = this.user;
        return data; 
    }

    clone(): DailyShiftEmployeeFullOutPut {
        const json = this.toJSON();
        let result = new DailyShiftEmployeeFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IDailyShiftEmployeeFullOutPut{
    id:number;
    dailyShift:DailyShiftPartOutPut;
    user:UserPartOutPut;
}
