import { UserPartOutPut } from "../../user/dtos/UserPartOutPut";
import { DailyShiftPartOutPut } from "../../dailyShift/dtos/DailyShiftPartOutPut";

export class UpdateDailyShiftEmployeeInput implements IUpdateDailyShiftEmployeeInput {
    id: number;
    user: UserPartOutPut;
    dailyShift: DailyShiftPartOutPut;
}

export interface IUpdateDailyShiftEmployeeInput {
    id:number;
    user: UserPartOutPut;
    dailyShift: DailyShiftPartOutPut;
}

