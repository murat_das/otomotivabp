export class GetDailyShiftEmployeeInput implements IGetDailyShiftEmployeeInput {
  id: number;
}

export interface IGetDailyShiftEmployeeInput {
  id: number;
}
