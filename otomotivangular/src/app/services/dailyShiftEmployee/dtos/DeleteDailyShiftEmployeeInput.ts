import { UserPartOutPut } from './../../user/dtos/UserPartOutPut';
import { DailyShiftPartOutPut } from "../../dailyShift/dtos/DailyShiftPartOutPut";

export class DeleteDailyShiftEmployeeInput implements IDeleteDailyShiftEmployeeInput {
    id:number;
    dailyShift:DailyShiftPartOutPut;
    user:UserPartOutPut;
}

export interface IDeleteDailyShiftEmployeeInput{
    id:number;
    dailyShift:DailyShiftPartOutPut;
    user:UserPartOutPut;
}
