/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DailyShiftEmployeeService } from './dailyShiftEmployee.service';

describe('Service: DailyShiftEmployee', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DailyShiftEmployeeService]
    });
  });

  it('should ...', inject([DailyShiftEmployeeService], (service: DailyShiftEmployeeService) => {
    expect(service).toBeTruthy();
  }));
});
