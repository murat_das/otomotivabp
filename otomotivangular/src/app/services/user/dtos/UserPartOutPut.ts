export class UserPartOutPut implements IUserPartOutPut{
    name: string;
    surName: string;
    userName: string;
    emailAddress: string;
}

export interface IUserPartOutPut {
    name:string;
    surName:string;
    userName:string;
    emailAddress:string;
}