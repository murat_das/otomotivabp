import { NgModule } from '@angular/core';
import { UtilsService } from './common/utils.service';
import { ModalManagerService } from './common/modal-manager.service';

@NgModule({
    providers: [
      UtilsService,
      ModalManagerService,
    ]
  })

export class OtomotivServiceModule {}
