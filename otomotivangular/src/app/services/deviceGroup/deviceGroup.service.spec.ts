/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DeviceGroupService } from './deviceGroup.service';

describe('Service: DeviceGroup', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeviceGroupService]
    });
  });

  it('should ...', inject([DeviceGroupService], (service: DeviceGroupService) => {
    expect(service).toBeTruthy();
  }));
});
