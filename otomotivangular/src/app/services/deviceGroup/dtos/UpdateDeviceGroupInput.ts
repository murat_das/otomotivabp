import { DepartmentPartOutPut } from "../../department/dtos/DepartmentPartOutPut";

export class UpdateDeviceGroupInput implements IUpdateDeviceGroupInput {
    name: string;
    ipAddress:string;
    department:DepartmentPartOutPut;
  }
  
  export interface IUpdateDeviceGroupInput {
    name: string;
    ipAddress:string;
    department:DepartmentPartOutPut;
  }
  