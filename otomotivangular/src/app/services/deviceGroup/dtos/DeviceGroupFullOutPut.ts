import { isObject } from "util";
import { DepartmentPartOutPut } from "../../department/dtos/DepartmentPartOutPut";

export class DeviceGroupFullOutPut implements IDeviceGroupFullOutPut {
  
    id:number;
    name: string;
    ipAddress:string;
    department:DepartmentPartOutPut;

    constructor(data?: IDeviceGroupFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.name = data.name;
                this.department = data.department;
                this.ipAddress = data.ipAddress;
            }
        }
    }

    static fromJS(data: any): DeviceGroupFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new DeviceGroupFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["department"] = this.department;
        data["ipAddress"] = this.ipAddress;
        return data; 
    }

    clone(): DeviceGroupFullOutPut {
        const json = this.toJSON();
        let result = new DeviceGroupFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IDeviceGroupFullOutPut{
    id:number;
    name: string;
    ipAddress:string;
    department:DepartmentPartOutPut;
}
