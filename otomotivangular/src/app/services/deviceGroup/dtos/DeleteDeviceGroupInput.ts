
export class DeleteDeviceGroupInput implements IDeleteDeviceGroupInput {
    id: number;
    name: string;
  }
  
  export interface IDeleteDeviceGroupInput {
    id: number;
    name: string;
  }
  