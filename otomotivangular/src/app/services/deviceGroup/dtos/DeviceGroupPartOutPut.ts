export class DeviceGroupPartOutPut implements IDeviceGroupPartOutPut {
    name: string;
    ipAddress:string;
  }
  
  export interface IDeviceGroupPartOutPut {
    name: string;
    ipAddress:string;
  }
  