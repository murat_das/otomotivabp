import { DepartmentPartOutPut } from './../../department/dtos/DepartmentPartOutPut';

export class CreateDeviceGroupInput implements ICreateDeviceGroupInput {
    name: string;
    ipAddress:string;
    department:DepartmentPartOutPut;
}

export interface ICreateDeviceGroupInput {
  name: string;
  ipAddress:string;
  department:DepartmentPartOutPut;
}
