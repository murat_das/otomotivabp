
export class DeleteFactoryInput implements IDeleteFactoryInput {
    id: number;
    name: string;
  }
  
  export interface IDeleteFactoryInput {
    id: number;
    name: string;
  }
  