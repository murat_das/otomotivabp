export class CreateFactoryInput implements ICreateFactoryInput {
  name: string;
}

export interface ICreateFactoryInput {
  name: string;
}
