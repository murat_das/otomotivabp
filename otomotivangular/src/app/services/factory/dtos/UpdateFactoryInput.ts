export class UpdateFactoryInput implements IUpdateFactoryInput {
    id: number;
    name: string;
  }
  
  export interface IUpdateFactoryInput {
    id: number;
    name: string;
  }
  