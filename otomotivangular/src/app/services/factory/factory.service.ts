import { UpdateFactoryInput } from './dtos/UpdateFactoryInput';
import { DeleteFactoryInput } from './dtos/DeleteFactoryInput';
import { GetFactoryInput } from './dtos/GetFactoryInput';
import { FactoryFullOutPut } from './dtos/FactoryFullOutPut';
import { CreateFactoryInput } from './dtos/CreateFactoryInput';
import { UtilsService } from './../common/utils.service';
import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponseBase, HttpResponse, HttpParams } from '@angular/common/http';
import { API_BASE_URL } from '../../../shared/service-proxies/service-proxies';
import { Observable, throwError as _observableThrow, of as _observableOf } from "rxjs";
import { mergeMap as _observableMergeMap, catchError as _observableCatch } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class FactoryService {
  private utilsService: UtilsService;
  private http: HttpClient;
  private baseUrl: string;
  protected jsonParseReviver:
      | ((key: string, value: any) => any)
      | undefined = undefined;
  constructor(@Inject(HttpClient) http: HttpClient, utilsService: UtilsService, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
      this.utilsService = utilsService;
      this.http = http;
      this.baseUrl = baseUrl ? baseUrl : "";
  }

  /**
    * @param body (required) 
    * @return Success
    */
   create(body: CreateFactoryInput | undefined): Observable<FactoryFullOutPut> {
      let url_ = this.baseUrl + "/api/services/app/Factory/Create";
      url_ = url_.replace(/[?&]$/, "");

      const content_ = JSON.stringify(body);

      let options_: any = {
          body: content_,
          observe: "response",
          responseType: "blob",
          headers: new HttpHeaders({
              "Content-Type": "application/json-patch+json",
              "Accept": "text/plain"
          })
      };

      return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
          return this.processCreate(response_);
      })).pipe(_observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
              try {
                  return this.processCreate(<any>response_);
              } catch (e) {
                  return <Observable<FactoryFullOutPut>><any>_observableThrow(e);
              }
          } else
              return <Observable<FactoryFullOutPut>><any>_observableThrow(response_);
      }));
  }

  protected processCreate(response: HttpResponseBase): Observable<FactoryFullOutPut> {
      const status = response.status;
      const responseBlob =
          response instanceof HttpResponse ? response.body :
              (<any>response).error instanceof Blob ? (<any>response).error : undefined;

      let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
      if (status === 200) {
          return this.utilsService.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
              let result200: any = null;
              let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
              result200 = FactoryFullOutPut.fromJS(resultData200);
              return _observableOf(result200);
          }));
      } else if (status !== 200 && status !== 204) {
          return this.utilsService.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
              return this.utilsService.throwException("An unexpected server error occurred.", status, _responseText, _headers);
          }));
      }
      return _observableOf<FactoryFullOutPut>(<any>null);
  }

  /**
   * @param body (required) 
   * @return Success
   */
  get(body: GetFactoryInput): Observable<FactoryFullOutPut> {
      let url_ = this.baseUrl + "/api/services/app/Factory/Get";
      url_ = url_.replace(/[?&]$/, "");

      const content_ = JSON.stringify(body);

      let options_ : any = {
          body: content_,
          observe: "response",
          responseType: "blob",
          headers: new HttpHeaders({
              "Content-Type": "application/json-patch+json",
              "Accept": "text/plain"
          })
      };

      return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_ : any) => {
          return this.processGet(response_);
      })).pipe(_observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
              try {
                  return this.processGet(<any>response_);
              } catch (e) {
                  return <Observable<FactoryFullOutPut>><any>_observableThrow(e);
              }
          } else
              return <Observable<FactoryFullOutPut>><any>_observableThrow(response_);
      }));
  }

  protected processGet(response: HttpResponseBase): Observable<FactoryFullOutPut> {
      const status = response.status;
      const responseBlob = 
          response instanceof HttpResponse ? response.body : 
          (<any>response).error instanceof Blob ? (<any>response).error : undefined;

      let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }};
      if (status === 200) {
          return this.utilsService.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
          let result200: any = null;
          let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
          result200 = FactoryFullOutPut.fromJS(resultData200);
          return _observableOf(result200);
          }));
      } else if (status !== 200 && status !== 204) {
          return this.utilsService.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
          return this.utilsService.throwException("An unexpected server error occurred.", status, _responseText, _headers);
          }));
      }
      return _observableOf<FactoryFullOutPut>(<any>null);
  }

  /**
  * @return Success
  */
  getList(body: HttpParams |undefined): Observable<any[]> {
      let url_ = this.baseUrl + "/api/services/app/Factory/GetList";
      url_ = url_.replace(/[?&]$/, "");
      let options_: any = {
          observe: "response",
          responseType: "blob",
          params:body,
          headers: new HttpHeaders({
              "Accept": "text/plain"
          })
      };

      return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
          return this.processGetList(response_)

      })).pipe(_observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {

              try {
                  return this.processGetList(<any>response_);
              } catch (e) {
                  return <Observable<any[]>><any>_observableThrow(e);
              }
          } else {
              return <Observable<any[]>><any>_observableThrow(response_);
          }
      }));
  }

  protected processGetList(response: HttpResponseBase): Observable<any[]> {
      const status = response.status;
      const responseBlob =
          response instanceof HttpResponse ? response.body :
              (<any>response).error instanceof Blob ? (<any>response).error : undefined;

      let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
      if (status === 200) {
          return this.utilsService.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
              // let result200: any = null;
              let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
              // result200 = FactoryFullOutPut.fromJS(resultData200);
              return _observableOf(resultData200);
          }));
      } else if (status !== 200 && status !== 204) {
          return this.utilsService.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
              return this.utilsService.throwException("An unexpected server error occurred.", status, _responseText, _headers);
          }));
      }
      return _observableOf<any[]>(<any>null);
  }

  /**
   * @param body (required) 
   * @return Success
   */
  delete(body: DeleteFactoryInput): Observable<void> {
      let url_ = this.baseUrl + "/api/services/app/Factory/Delete";
      url_ = url_.replace(/[?&]$/, "");

      const content_ = JSON.stringify(body);
      let options_ : any = {
          body: content_,
          observe: "response",
          responseType: "blob",
          headers: new HttpHeaders({
              "Content-Type": "application/json-patch+json",
              "Accept": "text/plain"
          })
      };

      return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_ : any) => {
          return this.processDelete(response_);
      })).pipe(_observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
              try {
                  return this.processDelete(<any>response_);
              } catch (e) {
                  return <Observable<void>><any>_observableThrow(e);
              }
          } else
              return <Observable<void>><any>_observableThrow(response_);
      }));
  }

  protected processDelete(response: HttpResponseBase): Observable<void> {
      const status = response.status;
      const responseBlob = 
          response instanceof HttpResponse ? response.body : 
          (<any>response).error instanceof Blob ? (<any>response).error : undefined;

      let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }};
      if (status === 200) {
          return this.utilsService.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
          return _observableOf<void>(<any>null);
          }));
      } else if (status !== 200 && status !== 204) {
          return this.utilsService.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
          return this.utilsService.throwException("An unexpected server error occurred.", status, _responseText, _headers);
          }));
      }
      return _observableOf<void>(<any>null);
  }

  /**
   * @param body (optional) 
   * @return Success
   */
  update(body: UpdateFactoryInput): Observable<FactoryFullOutPut> {
      let url_ = this.baseUrl + "/api/services/app/Factory/Update";
      url_ = url_.replace(/[?&]$/, "");
      
      const content_ = JSON.stringify(body);

      let options_ : any = {
          body: content_,
          observe: "response",
          responseType: "blob",
          headers: new HttpHeaders({
              "Content-Type": "application/json-patch+json", 
              "Accept": "text/plain"
          })
      };

      return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_ : any) => {
          return this.processUpdate(response_);
      })).pipe(_observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
              try {
                  return this.processUpdate(<any>response_);
              } catch (e) {
                  return <Observable<FactoryFullOutPut>><any>_observableThrow(e);
              }
          } else
              return <Observable<FactoryFullOutPut>><any>_observableThrow(response_);
      }));
  }

  protected processUpdate(response: HttpResponseBase): Observable<FactoryFullOutPut> {
      const status = response.status;
      const responseBlob = 
          response instanceof HttpResponse ? response.body : 
          (<any>response).error instanceof Blob ? (<any>response).error : undefined;

      let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); }};
      if (status === 200) {
          return this.utilsService.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
          let result200: any = null;
          let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
          result200 = FactoryFullOutPut.fromJS(resultData200);
          return _observableOf(result200);
          }));
      } else if (status !== 200 && status !== 204) {
          return this.utilsService.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
          return this.utilsService.throwException("An unexpected server error occurred.", status, _responseText, _headers);
          }));
      }
      return _observableOf<FactoryFullOutPut>(<any>null);
  }
}
