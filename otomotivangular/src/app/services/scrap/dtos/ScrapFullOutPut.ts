
import { isObject } from "util";
import { ScrapTypePartOutPut } from "../../scrapType/dtos/ScrapTypePartOutPut";

export class ScrapFullOutPut implements IScrapFullOutPut {
  
    id:number;
    name: string;
    scrapType:ScrapTypePartOutPut;

    constructor(data?: IScrapFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.name = data.name;
                this.scrapType = data.scrapType;
            }
        }
    }

    static fromJS(data: any): ScrapFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new ScrapFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["scrapType"] = this.scrapType;
        return data; 
    }

    clone(): ScrapFullOutPut {
        const json = this.toJSON();
        let result = new ScrapFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IScrapFullOutPut{
    id:number;
    name: string;
    scrapType:ScrapTypePartOutPut;
}
