import { ScrapTypePartOutPut } from "../../scrapType/dtos/ScrapTypePartOutPut";

export class UpdateScrapInput implements IUpdateScrapInput {
    id: number;
    name: string;
    scrapType:ScrapTypePartOutPut;

  }
  
  export interface IUpdateScrapInput {
    id: number;
    name: string;
    scrapType:ScrapTypePartOutPut;

  }
  