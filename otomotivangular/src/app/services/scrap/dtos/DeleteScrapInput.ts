
export class DeleteScrapInput implements IDeleteScrapInput {
    id: number;
    name: string;
  }
  
  export interface IDeleteScrapInput {
    id: number;
    name: string;
  }
  