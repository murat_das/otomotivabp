import { ScrapTypePartOutPut } from './../../scrapType/dtos/ScrapTypePartOutPut';

export class CreateScrapInput implements ICreateScrapInput {
    name: string;
    scrapType:ScrapTypePartOutPut;

  }
  
  export interface ICreateScrapInput {
    name: string;
    scrapType:ScrapTypePartOutPut;
  }
  