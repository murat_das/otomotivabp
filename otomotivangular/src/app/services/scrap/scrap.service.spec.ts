/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ScrapService } from './scrap.service';

describe('Service: Scrap', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScrapService]
    });
  });

  it('should ...', inject([ScrapService], (service: ScrapService) => {
    expect(service).toBeTruthy();
  }));
});
