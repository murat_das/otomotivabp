/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MoldService } from './mold.service';

describe('Service: Mold', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MoldService]
    });
  });

  it('should ...', inject([MoldService], (service: MoldService) => {
    expect(service).toBeTruthy();
  }));
});
