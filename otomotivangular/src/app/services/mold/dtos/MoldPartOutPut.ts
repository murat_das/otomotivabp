
export class MoldPartOutPut implements IMoldPartOutPut {
    id: number;
    name: string;
    barcode: string;
    cavity: number;
  }
  
  export interface IMoldPartOutPut {
    id: number;
    name: string;
    barcode: string;
    cavity: number;
  }
  