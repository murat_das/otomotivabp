import { isObject } from "util";
import { ProductPartOutPut } from "../../product/dtos/ProductPartOutPut";

export class MoldFullOutPut implements IMoldFullOutPut {
  
    id:number;
    name: string;
    barcode: string;
    cavity: number;
    product: ProductPartOutPut;

    constructor(data?: IMoldFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.name = data.name;
                this.barcode = data.barcode;
                this.cavity = data.cavity;
                this.product = data.product;
            }
        }
    }

    static fromJS(data: any): MoldFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new MoldFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["barcode"] = this.barcode;
        data["cavity"] = this.cavity;
        data["product"] = this.product;
        return data; 
    }

    clone(): MoldFullOutPut {
        const json = this.toJSON();
        let result = new MoldFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IMoldFullOutPut{
    id:number;
    name: string;
    barcode: string;
    cavity: number;
    product: ProductPartOutPut;
}
