export class DeleteMoldInput implements IDeleteMoldInput {
    id: number;
    name: string;
  }
  
  export interface IDeleteMoldInput {
    id: number;
    name: string;
  }
  