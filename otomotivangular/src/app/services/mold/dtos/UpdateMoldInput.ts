import { ProductPartOutPut } from "../../product/dtos/ProductPartOutPut";

export class UpdateMoldInput implements IUpdateMoldInput {
  id: number;
  name: string;
  barcode: string;
  cavity: number;
  product: ProductPartOutPut;
}

export interface IUpdateMoldInput {
  id: number;
  name: string;
  barcode: string;
  cavity: number;
  product: ProductPartOutPut;
}
