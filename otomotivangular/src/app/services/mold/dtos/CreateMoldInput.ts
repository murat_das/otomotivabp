import { ProductPartOutPut } from './../../product/dtos/ProductPartOutPut';

export class CreateMoldInput implements ICreateMoldInput {
    name: string;
    barcode: string;
    cavity: number;
    product: ProductPartOutPut;
  }
  
  export interface ICreateMoldInput {
    name: string;
    barcode: string;
    cavity: number;
    product: ProductPartOutPut;
  }
  