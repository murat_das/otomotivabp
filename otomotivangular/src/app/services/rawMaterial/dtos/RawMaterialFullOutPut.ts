import { isObject } from "util";

export class RawMaterialFullOutPut implements IRawMaterialFullOutPut {
  
    id:number;
    name: string;
    barcode: string;

    constructor(data?: IRawMaterialFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.name = data.name;
                this.barcode = data.barcode;
            }
        }
    }

    static fromJS(data: any): RawMaterialFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new RawMaterialFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["barcode"] = this.barcode;
        return data; 
    }

    clone(): RawMaterialFullOutPut {
        const json = this.toJSON();
        let result = new RawMaterialFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IRawMaterialFullOutPut{
    id:number;
    name: string;
    barcode: string;
}
