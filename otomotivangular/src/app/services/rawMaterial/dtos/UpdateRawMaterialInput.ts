export class UpdateRawMaterialInput implements IUpdateRawMaterialInput {
    id: number;
    name: string;
    barcode: string;
  }
  
  export interface IUpdateRawMaterialInput {
    id: number;
    name: string;
    barcode: string;
  }
  