export class DeleteRawMaterialInput implements IDeleteRawMaterialInput {
    id: number;
    name: string;
  }
  
  export interface IDeleteRawMaterialInput {
    id: number;
    name: string;
  }
  