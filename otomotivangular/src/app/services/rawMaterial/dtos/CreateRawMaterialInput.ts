export class CreateRawMaterialInput implements ICreateRawMaterialInput {
    name: string;
    barcode: string;
  }
  
  export interface ICreateRawMaterialInput {
    name: string;
    barcode: string;
  }
  