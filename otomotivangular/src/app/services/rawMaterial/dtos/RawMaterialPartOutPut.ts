export class RawMaterialPartOutPut implements IRawMaterialPartOutPut {
    id: number;
    name: string;
    barcode: string;
  }
  
  export interface IRawMaterialPartOutPut {
    id: number;
    name: string;
    barcode: string;
  }
  