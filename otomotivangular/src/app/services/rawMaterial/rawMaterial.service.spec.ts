/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RawMaterialService } from './rawMaterial.service';

describe('Service: RawMaterial', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RawMaterialService]
    });
  });

  it('should ...', inject([RawMaterialService], (service: RawMaterialService) => {
    expect(service).toBeTruthy();
  }));
});
