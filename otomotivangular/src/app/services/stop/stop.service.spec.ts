/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { StopService } from './stop.service';

describe('Service: Stop', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StopService]
    });
  });

  it('should ...', inject([StopService], (service: StopService) => {
    expect(service).toBeTruthy();
  }));
});
