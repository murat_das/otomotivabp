export class DeleteStopInput implements IDeleteStopInput {
    id: number;
    name: string;
  }
  
  export interface IDeleteStopInput {
    id: number;
    name: string;
  }
  