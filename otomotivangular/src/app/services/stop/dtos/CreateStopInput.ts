import { StopTypePartOutPut } from "./../../stopType/dtos/StopTypePartOutPut";

export class CreateStopInput implements ICreateStopInput {
  name: string;
  stopType: StopTypePartOutPut;
}

export interface ICreateStopInput {
  name: string;
  stopType: StopTypePartOutPut;
}
