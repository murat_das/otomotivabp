import { isObject } from "util";
import { StopTypePartOutPut } from "../../stopType/dtos/StopTypePartOutPut";

export class StopFullOutPut implements IStopFullOutPut {
  id: number;
  name: string;
  stopType: StopTypePartOutPut;

  constructor(data?: IStopFullOutPut) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (isObject(data)) {
        this.id = data.id;
        this.name = data.name;
        this.stopType = data.stopType;
      }
    }
  }

  static fromJS(data: any): StopFullOutPut {
    data = typeof data === "object" ? data : {};
    let result = new StopFullOutPut();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["name"] = this.name;
    data["stopType"] = this.stopType;
    return data;
  }

  clone(): StopFullOutPut {
    const json = this.toJSON();
    let result = new StopFullOutPut();
    result.init(json);
    return result;
  }
}

export interface IStopFullOutPut {
  id: number;
  name: string;
  stopType: StopTypePartOutPut;
}
