export class StopPartOutPut implements IStopPartOutPut {
    id: number;
    name: string;
  }
  
  export interface IStopPartOutPut {
    id: number;
    name: string;
  }
  