import { StopTypePartOutPut } from "../../stopType/dtos/StopTypePartOutPut";

export class UpdateStopInput implements IUpdateStopInput {
    id: number;
    name: string;
    stopType: StopTypePartOutPut;
  }
  
  export interface IUpdateStopInput {
    id: number;
    name: string;
    stopType: StopTypePartOutPut;
  }
  