export class GetProductRawMaterialInput implements IGetProductRawMaterialInput {
    id: number;
  }
  
  export interface IGetProductRawMaterialInput {
    id: number;
  }
  