import { ProductPartOutPut } from "../../product/dtos/ProductPartOutPut";
import { RawMaterialPartOutPut } from "../../rawMaterial/dtos/RawMaterialPartOutPut";

export class DeleteProductRawMaterialInput
  implements IDeleteProductRawMaterialInput {
  id: number;
  product: ProductPartOutPut;
  rawMaterial: RawMaterialPartOutPut;
}

export interface IDeleteProductRawMaterialInput {
  id: number;
  product: ProductPartOutPut;
  rawMaterial: RawMaterialPartOutPut;
}
