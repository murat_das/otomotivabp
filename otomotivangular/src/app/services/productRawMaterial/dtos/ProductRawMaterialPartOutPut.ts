import { ProductPartOutPut } from "../../product/dtos/ProductPartOutPut";
import { RawMaterialPartOutPut } from "../../rawMaterial/dtos/RawMaterialPartOutPut";

export class ProductRawMaterialPartOutPut
  implements IProductRawMaterialPartOutPut {
  id: number;
  product: ProductPartOutPut;
  rawMaterial: RawMaterialPartOutPut;
}

export interface IProductRawMaterialPartOutPut {
  id: number;
  product: ProductPartOutPut;
  rawMaterial: RawMaterialPartOutPut;
}
