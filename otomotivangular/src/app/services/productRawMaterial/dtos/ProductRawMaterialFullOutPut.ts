
import { isObject } from "util";
import { ProductPartOutPut } from "../../product/dtos/ProductPartOutPut";
import { RawMaterialPartOutPut } from "../../rawMaterial/dtos/RawMaterialPartOutPut";

export class ProductRawMaterialFullOutPut implements IProductRawMaterialFullOutPut {
  
    id:number;
    product: ProductPartOutPut;
    rawMaterial: RawMaterialPartOutPut;

    constructor(data?: IProductRawMaterialFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.product = data.product;
                this.rawMaterial = data.rawMaterial;
            }
        }
    }

    static fromJS(data: any): ProductRawMaterialFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new ProductRawMaterialFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["product"] = this.product;
        data["rawMaterial"] = this.rawMaterial;
        return data; 
    }

    clone(): ProductRawMaterialFullOutPut {
        const json = this.toJSON();
        let result = new ProductRawMaterialFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IProductRawMaterialFullOutPut{
    id:number;
    product: ProductPartOutPut;
    rawMaterial: RawMaterialPartOutPut;
}
