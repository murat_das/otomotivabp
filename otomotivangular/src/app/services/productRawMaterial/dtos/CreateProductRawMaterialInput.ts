import { RawMaterialPartOutPut } from "./../../rawMaterial/dtos/RawMaterialPartOutPut";
import { ProductPartOutPut } from "./../../product/dtos/ProductPartOutPut";

export class CreateProductRawMaterialInput
  implements ICreateProductRawMaterialInput {
  product: ProductPartOutPut;
  rawMaterial: RawMaterialPartOutPut;
}

export interface ICreateProductRawMaterialInput {
  product: ProductPartOutPut;
  rawMaterial: RawMaterialPartOutPut;
}
