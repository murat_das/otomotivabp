import { ProductPartOutPut } from "../../product/dtos/ProductPartOutPut";
import { RawMaterialPartOutPut } from "../../rawMaterial/dtos/RawMaterialPartOutPut";

export class UpdateProductRawMaterialInput
  implements IUpdateProductRawMaterialInput {
  id: number;
  product: ProductPartOutPut;
  rawMaterial: RawMaterialPartOutPut;
}

export interface IUpdateProductRawMaterialInput {
  id: number;
  product: ProductPartOutPut;
  rawMaterial: RawMaterialPartOutPut;
}
