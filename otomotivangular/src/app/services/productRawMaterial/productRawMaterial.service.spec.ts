/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProductRawMaterialService } from './productRawMaterial.service';

describe('Service: ProductRawMaterial', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductRawMaterialService]
    });
  });

  it('should ...', inject([ProductRawMaterialService], (service: ProductRawMaterialService) => {
    expect(service).toBeTruthy();
  }));
});
