
export class DeleteScrapTypeInput implements IDeleteScrapTypeInput {
    id: number;
    name: string;
  }
  
  export interface IDeleteScrapTypeInput {
    id: number;
    name: string;
  }
  