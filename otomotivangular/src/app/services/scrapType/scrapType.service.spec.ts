/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ScrapTypeService } from './scrapType.service';

describe('Service: ScrapType', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScrapTypeService]
    });
  });

  it('should ...', inject([ScrapTypeService], (service: ScrapTypeService) => {
    expect(service).toBeTruthy();
  }));
});
