export class CreateStopTypeInput implements ICreateStopTypeInput {
    name: string;
  }
  
  export interface ICreateStopTypeInput {
    name: string;
  }
  