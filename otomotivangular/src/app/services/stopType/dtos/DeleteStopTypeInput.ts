export class DeleteStopTypeInput implements IDeleteStopTypeInput {
    id: number;
    name: string;
  }
  
  export interface IDeleteStopTypeInput {
    id: number;
    name: string;
  }
  