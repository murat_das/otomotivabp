export class StopTypePartOutPut implements IStopTypePartOutPut {
    id: number;
    name: string;
  }
  
  export interface IStopTypePartOutPut {
    id: number;
    name: string;
  }
  