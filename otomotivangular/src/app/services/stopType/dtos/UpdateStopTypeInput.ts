export class UpdateStopTypeInput implements IUpdateStopTypeInput {
    id: number;
    name: string;
  }
  
  export interface IUpdateStopTypeInput {
    id: number;
    name: string;
  }
  