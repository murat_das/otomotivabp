export class DeleteDeviceInput implements IDeleteDeviceInput {
    id: number;
    name: string;
  }
  
  export interface IDeleteDeviceInput {
    id: number;
    name: string;
  }
  