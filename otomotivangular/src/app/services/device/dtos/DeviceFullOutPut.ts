
import { isObject } from "util";
import { DeviceGroupPartOutPut } from "../../deviceGroup/dtos/DeviceGroupPartOutPut";

export class DeviceFullOutPut implements IDeviceFullOutPut {
  
    id:number;
    name: string;
    no:string;
    status:boolean;
    ipAddress:string;
    deviceGroup: DeviceGroupPartOutPut;

    constructor(data?: IDeviceFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.name = data.name;
                this.no = data.no;
                this.status = data.status;
                this.ipAddress = data.ipAddress;
                this.deviceGroup = data.deviceGroup;
            }
        }
    }

    static fromJS(data: any): DeviceFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new DeviceFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["no"] = this.no;
        data["status"] = this.status;
        data["ipAddress"] = this.ipAddress;
        data["deviceGroup"] = this.deviceGroup;
        return data; 
    }

    clone(): DeviceFullOutPut {
        const json = this.toJSON();
        let result = new DeviceFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IDeviceFullOutPut{
    id:number;
    name: string;
    no:string;
    status:boolean;
    ipAddress:string;
    deviceGroup: DeviceGroupPartOutPut;
}
