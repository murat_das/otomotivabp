import { DeviceGroupPartOutPut } from "../../deviceGroup/dtos/DeviceGroupPartOutPut";

export class UpdateDeviceInput implements IUpdateDeviceInput {
  id: number;
  name: string;
  no: string;
  status: boolean;
  ipAddress: string;
  deviceGroup: DeviceGroupPartOutPut;
}

export interface IUpdateDeviceInput {
  id: number;
  name: string;
  no: string;
  status: boolean;
  ipAddress: string;
  deviceGroup: DeviceGroupPartOutPut;
}
