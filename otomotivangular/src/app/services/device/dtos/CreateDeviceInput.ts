import { DeviceGroupPartOutPut } from "./../../deviceGroup/dtos/DeviceGroupPartOutPut";

export class CreateDeviceInput implements ICreateDeviceInput {
  name: string;
  no:string;
  status:boolean;
  ipAddress:string;
  deviceGroup: DeviceGroupPartOutPut;
}

export interface ICreateDeviceInput {
  name: string;
  no:string;
  status:boolean;
  ipAddress:string;
  deviceGroup: DeviceGroupPartOutPut;
}
