
export class DevicePartOutPut implements IDevicePartOutPut {
    id: number;
    name: string;
    no:string;
    status:boolean;
    ipAddress:string;
  }
  
  export interface IDevicePartOutPut {
    id: number;
    name: string;
    no:string;
    status:boolean;
    ipAddress:string;
  }
  