import { DevicePartOutPut } from "../../device/dtos/DevicePartOutPut";
import { MoldPartOutPut } from "../../mold/dtos/MoldPartOutPut";

export class DeviceMoldPartOutPut implements IDeviceMoldPartOutPut {
  id: number;
  device: DevicePartOutPut;
  mold: MoldPartOutPut;
}

export interface IDeviceMoldPartOutPut {
  id: number;
  device: DevicePartOutPut;
  mold: MoldPartOutPut;
}
