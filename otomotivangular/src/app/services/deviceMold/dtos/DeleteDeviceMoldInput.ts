import { DevicePartOutPut } from "../../device/dtos/DevicePartOutPut";
import { MoldPartOutPut } from "../../mold/dtos/MoldPartOutPut";

export class DeleteDeviceMoldInput implements IDeleteDeviceMoldInput {
    device: DevicePartOutPut;
    mold: MoldPartOutPut;
  }
  
  export interface IDeleteDeviceMoldInput {
    device: DevicePartOutPut;
    mold: MoldPartOutPut;
  }
  