import { MoldPartOutPut } from "./../../mold/dtos/MoldPartOutPut";
import { DevicePartOutPut } from "./../../device/dtos/DevicePartOutPut";

export class CreateDeviceMoldInput implements ICreateDeviceMoldInput {
  device: DevicePartOutPut;
  mold: MoldPartOutPut;
}

export interface ICreateDeviceMoldInput {
  device: DevicePartOutPut;
  mold: MoldPartOutPut;
}
