import { isObject } from "util";
import { DeviceGroupPartOutPut } from "../../deviceGroup/dtos/DeviceGroupPartOutPut";
import { DevicePartOutPut } from "../../device/dtos/DevicePartOutPut";
import { MoldPartOutPut } from "../../mold/dtos/MoldPartOutPut";

export class DeviceMoldFullOutPut implements IDeviceMoldFullOutPut {
  id: number;
  device: DevicePartOutPut;
  mold: MoldPartOutPut;

  constructor(data?: IDeviceMoldFullOutPut) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (isObject(data)) {
        this.id = data.id;
        this.device = data.device;
        this.mold = data.mold;
      }
    }
  }

  static fromJS(data: any): DeviceMoldFullOutPut {
    data = typeof data === "object" ? data : {};
    let result = new DeviceMoldFullOutPut();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["device"] = this.device;
    data["mold"] = this.mold;
    return data;
  }

  clone(): DeviceMoldFullOutPut {
    const json = this.toJSON();
    let result = new DeviceMoldFullOutPut();
    result.init(json);
    return result;
  }
}

export interface IDeviceMoldFullOutPut {
  id: number;
  device: DevicePartOutPut;
  mold: MoldPartOutPut;
}
