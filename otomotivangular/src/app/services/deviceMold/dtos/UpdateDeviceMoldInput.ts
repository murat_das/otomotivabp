import { DevicePartOutPut } from "../../device/dtos/DevicePartOutPut";
import { MoldPartOutPut } from "../../mold/dtos/MoldPartOutPut";

export class UpdateDeviceMoldInput implements IUpdateDeviceMoldInput {
  id: number;
  device: DevicePartOutPut;
  mold: MoldPartOutPut;
}

export interface IUpdateDeviceMoldInput {
  id: number;
  device: DevicePartOutPut;
  mold: MoldPartOutPut;
}
