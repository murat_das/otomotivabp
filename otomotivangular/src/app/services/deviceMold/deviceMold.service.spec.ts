/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DeviceMoldService } from './deviceMold.service';

describe('Service: DeviceMold', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeviceMoldService]
    });
  });

  it('should ...', inject([DeviceMoldService], (service: DeviceMoldService) => {
    expect(service).toBeTruthy();
  }));
});
