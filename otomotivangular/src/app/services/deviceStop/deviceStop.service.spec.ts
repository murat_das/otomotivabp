/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DeviceStopService } from './deviceStop.service';

describe('Service: DeviceStop', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeviceStopService]
    });
  });

  it('should ...', inject([DeviceStopService], (service: DeviceStopService) => {
    expect(service).toBeTruthy();
  }));
});
