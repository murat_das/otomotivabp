import { isObject } from "util";
import { DevicePartOutPut } from "../../device/dtos/DevicePartOutPut";
import { StopPartOutPut } from "../../stop/dtos/StopPartOutPut";

export class DeviceStopFullOutPut implements IDeviceStopFullOutPut {
  id: number;
  device: DevicePartOutPut;
  stop: StopPartOutPut;

  constructor(data?: IDeviceStopFullOutPut) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (isObject(data)) {
        this.id = data.id;
        this.device = data.device;
        this.stop = data.stop;
      }
    }
  }

  static fromJS(data: any): DeviceStopFullOutPut {
    data = typeof data === "object" ? data : {};
    let result = new DeviceStopFullOutPut();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["device"] = this.device;
    data["stop"] = this.stop;
    return data;
  }

  clone(): DeviceStopFullOutPut {
    const json = this.toJSON();
    let result = new DeviceStopFullOutPut();
    result.init(json);
    return result;
  }
}

export interface IDeviceStopFullOutPut {
  id: number;
  device: DevicePartOutPut;
  stop: StopPartOutPut;
}
