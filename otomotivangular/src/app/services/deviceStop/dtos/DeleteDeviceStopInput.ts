import { DevicePartOutPut } from "../../device/dtos/DevicePartOutPut";
import { StopPartOutPut } from "../../stop/dtos/StopPartOutPut";

export class DeleteDeviceStopInput implements IDeleteDeviceStopInput {
    device: DevicePartOutPut;
    stop: StopPartOutPut;
  }
  
  export interface IDeleteDeviceStopInput {
    device: DevicePartOutPut;
    stop: StopPartOutPut;
  }
  