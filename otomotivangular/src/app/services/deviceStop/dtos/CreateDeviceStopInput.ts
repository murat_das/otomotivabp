import { StopPartOutPut } from "./../../stop/dtos/StopPartOutPut";
import { DevicePartOutPut } from "./../../device/dtos/DevicePartOutPut";

export class CreateDeviceStopInput implements ICreateDeviceStopInput {
  device: DevicePartOutPut;
  stop: StopPartOutPut;
}

export interface ICreateDeviceStopInput {
  device: DevicePartOutPut;
  stop: StopPartOutPut;
}
