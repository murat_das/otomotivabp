import { DevicePartOutPut } from "../../device/dtos/DevicePartOutPut";
import { StopPartOutPut } from "../../stop/dtos/StopPartOutPut";

export class DeviceStopPartOutPut implements IDeviceStopPartOutPut {
    id: number;
    device: DevicePartOutPut;
    stop: StopPartOutPut;
  }
  
  export interface IDeviceStopPartOutPut {
    id: number;
    device: DevicePartOutPut;
    stop: StopPartOutPut;
  }
  