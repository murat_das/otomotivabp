import { DevicePartOutPut } from "../../device/dtos/DevicePartOutPut";
import { StopPartOutPut } from "../../stop/dtos/StopPartOutPut";

export class UpdateDeviceStopInput implements IUpdateDeviceStopInput {
    id: number;
    device: DevicePartOutPut;
    stop: StopPartOutPut;
  }
  
  export interface IUpdateDeviceStopInput {
    id: number;
    device: DevicePartOutPut;
    stop: StopPartOutPut;
  }