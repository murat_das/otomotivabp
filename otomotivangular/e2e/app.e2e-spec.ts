import { OtomotivAbpTemplatePage } from './app.po';

describe('OtomotivAbp App', function() {
  let page: OtomotivAbpTemplatePage;

  beforeEach(() => {
    page = new OtomotivAbpTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
