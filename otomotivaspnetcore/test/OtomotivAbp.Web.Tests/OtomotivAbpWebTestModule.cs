﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using OtomotivAbp.EntityFrameworkCore;
using OtomotivAbp.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace OtomotivAbp.Web.Tests
{
    [DependsOn(
        typeof(OtomotivAbpWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class OtomotivAbpWebTestModule : AbpModule
    {
        public OtomotivAbpWebTestModule(OtomotivAbpEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(OtomotivAbpWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(OtomotivAbpWebMvcModule).Assembly);
        }
    }
}