﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class DailyShiftRepository : OtomotivAbpRepositoryBase<DailyShift, int>, IDailyShiftRepository
    {
        public DailyShiftRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}