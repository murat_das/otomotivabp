﻿using Abp.Domain.Repositories;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public interface IScrapTypeRepository : IRepository<ScrapType, int>
    {

    }
}