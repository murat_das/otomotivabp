﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class ScrapTypeRepository : OtomotivAbpRepositoryBase<ScrapType, int>, IScrapTypeRepository
    {
        public ScrapTypeRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}