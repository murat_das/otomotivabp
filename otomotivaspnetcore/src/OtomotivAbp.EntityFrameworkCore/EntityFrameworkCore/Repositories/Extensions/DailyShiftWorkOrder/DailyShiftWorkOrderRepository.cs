﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class DailyShiftWorkOrderRepository : OtomotivAbpRepositoryBase<DailyShiftWorkOrder, int>, IDailyShiftWorkOrderRepository
    {
        public DailyShiftWorkOrderRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}