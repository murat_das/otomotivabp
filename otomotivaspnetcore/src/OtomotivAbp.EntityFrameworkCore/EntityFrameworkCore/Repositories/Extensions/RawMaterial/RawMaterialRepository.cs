﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class RawMaterialRepository : OtomotivAbpRepositoryBase<RawMaterial, int>, IRawMaterialRepository
    {
        public RawMaterialRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}