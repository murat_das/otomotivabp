﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class ProductScrapRepository : OtomotivAbpRepositoryBase<ProductScrap, int>, IProductScrapRepository
    {
        public ProductScrapRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}