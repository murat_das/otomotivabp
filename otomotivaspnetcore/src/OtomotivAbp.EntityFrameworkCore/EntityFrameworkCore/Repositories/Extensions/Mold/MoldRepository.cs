﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class MoldRepository : OtomotivAbpRepositoryBase<Mold, int>, IMoldRepository
    {
        public MoldRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}