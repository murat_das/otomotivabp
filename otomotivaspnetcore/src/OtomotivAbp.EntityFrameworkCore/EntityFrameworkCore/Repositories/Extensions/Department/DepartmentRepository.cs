﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class DepartmentRepository : OtomotivAbpRepositoryBase<Department, int>, IDepartmentRepository
    {
        public DepartmentRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}