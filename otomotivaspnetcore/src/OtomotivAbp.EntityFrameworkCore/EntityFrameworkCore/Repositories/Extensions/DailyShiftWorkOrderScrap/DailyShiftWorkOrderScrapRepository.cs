﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class DailyShiftWorkOrderScrapRepository : OtomotivAbpRepositoryBase<DailyShiftWorkOrderScrap, int>, IDailyShiftWorkOrderScrapRepository
    {
        public DailyShiftWorkOrderScrapRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}