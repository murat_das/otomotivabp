﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class ProductRawMaterialRepository : OtomotivAbpRepositoryBase<ProductRawMaterial, int>, IProductRawMaterialRepository
    {
        public ProductRawMaterialRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}