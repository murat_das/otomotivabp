﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class DeviceGroupRepository : OtomotivAbpRepositoryBase<DeviceGroup, int>, IDeviceGroupRepository
    {
        public DeviceGroupRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}