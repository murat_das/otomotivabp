﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class DeviceRepository : OtomotivAbpRepositoryBase<Device, int>, IDeviceRepository
    {
        public DeviceRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}