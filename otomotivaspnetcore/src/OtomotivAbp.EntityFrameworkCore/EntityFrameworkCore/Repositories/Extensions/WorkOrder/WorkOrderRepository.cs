﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class WorkOrderRepository : OtomotivAbpRepositoryBase<WorkOrder, int>, IWorkOrderRepository
    {
        public WorkOrderRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}