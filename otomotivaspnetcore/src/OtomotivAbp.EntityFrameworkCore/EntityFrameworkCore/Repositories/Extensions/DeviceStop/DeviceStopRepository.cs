﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class DeviceStopRepository : OtomotivAbpRepositoryBase<DeviceStop, int>, IDeviceStopRepository
    {
        public DeviceStopRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}