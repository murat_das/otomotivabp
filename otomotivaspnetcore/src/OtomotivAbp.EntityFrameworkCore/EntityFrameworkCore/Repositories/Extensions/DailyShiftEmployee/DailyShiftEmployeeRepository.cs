﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class DailyShiftEmployeeRepository : OtomotivAbpRepositoryBase<DailyShiftEmployee, int>, IDailyShiftEmployeeRepository
    {
        public DailyShiftEmployeeRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}