﻿using Abp.Domain.Repositories;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public interface IScrapRepository : IRepository<Domain.Entities.Scrap, int>
    {
        
    }
}