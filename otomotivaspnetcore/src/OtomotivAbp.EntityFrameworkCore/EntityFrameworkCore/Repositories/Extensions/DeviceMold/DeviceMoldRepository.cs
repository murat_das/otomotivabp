﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class DeviceMoldRepository : OtomotivAbpRepositoryBase<DeviceMold, int>, IDeviceMoldRepository
    {
        public DeviceMoldRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}