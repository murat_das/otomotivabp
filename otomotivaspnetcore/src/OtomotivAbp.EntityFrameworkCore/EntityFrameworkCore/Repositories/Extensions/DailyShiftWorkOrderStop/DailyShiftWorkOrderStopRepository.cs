﻿using Abp.EntityFrameworkCore;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.EntityFrameworkCore.Repositories
{
    public class DailyShiftWorkOrderStopRepository : OtomotivAbpRepositoryBase<DailyShiftWorkOrderStop, int>, IDailyShiftWorkOrderStopRepository
    {
        public DailyShiftWorkOrderStopRepository(IDbContextProvider<OtomotivAbpDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}