﻿using System;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using OtomotivAbp.Authorization.Roles;
using OtomotivAbp.Authorization.Users;
using OtomotivAbp.Domain.Configurations;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.MultiTenancy;

namespace OtomotivAbp.EntityFrameworkCore
{
    public class OtomotivAbpDbContext : AbpZeroDbContext<Tenant, Role, User, OtomotivAbpDbContext>
    {
        /* Define a DbSet for each entity of the application */
        #region DBSet
        public DbSet<DailyShift> DailyShifts { get; set; }
        public DbSet<DailyShiftEmployee> DailyShiftEmployees { get; set; }
        public DbSet<DailyShiftWorkOrder> DailyShiftWorkOrders { get; set; }
        public DbSet<DailyShiftWorkOrderScrap> DailyShiftWorkOrderScraps { get; set; }
        public DbSet<DailyShiftWorkOrderStop> DailyShiftWorkOrderStops { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<DeviceGroup> DeviceGroups { get; set; }
        public DbSet<DeviceMold> DeviceMolds { get; set; }
        public DbSet<DeviceStop> DeviceStops { get; set; }
        public DbSet<Factory> Factories { get; set; }
        public DbSet<Mold> Molds { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductRawMaterial> ProductRawMaterials { get; set; }
        public DbSet<ProductScrap> ProductScraps { get; set; }
        public DbSet<RawMaterial> RawMaterials { get; set; }
        public DbSet<Scrap> Scraps { get; set; }
        public DbSet<ScrapType> ScrapTypes { get; set; }
        public DbSet<Shift> Shifts { get; set; }
        public DbSet<Stop> Stops { get; set; }
        public DbSet<StopType> StopTypes { get; set; }
        public DbSet<WorkOrder> WorkOrders { get; set; }

        #endregion DBSet

        public OtomotivAbpDbContext(DbContextOptions<OtomotivAbpDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .ApplyConfiguration(new DailyShiftConfiguration())
                .ApplyConfiguration(new DailyShiftEmployeeConfiguration())
                .ApplyConfiguration(new DailyShiftWorkOrderConfiguration())
                .ApplyConfiguration(new DailyShiftWorkOrderScrapConfiguration())
                .ApplyConfiguration(new DailyShiftWorkOrderStopConfiguration())
                .ApplyConfiguration(new DepartmentConfiguration())
                .ApplyConfiguration(new DeviceConfiguration())
                .ApplyConfiguration(new DeviceGroupConfiguration())
                .ApplyConfiguration(new DeviceMoldConfiguration())
                .ApplyConfiguration(new DeviceStopConfiguration())
                .ApplyConfiguration(new FactoryConfiguration())
                .ApplyConfiguration(new MoldConfiguration())
                .ApplyConfiguration(new ProductConfiguration())
                .ApplyConfiguration(new ProductRawMaterialConfiguration())
                .ApplyConfiguration(new ProductScrapConfiguration())
                .ApplyConfiguration(new RawMaterialConfiguration())
                .ApplyConfiguration(new ScrapConfiguration())
                .ApplyConfiguration(new ScrapTypeConfiguration())
                .ApplyConfiguration(new ShiftConfiguration())
                .ApplyConfiguration(new StopConfiguration())
                .ApplyConfiguration(new StopTypeConfiguration())
                .ApplyConfiguration(new WorkOrderConfiguration());

        }
    }
}
