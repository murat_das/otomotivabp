﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using OtomotivAbp.Configuration;
using OtomotivAbp.Web;

namespace OtomotivAbp.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class OtomotivAbpDbContextFactory : IDesignTimeDbContextFactory<OtomotivAbpDbContext>
    {
        public OtomotivAbpDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<OtomotivAbpDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());
            builder.UseLazyLoadingProxies();


            OtomotivAbpDbContextConfigurer.Configure(builder, configuration.GetConnectionString(OtomotivAbpConsts.ConnectionStringName));

            return new OtomotivAbpDbContext(builder.Options);
        }
    }
}
