using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace OtomotivAbp.EntityFrameworkCore
{
    public static class OtomotivAbpDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<OtomotivAbpDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<OtomotivAbpDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
