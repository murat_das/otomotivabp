﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OtomotivAbp.Migrations
{
    public partial class deneme6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StopId",
                table: "DailyShiftWorkOrderStop",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_DailyShiftWorkOrderStop_StopId",
                table: "DailyShiftWorkOrderStop",
                column: "StopId");

            migrationBuilder.AddForeignKey(
                name: "FK_DailyShiftWorkOrderStop_Stop_StopId",
                table: "DailyShiftWorkOrderStop",
                column: "StopId",
                principalTable: "Stop",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DailyShiftWorkOrderStop_Stop_StopId",
                table: "DailyShiftWorkOrderStop");

            migrationBuilder.DropIndex(
                name: "IX_DailyShiftWorkOrderStop_StopId",
                table: "DailyShiftWorkOrderStop");

            migrationBuilder.DropColumn(
                name: "StopId",
                table: "DailyShiftWorkOrderStop");
        }
    }
}
