﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OtomotivAbp.Migrations
{
    public partial class deneme2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "No",
                table: "WorkOrder",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "WorkOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "TargetAmount",
                table: "WorkOrder",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "WorkOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Barcode",
                table: "Product",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StockCode",
                table: "Product",
                maxLength: 150,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "No",
                table: "WorkOrder");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "WorkOrder");

            migrationBuilder.DropColumn(
                name: "TargetAmount",
                table: "WorkOrder");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "WorkOrder");

            migrationBuilder.DropColumn(
                name: "Barcode",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "StockCode",
                table: "Product");
        }
    }
}
