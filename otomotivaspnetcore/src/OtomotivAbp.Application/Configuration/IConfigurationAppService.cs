﻿using System.Threading.Tasks;
using OtomotivAbp.Configuration.Dto;

namespace OtomotivAbp.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
