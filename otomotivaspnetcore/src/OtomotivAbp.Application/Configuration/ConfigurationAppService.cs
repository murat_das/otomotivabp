﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using OtomotivAbp.Configuration.Dto;

namespace OtomotivAbp.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : OtomotivAbpAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
