﻿using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Sessions.Dto;

namespace OtomotivAbp.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
