﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.Factories.Dtos;

namespace OtomotivAbp.Domain.Departments.Dtos
{
    public class DepartmentFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public FactoryPartOutPut Factory { get; set; }

    }
}