﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.Departments.Dtos
{
    public class DepartmentPartOutPut : EntityDto<int>
    {
        public string Name { get; set; }
    }
}