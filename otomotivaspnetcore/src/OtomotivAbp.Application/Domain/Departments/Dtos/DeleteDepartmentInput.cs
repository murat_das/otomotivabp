﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.Departments.Dtos
{
    public class DeleteDepartmentInput : EntityDto<int>
    {

    }
}