﻿using OtomotivAbp.Domain.Factories.Dtos;

namespace OtomotivAbp.Domain.Departments.Dtos
{
    public class CreateDepartmentInput
    {
        public string Name { get; set; }
        public FactoryPartOutPut Factory { get; set; }
    }
}