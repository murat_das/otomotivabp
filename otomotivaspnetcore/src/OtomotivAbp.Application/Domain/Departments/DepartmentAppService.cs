﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.Departments.Dtos;

namespace OtomotivAbp.Domain.Departments
{
    public class DepartmentAppService : OtomotivAbpAppServiceBase, IDepartmentAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IDepartmentRepository _departmentRepository;

        public DepartmentAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IDepartmentRepository departmentRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _departmentRepository = departmentRepository;
        }

        [HttpPost]
        public async Task<DepartmentFullOutPut> CreateAsync(CreateDepartmentInput input)
        {
            var department = new Department()
            {
               Name = input.Name,
               FactoryId = input.Factory.Id
            };

            await _departmentRepository.InsertAsync(department);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DepartmentFullOutPut>(department);
        }

        [HttpPost]
        public async Task<DepartmentFullOutPut> GetAsync(GetDepartmentInput input)
        {
            var department = await _entityManager.GetDepartmentAsync(input.Id);

            return ObjectMapper.Map<DepartmentFullOutPut>(department);
        }

        [HttpPost]
        public async Task<List<DepartmentFullOutPut>> GetListAsync()
        {
            var departmentList = await _departmentRepository.GetAllListAsync();

            return ObjectMapper.Map<List<DepartmentFullOutPut>>(departmentList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteDepartmentInput input)
        {
            var department = await _entityManager.GetDepartmentAsync(input.Id);

            await _departmentRepository.DeleteAsync(department.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<DepartmentFullOutPut> UpdateAsync(UpdateDepartmentInput input)
        {
            var department = await _entityManager.GetDepartmentAsync(input.Id);

            department.Name = input.Name;
            department.FactoryId = input.Factory.Id;

            await _departmentRepository.UpdateAsync(department);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DepartmentFullOutPut>(department);
        }
    }
}