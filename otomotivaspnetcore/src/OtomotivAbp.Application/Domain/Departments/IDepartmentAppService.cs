﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.Departments.Dtos;

namespace OtomotivAbp.Domain.Departments
{
    public interface IDepartmentAppService : IApplicationService
    {
        #region Async Methods
        Task<DepartmentFullOutPut> CreateAsync(CreateDepartmentInput input);
        Task<DepartmentFullOutPut> GetAsync(GetDepartmentInput input);
        Task<List<DepartmentFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteDepartmentInput input);
        Task<DepartmentFullOutPut> UpdateAsync(UpdateDepartmentInput input);
        #endregion Async Methods
    }
}