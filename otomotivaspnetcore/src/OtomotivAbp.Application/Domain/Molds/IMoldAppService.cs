﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.Molds.Dtos;

namespace OtomotivAbp.Domain.Molds
{
    public interface IMoldAppService : IApplicationService
    {
        #region Async Methods
        Task<MoldFullOutPut> CreateAsync(CreateMoldInput input);
        Task<MoldFullOutPut> GetAsync(GetMoldInput input);
        Task<List<MoldFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteMoldInput input);
        Task<MoldFullOutPut> UpdateAsync(UpdateMoldInput input);
        #endregion Async Methods
    }
}