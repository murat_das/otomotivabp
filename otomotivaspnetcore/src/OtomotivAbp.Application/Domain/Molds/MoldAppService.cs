﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.Molds.Dtos;

namespace OtomotivAbp.Domain.Molds
{
    public class MoldAppService : OtomotivAbpAppServiceBase, IMoldAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IMoldRepository _moldRepository;

        public MoldAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IMoldRepository moldRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _moldRepository = moldRepository;
        }

        [HttpPost]
        public async Task<MoldFullOutPut> CreateAsync(CreateMoldInput input)
        {
            var mold = new Mold()
            {
                Name = input.Name,
                Barcode = input.Barcode,
                Cavity = input.Cavity,
                ProductId = input.Product.Id
            };

            await _moldRepository.InsertAsync(mold);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<MoldFullOutPut>(mold);
        }

        [HttpPost]
        public async Task<MoldFullOutPut> GetAsync(GetMoldInput input)
        {
            var mold = await _entityManager.GetMoldAsync(input.Id);

            return ObjectMapper.Map<MoldFullOutPut>(mold);
        }

        [HttpPost]
        public async Task<List<MoldFullOutPut>> GetListAsync()
        {
            var moldList = await _moldRepository.GetAllListAsync();

            return ObjectMapper.Map<List<MoldFullOutPut>>(moldList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteMoldInput input)
        {
            var mold = await _entityManager.GetMoldAsync(input.Id);

            await _moldRepository.DeleteAsync(mold.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<MoldFullOutPut> UpdateAsync(UpdateMoldInput input)
        {
            var mold = await _entityManager.GetMoldAsync(input.Id);

            mold.Name = input.Name;
            mold.Barcode = input.Barcode;
            mold.Cavity = input.Cavity;
            mold.ProductId = input.Product.Id;

            await _moldRepository.UpdateAsync(mold);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<MoldFullOutPut>(mold);
        }
    }
}