﻿using OtomotivAbp.Domain.Products.Dtos;

namespace OtomotivAbp.Domain.Molds.Dtos
{
    public class CreateMoldInput
    {
        public string Name { get; set; }
        public string Barcode { get; set; }
        public int Cavity { get; set; }
        public ProductPartOutPut Product { get; set; }
    }
}