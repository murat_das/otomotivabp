﻿using System;
using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.Shifts.Dtos
{
    public class ShiftFullOutPut:EntityDto<int>
    {
        public string Name { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
    }
}