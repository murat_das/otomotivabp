﻿using System;

namespace OtomotivAbp.Domain.Shifts.Dtos
{
    public class CreateShiftInput
    {
        public string Name { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
    }
}