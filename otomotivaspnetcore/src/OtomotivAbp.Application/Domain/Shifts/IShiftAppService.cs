﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.Shifts.Dtos;

namespace OtomotivAbp.Domain.Shifts
{
    public interface IShiftAppService : IApplicationService
    {
        #region Async Methods
        Task<ShiftFullOutPut> CreateAsync(CreateShiftInput input);
        Task<ShiftFullOutPut> GetAsync(GetShiftInput input);
        Task<List<ShiftFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteShiftInput input);
        Task<ShiftFullOutPut> UpdateAsync(UpdateShiftInput input);
        #endregion Async Methods
    }
}