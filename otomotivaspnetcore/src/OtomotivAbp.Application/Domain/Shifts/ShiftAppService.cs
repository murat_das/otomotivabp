﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.Shifts.Dtos;

namespace OtomotivAbp.Domain.Shifts
{
    public class ShiftAppService : OtomotivAbpAppServiceBase, IShiftAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IShiftRepository _shiftRepository;

        public ShiftAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IShiftRepository shiftRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _shiftRepository = shiftRepository;
        }

        [HttpPost]
        public async Task<ShiftFullOutPut> CreateAsync(CreateShiftInput input)
        {
            var shift = new Shift()
            {
                Name = input.Name,
                StartTime = input.StartTime,
                EndTime = input.EndTime,
            };

            await _shiftRepository.InsertAsync(shift);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ShiftFullOutPut>(shift);
        }

        [HttpPost]
        public async Task<ShiftFullOutPut> GetAsync(GetShiftInput input)
        {
            var shift = await _entityManager.GetShiftAsync(input.Id);

            return ObjectMapper.Map<ShiftFullOutPut>(shift);
        }

        [HttpPost]
        public async Task<List<ShiftFullOutPut>> GetListAsync()
        {
            var shiftList = await _shiftRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ShiftFullOutPut>>(shiftList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteShiftInput input)
        {
            var shift = await _entityManager.GetShiftAsync(input.Id);

            await _shiftRepository.DeleteAsync(shift.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<ShiftFullOutPut> UpdateAsync(UpdateShiftInput input)
        {
            var shift = await _entityManager.GetShiftAsync(input.Id);

            shift.Name = input.Name;
            shift.StartTime = input.StartTime;
            shift.EndTime = input.EndTime;

            await _shiftRepository.UpdateAsync(shift);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ShiftFullOutPut>(shift);
        }
    }
}