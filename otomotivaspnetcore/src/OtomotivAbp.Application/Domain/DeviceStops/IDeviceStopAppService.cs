﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.DeviceStops.Dtos;

namespace OtomotivAbp.Domain.DeviceStops
{
    public interface IDeviceStopAppService : IApplicationService
    {
        #region Async Methods
        Task<DeviceStopFullOutPut> CreateAsync(CreateDeviceStopInput input);
        Task<DeviceStopFullOutPut> GetAsync(GetDeviceStopInput input);
        Task<List<DeviceStopFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteDeviceStopInput input);
        Task<DeviceStopFullOutPut> UpdateAsync(UpdateDeviceStopInput input);
        #endregion Async Methods
    }
}