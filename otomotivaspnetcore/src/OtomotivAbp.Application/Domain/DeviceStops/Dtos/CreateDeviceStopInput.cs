﻿using OtomotivAbp.Domain.Devices.Dtos;
using OtomotivAbp.Domain.Stops.Dtos;

namespace OtomotivAbp.Domain.DeviceStops.Dtos
{
    public class CreateDeviceStopInput
    {
        public StopPartOutPut Stop { get; set; }
        public DevicePartOutPut Device { get; set; }
    }
}