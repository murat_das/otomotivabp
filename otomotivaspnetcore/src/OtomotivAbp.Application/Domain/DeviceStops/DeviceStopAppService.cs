﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.DeviceStops.Dtos;

namespace OtomotivAbp.Domain.DeviceStops
{
    public class DeviceStopAppService : OtomotivAbpAppServiceBase, IDeviceStopAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IDeviceStopRepository _deviceStopRepository;

        public DeviceStopAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IDeviceStopRepository deviceStopRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _deviceStopRepository = deviceStopRepository;
        }

        [HttpPost]
        public async Task<DeviceStopFullOutPut> CreateAsync(CreateDeviceStopInput input)
        {
            var deviceStop = new DeviceStop()
            {
                DeviceId = input.Device.Id,
                StopId = input.Stop.Id,
            };

            await _deviceStopRepository.InsertAsync(deviceStop);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DeviceStopFullOutPut>(deviceStop);
        }

        [HttpPost]
        public async Task<DeviceStopFullOutPut> GetAsync(GetDeviceStopInput input)
        {
            var deviceStop = await _entityManager.GetDeviceStopAsync(input.Id);

            return ObjectMapper.Map<DeviceStopFullOutPut>(deviceStop);
        }

        [HttpPost]
        public async Task<List<DeviceStopFullOutPut>> GetListAsync()
        {
            var deviceStopList = await _deviceStopRepository.GetAllListAsync();

            return ObjectMapper.Map<List<DeviceStopFullOutPut>>(deviceStopList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteDeviceStopInput input)
        {
            var deviceStop = await _entityManager.GetDeviceStopAsync(input.Id);

            await _deviceStopRepository.DeleteAsync(deviceStop.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<DeviceStopFullOutPut> UpdateAsync(UpdateDeviceStopInput input)
        {
            var deviceStop = await _entityManager.GetDeviceStopAsync(input.Id);

            deviceStop.DeviceId = input.Device.Id;
            deviceStop.StopId = input.Stop.Id;

            await _deviceStopRepository.UpdateAsync(deviceStop);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DeviceStopFullOutPut>(deviceStop);
        }
    }
}