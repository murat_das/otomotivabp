﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.WorkOrders.Dtos;

namespace OtomotivAbp.Domain.WorkOrders
{
    public class WorkOrderAppService : OtomotivAbpAppServiceBase, IWorkOrderAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IWorkOrderRepository _workOrderRepository;

        public WorkOrderAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IWorkOrderRepository workOrderRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _workOrderRepository = workOrderRepository;
        }

        [HttpPost]
        public async Task<WorkOrderFullOutPut> CreateAsync(CreateWorkOrderInput input)
        {
            var workOrder = new WorkOrder()
            {
               DeviceId = input.Device.Id,
               MoldId = input.Mold.Id,
               ProductId = input.Product.Id,
               No = input.No,
               Status = input.Status,
               TargetAmount = input.TargetAmount,
               Type = input.Type
            };

            await _workOrderRepository.InsertAsync(workOrder);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<WorkOrderFullOutPut>(workOrder);
        }

        [HttpPost]
        public async Task<WorkOrderFullOutPut> GetAsync(GetWorkOrderInput input)
        {
            var workOrder = await _entityManager.GetWorkOrderAsync(input.Id);

            return ObjectMapper.Map<WorkOrderFullOutPut>(workOrder);
        }

        [HttpPost]
        public async Task<List<WorkOrderFullOutPut>> GetListAsync()
        {
            var workOrderList = await _workOrderRepository.GetAllListAsync();

            return ObjectMapper.Map<List<WorkOrderFullOutPut>>(workOrderList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteWorkOrderInput input)
        {
            var workOrder = await _entityManager.GetWorkOrderAsync(input.Id);

            await _workOrderRepository.DeleteAsync(workOrder.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<WorkOrderFullOutPut> UpdateAsync(UpdateWorkOrderInput input)
        {
            var workOrder = await _entityManager.GetWorkOrderAsync(input.Id);

            workOrder.DeviceId = input.Device.Id;
            workOrder.MoldId = input.Mold.Id;
            workOrder.ProductId = input.Product.Id;
            workOrder.No = input.No;
            workOrder.Status = input.Status;
            workOrder.TargetAmount = input.TargetAmount;
            workOrder.Type = input.Type;

            await _workOrderRepository.UpdateAsync(workOrder);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<WorkOrderFullOutPut>(workOrder);
        }
    }
}