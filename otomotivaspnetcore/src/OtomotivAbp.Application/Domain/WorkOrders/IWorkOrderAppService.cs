﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.WorkOrders.Dtos;

namespace OtomotivAbp.Domain.WorkOrders
{
    public interface IWorkOrderAppService : IApplicationService
    {
        #region Async Methods
        Task<WorkOrderFullOutPut> CreateAsync(CreateWorkOrderInput input);
        Task<WorkOrderFullOutPut> GetAsync(GetWorkOrderInput input);
        Task<List<WorkOrderFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteWorkOrderInput input);
        Task<WorkOrderFullOutPut> UpdateAsync(UpdateWorkOrderInput input);
        #endregion Async Methods
    }
}