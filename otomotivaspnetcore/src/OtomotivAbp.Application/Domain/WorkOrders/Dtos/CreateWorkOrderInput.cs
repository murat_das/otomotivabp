﻿using OtomotivAbp.Domain.Devices.Dtos;
using OtomotivAbp.Domain.Molds.Dtos;
using OtomotivAbp.Domain.Products.Dtos;

namespace OtomotivAbp.Domain.WorkOrders.Dtos
{
    public class CreateWorkOrderInput
    {
        public string No { get; set; }
        public int Status { get; set; }
        public decimal TargetAmount { get; set; }
        public int Type { get; set; }
        public DevicePartOutPut Device { get; set; }
        public ProductPartOutPut Product { get; set; }
        public MoldPartOutPut Mold { get; set; }
    }
}