﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.WorkOrders.Dtos
{
    public class WorkOrderPartOutPut : EntityDto<int>
    {
        public string No { get; set; }
        public int Status { get; set; }
        public decimal TargetAmount { get; set; }
        public int Type { get; set; }
    }
}