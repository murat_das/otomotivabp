﻿using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.Devices.Dtos;
using OtomotivAbp.Domain.Molds.Dtos;

namespace OtomotivAbp.Domain.DeviceMolds.Dtos
{
    public class DeviceMoldFullOutPut : EntityDto<int>
    {
        public DevicePartOutPut Device { get; set; }
        public MoldPartOutPut Mold { get; set; }
    }
}