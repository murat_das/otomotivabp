﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.DeviceMolds.Dtos
{
    public class DeleteDeviceMoldInput : EntityDto<int>
    {

    }
}