﻿using OtomotivAbp.Domain.Devices.Dtos;
using OtomotivAbp.Domain.Molds.Dtos;

namespace OtomotivAbp.Domain.DeviceMolds.Dtos
{
    public class CreateDeviceMoldInput
    {
        public DevicePartOutPut Device { get; set; }
        public MoldPartOutPut Mold { get; set; }
    }
}