﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.DeviceMolds.Dtos;

namespace OtomotivAbp.Domain.DeviceMolds
{
    public interface IDeviceMoldAppService : IApplicationService
    {
        #region Async Methods
        Task<DeviceMoldFullOutPut> CreateAsync(CreateDeviceMoldInput input);
        Task<DeviceMoldFullOutPut> GetAsync(GetDeviceMoldInput input);
        Task<List<DeviceMoldFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteDeviceMoldInput input);
        Task<DeviceMoldFullOutPut> UpdateAsync(UpdateDeviceMoldInput input);
        #endregion Async Methods
    }
}