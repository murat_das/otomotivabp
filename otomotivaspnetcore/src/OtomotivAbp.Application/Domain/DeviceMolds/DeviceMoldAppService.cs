﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.DeviceMolds.Dtos;

namespace OtomotivAbp.Domain.DeviceMolds
{
    public class DeviceMoldAppService : OtomotivAbpAppServiceBase, IDeviceMoldAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IDeviceMoldRepository _deviceMoldRepository;

        public DeviceMoldAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IDeviceMoldRepository deviceMoldRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _deviceMoldRepository = deviceMoldRepository;
        }

        [HttpPost]
        public async Task<DeviceMoldFullOutPut> CreateAsync(CreateDeviceMoldInput input)
        {
            var deviceMold = new DeviceMold()
            {
                DeviceId = input.Device.Id,
                MoldId = input.Mold.Id
            };

            await _deviceMoldRepository.InsertAsync(deviceMold);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DeviceMoldFullOutPut>(deviceMold);
        }

        [HttpPost]
        public async Task<DeviceMoldFullOutPut> GetAsync(GetDeviceMoldInput input)
        {
            var deviceMold = await _entityManager.GetDeviceMoldAsync(input.Id);

            return ObjectMapper.Map<DeviceMoldFullOutPut>(deviceMold);
        }

        [HttpPost]
        public async Task<List<DeviceMoldFullOutPut>> GetListAsync()
        {
            var deviceMoldList = await _deviceMoldRepository.GetAllListAsync();

            return ObjectMapper.Map<List<DeviceMoldFullOutPut>>(deviceMoldList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteDeviceMoldInput input)
        {
            var deviceMold = await _entityManager.GetDeviceMoldAsync(input.Id);

            await _deviceMoldRepository.DeleteAsync(deviceMold.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<DeviceMoldFullOutPut> UpdateAsync(UpdateDeviceMoldInput input)
        {
            var deviceMold = await _entityManager.GetDeviceMoldAsync(input.Id);

            deviceMold.DeviceId = input.Device.Id;
            deviceMold.MoldId = input.Mold.Id;

            await _deviceMoldRepository.UpdateAsync(deviceMold);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DeviceMoldFullOutPut>(deviceMold);
        }
    }
}