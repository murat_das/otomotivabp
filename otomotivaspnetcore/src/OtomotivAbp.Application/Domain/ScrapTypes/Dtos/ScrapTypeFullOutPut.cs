﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.ScrapTypes.Dtos
{
    public class ScrapTypeFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }
    }
}