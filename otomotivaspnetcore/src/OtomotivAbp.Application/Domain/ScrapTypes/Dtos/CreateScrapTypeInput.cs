﻿namespace OtomotivAbp.Domain.ScrapTypes.Dtos
{
    public class CreateScrapTypeInput
    {
        public string Name { get; set; }
    }
}