﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.ScrapTypes.Dtos;

namespace OtomotivAbp.Domain.ScrapTypes
{
    public class ScrapTypeAppService : OtomotivAbpAppServiceBase, IScrapTypeAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IScrapTypeRepository _scrapTypeRepository;

        public ScrapTypeAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IScrapTypeRepository scrapTypeRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _scrapTypeRepository = scrapTypeRepository;
        }

        [HttpPost]
        public async Task<ScrapTypeFullOutPut> CreateAsync(CreateScrapTypeInput input)
        {
            var scrapType = new ScrapType()
            {
                Name = input.Name
            };

            await _scrapTypeRepository.InsertAsync(scrapType);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ScrapTypeFullOutPut>(scrapType);
        }

        [HttpPost]
        public async Task<ScrapTypeFullOutPut> GetAsync(GetScrapTypeInput input)
        {
            var scrapType = await _entityManager.GetScrapTypeAsync(input.Id);

            return ObjectMapper.Map<ScrapTypeFullOutPut>(scrapType);
        }

        [HttpPost]
        public async Task<List<ScrapTypeFullOutPut>> GetListAsync()
        {
            var scrapTypeList = await _scrapTypeRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ScrapTypeFullOutPut>>(scrapTypeList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteScrapTypeInput input)
        {
            var scrapType = await _entityManager.GetScrapTypeAsync(input.Id);

            await _scrapTypeRepository.DeleteAsync(scrapType.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<ScrapTypeFullOutPut> UpdateAsync(UpdateScrapTypeInput input)
        {
            var scrapType = await _entityManager.GetScrapTypeAsync(input.Id);

            scrapType.Name = input.Name;

            await _scrapTypeRepository.UpdateAsync(scrapType);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ScrapTypeFullOutPut>(scrapType);
        }
    }
}