﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.ScrapTypes.Dtos;

namespace OtomotivAbp.Domain.ScrapTypes
{
    public interface IScrapTypeAppService : IApplicationService
    {
        #region Async Methods
        Task<ScrapTypeFullOutPut> CreateAsync(CreateScrapTypeInput input);
        Task<ScrapTypeFullOutPut> GetAsync(GetScrapTypeInput input);
        Task<List<ScrapTypeFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteScrapTypeInput input);
        Task<ScrapTypeFullOutPut> UpdateAsync(UpdateScrapTypeInput input);
        #endregion Async Methods
    }
}