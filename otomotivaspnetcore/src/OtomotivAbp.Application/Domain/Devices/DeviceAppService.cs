﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.Devices.Dtos;

namespace OtomotivAbp.Domain.Devices
{
    public class DeviceAppService : OtomotivAbpAppServiceBase, IDeviceAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IDeviceRepository _deviceRepository;

        public DeviceAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IDeviceRepository deviceRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _deviceRepository = deviceRepository;
        }

        [HttpPost]
        public async Task<DeviceFullOutPut> CreateAsync(CreateDeviceInput input)
        {
            var device = new Device()
            {
                Name = input.Name,
                DeviceGroupId = input.DeviceGroup.Id
            };

            await _deviceRepository.InsertAsync(device);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DeviceFullOutPut>(device);
        }

        [HttpPost]
        public async Task<DeviceFullOutPut> GetAsync(GetDeviceInput input)
        {
            var device = await _entityManager.GetDeviceAsync(input.Id);

            return ObjectMapper.Map<DeviceFullOutPut>(device);
        }

        [HttpPost]
        public async Task<List<DeviceFullOutPut>> GetListAsync()
        {
            var deviceList = await _deviceRepository.GetAllListAsync();

            return ObjectMapper.Map<List<DeviceFullOutPut>>(deviceList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteDeviceInput input)
        {
            var device = await _entityManager.GetDeviceAsync(input.Id);

            await _deviceRepository.DeleteAsync(device.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<DeviceFullOutPut> UpdateAsync(UpdateDeviceInput input)
        {
            var device = await _entityManager.GetDeviceAsync(input.Id);

            device.Name = input.Name;
            device.DeviceGroupId = input.DeviceGroup.Id;

            await _deviceRepository.UpdateAsync(device);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DeviceFullOutPut>(device);
        }
    }
}