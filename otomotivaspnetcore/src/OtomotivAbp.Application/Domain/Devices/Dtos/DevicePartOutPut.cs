﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.Devices.Dtos
{
    public class DevicePartOutPut:EntityDto<int>
    {
        public string Name { get; set; }
        public string No { get; set; }
        public bool Status { get; set; }
        public string IpAddress { get; set; }
    }
}