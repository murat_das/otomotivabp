﻿using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.DeviceGroups.Dtos;

namespace OtomotivAbp.Domain.Devices.Dtos
{
    public class UpdateDeviceInput : EntityDto<int>
    {
        public string Name { get; set; }
        public string No { get; set; }
        public bool Status { get; set; }
        public string IpAddress { get; set; }
        public DeviceGroupPartOutPut DeviceGroup { get; set; }
    }
}