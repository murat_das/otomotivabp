﻿using System.Security.AccessControl;
using OtomotivAbp.Domain.DeviceGroups.Dtos;

namespace OtomotivAbp.Domain.Devices.Dtos
{
    public class CreateDeviceInput
    {
        public string Name { get; set; }
        public string No { get; set; }
        public bool Status { get; set; }
        public string IpAddress { get; set; }
        public DeviceGroupPartOutPut DeviceGroup { get; set; }
    }
}