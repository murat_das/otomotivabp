﻿using OtomotivAbp.Domain.Products.Dtos;
using OtomotivAbp.Domain.Scraps.Dtos;

namespace OtomotivAbp.Domain.ProductScraps.Dtos
{
    public class CreateProductScrapInput
    {
        public ProductPartOutPut Product { get; set; }
        public ScrapPartOutPut Scrap { get; set; }
    }
}