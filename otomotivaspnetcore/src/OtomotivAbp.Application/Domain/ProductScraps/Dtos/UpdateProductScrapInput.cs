﻿using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.Products.Dtos;
using OtomotivAbp.Domain.Scraps.Dtos;

namespace OtomotivAbp.Domain.ProductScraps.Dtos
{
    public class UpdateProductScrapInput : EntityDto<int>
    {
        public ProductPartOutPut Product { get; set; }
        public ScrapPartOutPut Scrap { get; set; }
    }
}