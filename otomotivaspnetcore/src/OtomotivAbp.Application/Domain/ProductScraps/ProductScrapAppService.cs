﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.ProductScraps.Dtos;

namespace OtomotivAbp.Domain.ProductScraps
{
    public class ProductScrapAppService : OtomotivAbpAppServiceBase, IProductScrapAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IProductScrapRepository _productScrapRepository;

        public ProductScrapAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IProductScrapRepository productScrapRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _productScrapRepository = productScrapRepository;
        }

        [HttpPost]
        public async Task<ProductScrapFullOutPut> CreateAsync(CreateProductScrapInput input)
        {
            var productScrap = new ProductScrap()
            {
               ProductId = input.Product.Id,
               ScrapId = input.Scrap.Id
            };

            await _productScrapRepository.InsertAsync(productScrap);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductScrapFullOutPut>(productScrap);
        }

        [HttpPost]
        public async Task<ProductScrapFullOutPut> GetAsync(GetProductScrapInput input)
        {
            var productScrap = await _entityManager.GetProductScrapAsync(input.Id);

            return ObjectMapper.Map<ProductScrapFullOutPut>(productScrap);
        }

        [HttpPost]
        public async Task<List<ProductScrapFullOutPut>> GetListAsync()
        {
            var productScrapList = await _productScrapRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ProductScrapFullOutPut>>(productScrapList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteProductScrapInput input)
        {
            var productScrap = await _entityManager.GetProductScrapAsync(input.Id);

            await _productScrapRepository.DeleteAsync(productScrap.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<ProductScrapFullOutPut> UpdateAsync(UpdateProductScrapInput input)
        {
            var productScrap = await _entityManager.GetProductScrapAsync(input.Id);

            productScrap.ProductId = input.Product.Id;
            productScrap.ScrapId = input.Scrap.Id;

            await _productScrapRepository.UpdateAsync(productScrap);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductScrapFullOutPut>(productScrap);
        }
    }
}