﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.ProductScraps.Dtos;

namespace OtomotivAbp.Domain.ProductScraps
{
    public interface IProductScrapAppService : IApplicationService
    {
        #region Async Methods
        Task<ProductScrapFullOutPut> CreateAsync(CreateProductScrapInput input);
        Task<ProductScrapFullOutPut> GetAsync(GetProductScrapInput input);
        Task<List<ProductScrapFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteProductScrapInput input);
        Task<ProductScrapFullOutPut> UpdateAsync(UpdateProductScrapInput input);
        #endregion Async Methods
    }
}