﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.DailyShiftWorkOrders.Dtos;

namespace OtomotivAbp.Domain.DailyShiftWorkOrders
{
    public interface IDailyShiftWorkOrderAppService : IApplicationService
    {
        #region Async Methods
        Task<DailyShiftWorkOrderFullOutPut> CreateAsync(CreateDailyShiftWorkOrderInput input);
        Task<DailyShiftWorkOrderFullOutPut> GetAsync(GetDailyShiftWorkOrderInput input);
        Task<List<DailyShiftWorkOrderFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteDailyShiftWorkOrderInput input);
        Task<DailyShiftWorkOrderFullOutPut> UpdateAsync(UpdateDailyShiftWorkOrderInput input);
        #endregion Async Methods
    }
}