﻿using System;
using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.DailyShifts.Dtos;
using OtomotivAbp.Domain.WorkOrders.Dtos;

namespace OtomotivAbp.Domain.DailyShiftWorkOrders.Dtos
{
    public class UpdateDailyShiftWorkOrderInput : EntityDto<int>
    {
        public decimal Amount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DailyShiftPartOutPut DailyShift { get; set; }
        public WorkOrderPartOutPut WorkOrder { get; set; }
    }
}