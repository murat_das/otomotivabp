﻿using System;
using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.DailyShiftWorkOrders.Dtos
{
    public class DailyShiftWorkOrderPartOutPut : EntityDto<int>
    {
        public decimal Amount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}