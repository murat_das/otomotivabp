﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.DailyShiftWorkOrders.Dtos
{
    public class DeleteDailyShiftWorkOrderInput : EntityDto<int>
    {

    }
}