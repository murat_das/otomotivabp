﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.DailyShiftWorkOrders.Dtos;

namespace OtomotivAbp.Domain.DailyShiftWorkOrders
{
    public class DailyShiftWorkOrderAppService : OtomotivAbpAppServiceBase, IDailyShiftWorkOrderAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IDailyShiftWorkOrderRepository _dailyShiftWorkOrderRepository;

        public DailyShiftWorkOrderAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IDailyShiftWorkOrderRepository dailyShiftWorkOrderRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _dailyShiftWorkOrderRepository = dailyShiftWorkOrderRepository;
        }

        [HttpPost]
        public async Task<DailyShiftWorkOrderFullOutPut> CreateAsync(CreateDailyShiftWorkOrderInput input)
        {
            // ileride gözden geçirilecek geçiçi olarak yazıldı
            var dailyShiftWorkOrder = new DailyShiftWorkOrder()
            {
                DailyShiftId = input.DailyShift.Id,
                Amount = input.Amount,
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                WorkOrderId = input.WorkOrder.Id
            };

            await _dailyShiftWorkOrderRepository.InsertAsync(dailyShiftWorkOrder);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DailyShiftWorkOrderFullOutPut>(dailyShiftWorkOrder);
        }

        [HttpPost]
        public async Task<DailyShiftWorkOrderFullOutPut> GetAsync(GetDailyShiftWorkOrderInput input)
        {
            var dailyShiftWorkOrder = await _entityManager.GetDailyShiftWorkOrderAsync(input.Id);

            return ObjectMapper.Map<DailyShiftWorkOrderFullOutPut>(dailyShiftWorkOrder);
        }

        [HttpPost]
        public async Task<List<DailyShiftWorkOrderFullOutPut>> GetListAsync()
        {
            var dailyShiftWorkOrderList = await _dailyShiftWorkOrderRepository.GetAllListAsync();

            return ObjectMapper.Map<List<DailyShiftWorkOrderFullOutPut>>(dailyShiftWorkOrderList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteDailyShiftWorkOrderInput input)
        {
            var dailyShiftWorkOrder = await _entityManager.GetDailyShiftWorkOrderAsync(input.Id);

            await _dailyShiftWorkOrderRepository.DeleteAsync(dailyShiftWorkOrder.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<DailyShiftWorkOrderFullOutPut> UpdateAsync(UpdateDailyShiftWorkOrderInput input)
        {
            var dailyShiftWorkOrder = await _entityManager.GetDailyShiftWorkOrderAsync(input.Id);

            dailyShiftWorkOrder.DailyShiftId = input.DailyShift.Id;
            dailyShiftWorkOrder.Amount = input.Amount;
            dailyShiftWorkOrder.StartDate = input.StartDate;
            dailyShiftWorkOrder.EndDate = input.EndDate;
            dailyShiftWorkOrder.WorkOrderId = input.WorkOrder.Id;

            await _dailyShiftWorkOrderRepository.UpdateAsync(dailyShiftWorkOrder);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DailyShiftWorkOrderFullOutPut>(dailyShiftWorkOrder);
        }
    }
}