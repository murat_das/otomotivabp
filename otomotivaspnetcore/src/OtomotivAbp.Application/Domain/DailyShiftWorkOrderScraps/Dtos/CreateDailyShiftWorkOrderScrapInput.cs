﻿using OtomotivAbp.Domain.DailyShiftWorkOrders.Dtos;
using OtomotivAbp.Domain.Scraps.Dtos;

namespace OtomotivAbp.Domain.DailyShiftWorkOrderScraps.Dtos
{
    public class CreateDailyShiftWorkOrderScrapInput
    {
        public DailyShiftWorkOrderPartOutPut DailyShiftWorkOrder { get; set; }
        public ScrapPartOutPut Scrap { get; set; }
    }
}