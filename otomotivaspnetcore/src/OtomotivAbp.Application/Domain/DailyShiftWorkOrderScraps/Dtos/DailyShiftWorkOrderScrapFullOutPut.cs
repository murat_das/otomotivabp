﻿using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.DailyShiftWorkOrders.Dtos;
using OtomotivAbp.Domain.Scraps.Dtos;

namespace OtomotivAbp.Domain.DailyShiftWorkOrderScraps.Dtos
{
    public class DailyShiftWorkOrderScrapFullOutPut : EntityDto<int>
    {
        public DailyShiftWorkOrderPartOutPut DailyShiftWorkOrder { get; set; }
        public ScrapPartOutPut Scrap { get; set; }
    }
}