﻿using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.DailyShiftWorkOrders.Dtos;
using OtomotivAbp.Domain.Scraps.Dtos;

namespace OtomotivAbp.Domain.DailyShiftWorkOrderScraps.Dtos
{
    public class UpdateDailyShiftWorkOrderScrapInput : EntityDto<int>
    {
        public DailyShiftWorkOrderPartOutPut DailyShiftWorkOrder { get; set; }
        public ScrapPartOutPut Scrap { get; set; }
    }
}