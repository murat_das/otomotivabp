﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.DailyShiftWorkOrderScraps.Dtos;

namespace OtomotivAbp.Domain.DailyShiftWorkOrderScraps
{
    public interface IDailyShiftWorkOrderScrapAppService : IApplicationService
    {
        #region Async Methods
        Task<DailyShiftWorkOrderScrapFullOutPut> CreateAsync(CreateDailyShiftWorkOrderScrapInput input);
        Task<DailyShiftWorkOrderScrapFullOutPut> GetAsync(GetDailyShiftWorkOrderScrapInput input);
        Task<List<DailyShiftWorkOrderScrapFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteDailyShiftWorkOrderScrapInput input);
        Task<DailyShiftWorkOrderScrapFullOutPut> UpdateAsync(UpdateDailyShiftWorkOrderScrapInput input);
        #endregion Async Methods
    }
}