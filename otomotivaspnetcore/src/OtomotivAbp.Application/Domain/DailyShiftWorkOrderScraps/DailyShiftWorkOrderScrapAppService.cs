﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.DailyShiftWorkOrderScraps.Dtos;

namespace OtomotivAbp.Domain.DailyShiftWorkOrderScraps
{
    public class DailyShiftWorkOrderScrapAppService : OtomotivAbpAppServiceBase, IDailyShiftWorkOrderScrapAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IDailyShiftWorkOrderScrapRepository _dailyShiftWorkOrderScrapRepository;

        public DailyShiftWorkOrderScrapAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IDailyShiftWorkOrderScrapRepository dailyShiftWorkOrderScrapRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _dailyShiftWorkOrderScrapRepository = dailyShiftWorkOrderScrapRepository;
        }

        [HttpPost]
        public async Task<DailyShiftWorkOrderScrapFullOutPut> CreateAsync(CreateDailyShiftWorkOrderScrapInput input)
        {
            var dailyShiftWorkOrderScrap = new DailyShiftWorkOrderScrap()
            {
               DailyShiftWorkOrderId = input.DailyShiftWorkOrder.Id,
               ScrapId = input.Scrap.Id,
            };

            await _dailyShiftWorkOrderScrapRepository.InsertAsync(dailyShiftWorkOrderScrap);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DailyShiftWorkOrderScrapFullOutPut>(dailyShiftWorkOrderScrap);
        }

        [HttpPost]
        public async Task<DailyShiftWorkOrderScrapFullOutPut> GetAsync(GetDailyShiftWorkOrderScrapInput input)
        {
            var dailyShiftWorkOrderScrap = await _entityManager.GetDailyShiftWorkOrderScrapAsync(input.Id);

            return ObjectMapper.Map<DailyShiftWorkOrderScrapFullOutPut>(dailyShiftWorkOrderScrap);
        }

        [HttpPost]
        public async Task<List<DailyShiftWorkOrderScrapFullOutPut>> GetListAsync()
        {
            var dailyShiftWorkOrderScrapList = await _dailyShiftWorkOrderScrapRepository.GetAllListAsync();

            return ObjectMapper.Map<List<DailyShiftWorkOrderScrapFullOutPut>>(dailyShiftWorkOrderScrapList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteDailyShiftWorkOrderScrapInput input)
        {
            var dailyShiftWorkOrderScrap = await _entityManager.GetDailyShiftWorkOrderScrapAsync(input.Id);

            await _dailyShiftWorkOrderScrapRepository.DeleteAsync(dailyShiftWorkOrderScrap.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<DailyShiftWorkOrderScrapFullOutPut> UpdateAsync(UpdateDailyShiftWorkOrderScrapInput input)
        {
            var dailyShiftWorkOrderScrap = await _entityManager.GetDailyShiftWorkOrderScrapAsync(input.Id);

            dailyShiftWorkOrderScrap.DailyShiftWorkOrderId = input.DailyShiftWorkOrder.Id;
            dailyShiftWorkOrderScrap.ScrapId = input.Scrap.Id;

            await _dailyShiftWorkOrderScrapRepository.UpdateAsync(dailyShiftWorkOrderScrap);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DailyShiftWorkOrderScrapFullOutPut>(dailyShiftWorkOrderScrap);
        }
    }
}