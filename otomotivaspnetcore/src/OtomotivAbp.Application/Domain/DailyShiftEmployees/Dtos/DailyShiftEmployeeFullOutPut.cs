﻿using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.DailyShifts.Dtos;
using OtomotivAbp.Users.Dto;

namespace OtomotivAbp.Domain.DailyShiftEmployees.Dtos
{
    public class DailyShiftEmployeeFullOutPut : EntityDto<int>
    {
        public DailyShiftPartOutPut DailySift { get; set; }
        public UserPartOutPut User { get; set; }
    }
}