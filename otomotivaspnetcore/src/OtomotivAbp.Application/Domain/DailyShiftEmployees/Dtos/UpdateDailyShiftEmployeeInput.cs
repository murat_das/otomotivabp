﻿using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.DailyShifts.Dtos;
using OtomotivAbp.Users.Dto;

namespace OtomotivAbp.Domain.DailyShiftEmployees.Dtos
{
    public class UpdateDailyShiftEmployeeInput : EntityDto<int>
    {
        public DailyShiftPartOutPut DailyShift { get; set; }
        public UserPartOutPut User { get; set; }
    }
}