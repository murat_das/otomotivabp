﻿using OtomotivAbp.Domain.DailyShifts.Dtos;
using OtomotivAbp.Users.Dto;

namespace OtomotivAbp.Domain.DailyShiftEmployees.Dtos
{
    public class CreateDailyShiftEmployeeInput
    {
        public UserPartOutPut User { get; set; }
        public DailyShiftPartOutPut DailyShift { get; set; }
    }
}