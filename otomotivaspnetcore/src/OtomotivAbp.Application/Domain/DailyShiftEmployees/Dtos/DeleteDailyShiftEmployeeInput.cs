﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.DailyShiftEmployees.Dtos
{
    public class DeleteDailyShiftEmployeeInput : EntityDto<int>
    {

    }
}