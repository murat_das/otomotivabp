﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.DailyShiftEmployees.Dtos;

namespace OtomotivAbp.Domain.DailyShiftEmployees
{
    public class DailyShiftEmployeeAppService : OtomotivAbpAppServiceBase, IDailyShiftEmployeeAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IDailyShiftEmployeeRepository _dailyShiftEmployeeRepository;

        public DailyShiftEmployeeAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IDailyShiftEmployeeRepository dailyShiftEmployeeRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _dailyShiftEmployeeRepository = dailyShiftEmployeeRepository;
        }

        [HttpPost]
        public async Task<DailyShiftEmployeeFullOutPut> CreateAsync(CreateDailyShiftEmployeeInput input)
        {
            var dailyShiftEmployee = new DailyShiftEmployee()
            {
                DailyShiftId = input.DailyShift.Id,
                UserId = input.User.Id
            };

            await _dailyShiftEmployeeRepository.InsertAsync(dailyShiftEmployee);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DailyShiftEmployeeFullOutPut>(dailyShiftEmployee);
        }

        [HttpPost]
        public async Task<DailyShiftEmployeeFullOutPut> GetAsync(GetDailyShiftEmployeeInput input)
        {
            var dailyShiftEmployee = await _entityManager.GetDailyShiftEmployeeAsync(input.Id);

            return ObjectMapper.Map<DailyShiftEmployeeFullOutPut>(dailyShiftEmployee);
        }

        [HttpPost]
        public async Task<List<DailyShiftEmployeeFullOutPut>> GetListAsync()
        {
            var dailyShiftEmployeeList = await _dailyShiftEmployeeRepository.GetAllListAsync();

            return ObjectMapper.Map<List<DailyShiftEmployeeFullOutPut>>(dailyShiftEmployeeList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteDailyShiftEmployeeInput input)
        {
            var dailyShiftEmployee = await _entityManager.GetDailyShiftEmployeeAsync(input.Id);

            await _dailyShiftEmployeeRepository.DeleteAsync(dailyShiftEmployee.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<DailyShiftEmployeeFullOutPut> UpdateAsync(UpdateDailyShiftEmployeeInput input)
        {
            var dailyShiftEmployee = await _entityManager.GetDailyShiftEmployeeAsync(input.Id);

            dailyShiftEmployee.DailyShiftId = input.DailyShift.Id;
            dailyShiftEmployee.UserId = input.User.Id;

            await _dailyShiftEmployeeRepository.UpdateAsync(dailyShiftEmployee);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DailyShiftEmployeeFullOutPut>(dailyShiftEmployee);
        }
    }
}