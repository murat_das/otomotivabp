﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.DailyShiftEmployees.Dtos;

namespace OtomotivAbp.Domain.DailyShiftEmployees
{
    public interface IDailyShiftEmployeeAppService : IApplicationService
    {
        #region Async Methods
        Task<DailyShiftEmployeeFullOutPut> CreateAsync(CreateDailyShiftEmployeeInput input);
        Task<DailyShiftEmployeeFullOutPut> GetAsync(GetDailyShiftEmployeeInput input);
        Task<List<DailyShiftEmployeeFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteDailyShiftEmployeeInput input);
        Task<DailyShiftEmployeeFullOutPut> UpdateAsync(UpdateDailyShiftEmployeeInput input);
        #endregion Async Methods
    }
}