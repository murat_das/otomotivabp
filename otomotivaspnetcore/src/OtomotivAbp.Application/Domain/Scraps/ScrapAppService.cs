﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.Scraps.Dtos;

namespace OtomotivAbp.Domain.Scraps
{
    public class ScrapAppService : OtomotivAbpAppServiceBase, IScrapAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IScrapRepository _scrapRepository;

        public ScrapAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IScrapRepository scrapRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _scrapRepository = scrapRepository;
        }

        [HttpPost]
        public async Task<ScrapFullOutPut> CreateAsync(CreateScrapInput input)
        {
            var scrap = new Scrap()
            {
                Name = input.Name,
                ScrapTypeId = input.ScrapType.Id
            };

            await _scrapRepository.InsertAsync(scrap);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ScrapFullOutPut>(scrap);
        }

        [HttpPost]
        public async Task<ScrapFullOutPut> GetAsync(GetScrapInput input)
        {
            var scrap = await _entityManager.GetScrapAsync(input.Id);

            return ObjectMapper.Map<ScrapFullOutPut>(scrap);
        }

        [HttpPost]
        public async Task<List<ScrapFullOutPut>> GetListAsync()
        {
            var scrapList = await _scrapRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ScrapFullOutPut>>(scrapList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteScrapInput input)
        {
            var scrap = await _entityManager.GetScrapAsync(input.Id);

            await _scrapRepository.DeleteAsync(scrap.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<ScrapFullOutPut> UpdateAsync(UpdateScrapInput input)
        {
            var scrap = await _entityManager.GetScrapAsync(input.Id);

            scrap.Name = input.Name;
            scrap.ScrapTypeId = input.ScrapType.Id;

            await _scrapRepository.UpdateAsync(scrap);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ScrapFullOutPut>(scrap);
        }
    }
}