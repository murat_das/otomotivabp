﻿using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.ScrapTypes.Dtos;

namespace OtomotivAbp.Domain.Scraps.Dtos
{
    public class UpdateScrapInput : EntityDto<int>
    {
        public string Name { get; set; }
        public ScrapTypePartOutPut ScrapType { get; set; }
    }
}