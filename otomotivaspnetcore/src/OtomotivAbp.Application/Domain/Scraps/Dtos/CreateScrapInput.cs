﻿using OtomotivAbp.Domain.ScrapTypes.Dtos;

namespace OtomotivAbp.Domain.Scraps.Dtos
{
    public class CreateScrapInput
    {
        public string Name { get; set; }
        public ScrapTypePartOutPut ScrapType { get; set; }
    }
}