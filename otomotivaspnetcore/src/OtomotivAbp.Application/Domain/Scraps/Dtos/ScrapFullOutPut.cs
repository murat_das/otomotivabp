﻿using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.ScrapTypes.Dtos;

namespace OtomotivAbp.Domain.Scraps.Dtos
{
    public class ScrapFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public ScrapTypePartOutPut ScrapType { get; set; }
    }
}