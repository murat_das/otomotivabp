﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.ProductRawMaterials.Dtos;

namespace OtomotivAbp.Domain.ProductRawMaterials
{
    public interface IProductRawMaterialAppService : IApplicationService
    {
        #region Async Methods
        Task<ProductRawMaterialFullOutPut> CreateAsync(CreateProductRawMaterialInput input);
        Task<ProductRawMaterialFullOutPut> GetAsync(GetProductRawMaterialInput input);
        Task<List<ProductRawMaterialFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteProductRawMaterialInput input);
        Task<ProductRawMaterialFullOutPut> UpdateAsync(UpdateProductRawMaterialInput input);
        #endregion Async Methods
    }
}