﻿using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.Products.Dtos;
using OtomotivAbp.Domain.RawMaterials.Dtos;

namespace OtomotivAbp.Domain.ProductRawMaterials.Dtos
{
    public class UpdateProductRawMaterialInput : EntityDto<int>
    {
        public ProductPartOutPut Product { get; set; }
        public RawMaterialPartOutPut RawMaterial { get; set; }
    }
}