﻿using OtomotivAbp.Domain.Products.Dtos;
using OtomotivAbp.Domain.RawMaterials.Dtos;

namespace OtomotivAbp.Domain.ProductRawMaterials.Dtos
{
    public class CreateProductRawMaterialInput
    {
        public ProductPartOutPut Product { get; set; }
        public RawMaterialPartOutPut RawMaterial { get; set; }
    }
}