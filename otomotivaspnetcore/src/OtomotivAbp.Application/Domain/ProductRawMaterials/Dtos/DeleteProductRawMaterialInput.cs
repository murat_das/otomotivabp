﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.ProductRawMaterials.Dtos
{
    public class DeleteProductRawMaterialInput : EntityDto<int>
    {

    }
}