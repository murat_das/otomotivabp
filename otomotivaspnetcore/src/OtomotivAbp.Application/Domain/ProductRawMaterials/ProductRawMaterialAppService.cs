﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.ProductRawMaterials.Dtos;

namespace OtomotivAbp.Domain.ProductRawMaterials
{
    public class ProductRawMaterialAppService : OtomotivAbpAppServiceBase, IProductRawMaterialAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IProductRawMaterialRepository _productRawMaterialRepository;

        public ProductRawMaterialAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IProductRawMaterialRepository productRawMaterialRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _productRawMaterialRepository = productRawMaterialRepository;
        }

        [HttpPost]
        public async Task<ProductRawMaterialFullOutPut> CreateAsync(CreateProductRawMaterialInput input)
        {
            var productRawMaterial = new ProductRawMaterial()
            {
               ProductId = input.Product.Id,
               RawMaterialId = input.RawMaterial.Id,
            };

            await _productRawMaterialRepository.InsertAsync(productRawMaterial);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductRawMaterialFullOutPut>(productRawMaterial);
        }

        [HttpPost]
        public async Task<ProductRawMaterialFullOutPut> GetAsync(GetProductRawMaterialInput input)
        {
            var productRawMaterial = await _entityManager.GetProductRawMaterialAsync(input.Id);

            return ObjectMapper.Map<ProductRawMaterialFullOutPut>(productRawMaterial);
        }

        [HttpPost]
        public async Task<List<ProductRawMaterialFullOutPut>> GetListAsync()
        {
            var productRawMaterialList = await _productRawMaterialRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ProductRawMaterialFullOutPut>>(productRawMaterialList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteProductRawMaterialInput input)
        {
            var productRawMaterial = await _entityManager.GetProductRawMaterialAsync(input.Id);

            await _productRawMaterialRepository.DeleteAsync(productRawMaterial.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<ProductRawMaterialFullOutPut> UpdateAsync(UpdateProductRawMaterialInput input)
        {
            var productRawMaterial = await _entityManager.GetProductRawMaterialAsync(input.Id);

            productRawMaterial.ProductId = input.Product.Id;
            productRawMaterial.RawMaterialId= input.RawMaterial.Id;

            await _productRawMaterialRepository.UpdateAsync(productRawMaterial);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductRawMaterialFullOutPut>(productRawMaterial);
        }
    }
}