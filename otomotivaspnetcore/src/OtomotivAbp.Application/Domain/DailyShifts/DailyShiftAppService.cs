﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.DailyShifts.Dtos;

namespace OtomotivAbp.Domain.DailyShifts
{
    public class DailyShiftAppService : OtomotivAbpAppServiceBase, IDailyShiftAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IDailyShiftRepository _dailyShiftRepository;

        public DailyShiftAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IDailyShiftRepository dailyShiftRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _dailyShiftRepository = dailyShiftRepository;
        }

        [HttpPost]
        public async Task<DailyShiftFullOutPut> CreateAsync(CreateDailyShiftInput input)
        {
            var dailyShift = new DailyShift()
            {
               DeviceId = input.Device.Id,
               ShiftId = input.Shift.Id,
            };

            await _dailyShiftRepository.InsertAsync(dailyShift);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DailyShiftFullOutPut>(dailyShift);
        }

        [HttpPost]
        public async Task<DailyShiftFullOutPut> GetAsync(GetDailyShiftInput input)
        {
            var dailyShift = await _entityManager.GetDailyShiftAsync(input.Id);

            return ObjectMapper.Map<DailyShiftFullOutPut>(dailyShift);
        }

        [HttpPost]
        public async Task<List<DailyShiftFullOutPut>> GetListAsync()
        {
            var dailyShiftList = await _dailyShiftRepository.GetAllListAsync();

            return ObjectMapper.Map<List<DailyShiftFullOutPut>>(dailyShiftList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteDailyShiftInput input)
        {
            var dailyShift = await _entityManager.GetDailyShiftAsync(input.Id);

            await _dailyShiftRepository.DeleteAsync(dailyShift.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<DailyShiftFullOutPut> UpdateAsync(UpdateDailyShiftInput input)
        {
            var dailyShift = await _entityManager.GetDailyShiftAsync(input.Id);

            dailyShift.DeviceId = input.Device.Id;
            dailyShift.ShiftId = input.Shift.Id;

            await _dailyShiftRepository.UpdateAsync(dailyShift);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DailyShiftFullOutPut>(dailyShift);
        }
    }
}