﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.DailyShifts.Dtos;

namespace OtomotivAbp.Domain.DailyShifts
{
    public interface IDailyShiftAppService : IApplicationService
    {
        #region Async Methods
        Task<DailyShiftFullOutPut> CreateAsync(CreateDailyShiftInput input);
        Task<DailyShiftFullOutPut> GetAsync(GetDailyShiftInput input);
        Task<List<DailyShiftFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteDailyShiftInput input);
        Task<DailyShiftFullOutPut> UpdateAsync(UpdateDailyShiftInput input);
        #endregion Async Methods
    }
}