﻿using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.Devices.Dtos;
using OtomotivAbp.Domain.Shifts.Dtos;

namespace OtomotivAbp.Domain.DailyShifts.Dtos
{
    public class DailyShiftPartOutPut:EntityDto<int>
    {
        public DevicePartOutPut Device { get; set; }
        public ShiftPartOutPut Shift { get; set; }
    }
}