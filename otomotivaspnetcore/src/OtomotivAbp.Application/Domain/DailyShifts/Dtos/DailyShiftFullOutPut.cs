﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.DailyShiftEmployees.Dtos;
using OtomotivAbp.Domain.DailyShiftWorkOrders.Dtos;
using OtomotivAbp.Domain.Devices.Dtos;
using OtomotivAbp.Domain.Shifts.Dtos;

namespace OtomotivAbp.Domain.DailyShifts.Dtos
{
    public class DailyShiftFullOutPut:EntityDto<int>
    {
        public ShiftPartOutPut Shift { get; set; }
        public DevicePartOutPut Device { get; set; }

        public List<DailyShiftEmployeePartOutPut> DailyShiftEmployees { get; set; }
        public List<DailyShiftWorkOrderPartOutPut> DailyShiftWorkOrders { get; set; }
    }
}