﻿using OtomotivAbp.Domain.Devices.Dtos;
using OtomotivAbp.Domain.Shifts.Dtos;

namespace OtomotivAbp.Domain.DailyShifts.Dtos
{
    public class CreateDailyShiftInput
    {
        public ShiftPartOutPut Shift { get; set; }
        public DevicePartOutPut Device { get; set; }
    }
}