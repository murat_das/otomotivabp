﻿using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.DailyShiftWorkOrders.Dtos;
using OtomotivAbp.Domain.Stops.Dtos;

namespace OtomotivAbp.Domain.DailyShiftWorkOrderStops.Dtos
{
    public class DailyShiftWorkOrderStopFullOutPut : EntityDto<int>
    {
        public DailyShiftWorkOrderPartOutPut DailyShiftWorkOrder { get; set; }
        public StopPartOutPut Stop { get; set; }

    }
}