﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.DailyShiftWorkOrderStops.Dtos
{
    public class DeleteDailyShiftWorkOrderStopInput:EntityDto<int>
    {
        
    }
}