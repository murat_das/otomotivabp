﻿using OtomotivAbp.Domain.DailyShiftWorkOrders.Dtos;
using OtomotivAbp.Domain.Stops.Dtos;

namespace OtomotivAbp.Domain.DailyShiftWorkOrderStops.Dtos
{
    public class CreateDailyShiftWorkOrderStopInput
    {
        public DailyShiftWorkOrderPartOutPut DailyShiftWorkOrder { get; set; }
        public StopPartOutPut Stop { get; set; }

    }
}