﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.DailyShiftWorkOrderStops.Dtos;

namespace OtomotivAbp.Domain.DailyShiftWorkOrderStops
{
    public interface IDailyShiftWorkOrderStopAppService : IApplicationService
    {
        #region Async Methods
        Task<DailyShiftWorkOrderStopFullOutPut> CreateAsync(CreateDailyShiftWorkOrderStopInput input);
        Task<DailyShiftWorkOrderStopFullOutPut> GetAsync(GetDailyShiftWorkOrderStopInput input);
        Task<List<DailyShiftWorkOrderStopFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteDailyShiftWorkOrderStopInput input);
        Task<DailyShiftWorkOrderStopFullOutPut> UpdateAsync(UpdateDailyShiftWorkOrderStopInput input);
        #endregion Async Methods
    }
}