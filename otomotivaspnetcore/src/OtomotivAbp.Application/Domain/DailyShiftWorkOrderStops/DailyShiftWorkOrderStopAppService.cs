﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.DailyShiftWorkOrderStops.Dtos;

namespace OtomotivAbp.Domain.DailyShiftWorkOrderStops
{
    public class DailyShiftWorkOrderStopAppService : OtomotivAbpAppServiceBase, IDailyShiftWorkOrderStopAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IDailyShiftWorkOrderStopRepository _dailyShiftWorkOrderStopRepository;

        public DailyShiftWorkOrderStopAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IDailyShiftWorkOrderStopRepository dailyShiftWorkOrderStopRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _dailyShiftWorkOrderStopRepository = dailyShiftWorkOrderStopRepository;
        }

        [HttpPost]
        public async Task<DailyShiftWorkOrderStopFullOutPut> CreateAsync(CreateDailyShiftWorkOrderStopInput input)
        {
            var dailyShiftWorkOrderStop = new DailyShiftWorkOrderStop()
            {
                DailyShiftWorkOrderId = input.DailyShiftWorkOrder.Id,
                StopId = input.Stop.Id,
            };

            await _dailyShiftWorkOrderStopRepository.InsertAsync(dailyShiftWorkOrderStop);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DailyShiftWorkOrderStopFullOutPut>(dailyShiftWorkOrderStop);
        }

        [HttpPost]
        public async Task<DailyShiftWorkOrderStopFullOutPut> GetAsync(GetDailyShiftWorkOrderStopInput input)
        {
            var dailyShiftWorkOrderStop = await _entityManager.GetDailyShiftWorkOrderStopAsync(input.Id);

            return ObjectMapper.Map<DailyShiftWorkOrderStopFullOutPut>(dailyShiftWorkOrderStop);
        }

        [HttpPost]
        public async Task<List<DailyShiftWorkOrderStopFullOutPut>> GetListAsync()
        {
            var dailyShiftWorkOrderStopList = await _dailyShiftWorkOrderStopRepository.GetAllListAsync();

            return ObjectMapper.Map<List<DailyShiftWorkOrderStopFullOutPut>>(dailyShiftWorkOrderStopList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteDailyShiftWorkOrderStopInput input)
        {
            var dailyShiftWorkOrderStop = await _entityManager.GetDailyShiftWorkOrderStopAsync(input.Id);

            await _dailyShiftWorkOrderStopRepository.DeleteAsync(dailyShiftWorkOrderStop.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<DailyShiftWorkOrderStopFullOutPut> UpdateAsync(UpdateDailyShiftWorkOrderStopInput input)
        {
            var dailyShiftWorkOrderStop = await _entityManager.GetDailyShiftWorkOrderStopAsync(input.Id);

            dailyShiftWorkOrderStop.DailyShiftWorkOrderId = input.DailyShiftWorkOrder.Id;
            dailyShiftWorkOrderStop.StopId = input.Stop.Id;

            await _dailyShiftWorkOrderStopRepository.UpdateAsync(dailyShiftWorkOrderStop);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DailyShiftWorkOrderStopFullOutPut>(dailyShiftWorkOrderStop);
        }
    }
}