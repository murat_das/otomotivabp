﻿namespace OtomotivAbp.Domain.Products.Dtos
{
    public class CreateProductInput
    {
        public string Name { get; set; }
        public string Barcode { get; set; }
        public string StockCode { get; set; }
    }
}