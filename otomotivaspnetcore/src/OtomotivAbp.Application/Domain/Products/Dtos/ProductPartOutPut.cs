﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.Products.Dtos
{
    public class ProductPartOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public string Barcode { get; set; }
        public string StockCode { get; set; }
    }
}