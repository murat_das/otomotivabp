﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.Products.Dtos;

namespace OtomotivAbp.Domain.Products
{
    public class ProductAppService : OtomotivAbpAppServiceBase, IProductAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IProductRepository _productRepository;

        public ProductAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IProductRepository productRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _productRepository = productRepository;
        }

        [HttpPost]
        public async Task<ProductFullOutPut> CreateAsync(CreateProductInput input)
        {
            var product = new Product()
            {
                Name = input.Name,
                Barcode = input.Barcode,
                StockCode = input.StockCode,
            };

            await _productRepository.InsertAsync(product);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductFullOutPut>(product);
        }

        [HttpPost]
        public async Task<ProductFullOutPut> GetAsync(GetProductInput input)
        {
            var product = await _entityManager.GetProductAsync(input.Id);

            return ObjectMapper.Map<ProductFullOutPut>(product);
        }

        [HttpPost]
        public async Task<List<ProductFullOutPut>> GetListAsync()
        {
            var productList = await _productRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ProductFullOutPut>>(productList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteProductInput input)
        {
            var product = await _entityManager.GetProductAsync(input.Id);

            await _productRepository.DeleteAsync(product.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<ProductFullOutPut> UpdateAsync(UpdateProductInput input)
        {
            var product = await _entityManager.GetProductAsync(input.Id);

            product.Name = input.Name;
            product.Barcode = input.StockCode;
            product.StockCode = input.StockCode;

            await _productRepository.UpdateAsync(product);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ProductFullOutPut>(product);
        }
    }
}