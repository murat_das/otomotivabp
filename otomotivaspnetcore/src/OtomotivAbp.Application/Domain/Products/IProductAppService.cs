﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.Products.Dtos;

namespace OtomotivAbp.Domain.Products
{
    public interface IProductAppService : IApplicationService
    {
        #region Async Methods
        Task<ProductFullOutPut> CreateAsync(CreateProductInput input);
        Task<ProductFullOutPut> GetAsync(GetProductInput input);
        Task<List<ProductFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteProductInput input);
        Task<ProductFullOutPut> UpdateAsync(UpdateProductInput input);
        #endregion Async Methods
    }
}