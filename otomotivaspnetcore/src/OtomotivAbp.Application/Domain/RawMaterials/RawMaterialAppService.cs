﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.RawMaterials.Dtos;

namespace OtomotivAbp.Domain.RawMaterials
{
    public class RawMaterialAppService : OtomotivAbpAppServiceBase, IRawMaterialAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IRawMaterialRepository _rawMaterialRepository;

        public RawMaterialAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IRawMaterialRepository rawMaterialRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _rawMaterialRepository = rawMaterialRepository;
        }

        [HttpPost]
        public async Task<RawMaterialFullOutPut> CreateAsync(CreateRawMaterialInput input)
        {
            var rawMaterial = new RawMaterial()
            {
                Name = input.Name,
                Barcode = input.Barcode
            };

            await _rawMaterialRepository.InsertAsync(rawMaterial);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<RawMaterialFullOutPut>(rawMaterial);
        }

        [HttpPost]
        public async Task<RawMaterialFullOutPut> GetAsync(GetRawMaterialInput input)
        {
            var rawMaterial = await _entityManager.GetRawMaterialAsync(input.Id);

            return ObjectMapper.Map<RawMaterialFullOutPut>(rawMaterial);
        }

        [HttpPost]
        public async Task<List<RawMaterialFullOutPut>> GetListAsync()
        {
            var rawMaterialList = await _rawMaterialRepository.GetAllListAsync();

            return ObjectMapper.Map<List<RawMaterialFullOutPut>>(rawMaterialList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteRawMaterialInput input)
        {
            var rawMaterial = await _entityManager.GetRawMaterialAsync(input.Id);

            await _rawMaterialRepository.DeleteAsync(rawMaterial.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<RawMaterialFullOutPut> UpdateAsync(UpdateRawMaterialInput input)
        {
            var rawMaterial = await _entityManager.GetRawMaterialAsync(input.Id);

            rawMaterial.Name = input.Name;
            rawMaterial.Barcode = input.Barcode;

            await _rawMaterialRepository.UpdateAsync(rawMaterial);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<RawMaterialFullOutPut>(rawMaterial);
        }
    }
}