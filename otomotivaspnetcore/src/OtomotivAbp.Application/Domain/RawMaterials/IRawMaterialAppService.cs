﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.RawMaterials.Dtos;

namespace OtomotivAbp.Domain.RawMaterials
{
    public interface IRawMaterialAppService : IApplicationService
    {
        #region Async Methods
        Task<RawMaterialFullOutPut> CreateAsync(CreateRawMaterialInput input);
        Task<RawMaterialFullOutPut> GetAsync(GetRawMaterialInput input);
        Task<List<RawMaterialFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteRawMaterialInput input);
        Task<RawMaterialFullOutPut> UpdateAsync(UpdateRawMaterialInput input);
        #endregion Async Methods
    }
}