﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.RawMaterials.Dtos
{
    public class RawMaterialFullOutPut:EntityDto<int>
    {
        public string Name { get; set; }
        public string Barcode { get; set; }
    }
}