﻿namespace OtomotivAbp.Domain.RawMaterials.Dtos
{
    public class CreateRawMaterialInput
    {
        public string Name { get; set; }
        public string Barcode { get; set; }

    }
}