﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.DeviceGroups.Dtos;

namespace OtomotivAbp.Domain.DeviceGroups
{
    public class DeviceGroupAppService : OtomotivAbpAppServiceBase, IDeviceGroupAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IDeviceGroupRepository _deviceGroupRepository;

        public DeviceGroupAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IDeviceGroupRepository deviceGroupRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _deviceGroupRepository = deviceGroupRepository;
        }

        [HttpPost]
        public async Task<DeviceGroupFullOutPut> CreateAsync(CreateDeviceGroupInput input)
        {
            var deviceGroup = new DeviceGroup()
            {
                Name = input.Name,
                DepartmentId = input.Department.Id
            };

            await _deviceGroupRepository.InsertAsync(deviceGroup);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DeviceGroupFullOutPut>(deviceGroup);
        }

        [HttpPost]
        public async Task<DeviceGroupFullOutPut> GetAsync(GetDeviceGroupInput input)
        {
            var deviceGroup = await _entityManager.GetDeviceGroupAsync(input.Id);

            return ObjectMapper.Map<DeviceGroupFullOutPut>(deviceGroup);
        }

        [HttpPost]
        public async Task<List<DeviceGroupFullOutPut>> GetListAsync()
        {
            var deviceGroupList = await _deviceGroupRepository.GetAllListAsync();

            return ObjectMapper.Map<List<DeviceGroupFullOutPut>>(deviceGroupList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteDeviceGroupInput input)
        {
            var deviceGroup = await _entityManager.GetDeviceGroupAsync(input.Id);

            await _deviceGroupRepository.DeleteAsync(deviceGroup.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<DeviceGroupFullOutPut> UpdateAsync(UpdateDeviceGroupInput input)
        {
            var deviceGroup = await _entityManager.GetDeviceGroupAsync(input.Id);

            deviceGroup.Name = input.Name;
            deviceGroup.DepartmentId = input.Department.Id;

            await _deviceGroupRepository.UpdateAsync(deviceGroup);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DeviceGroupFullOutPut>(deviceGroup);
        }
    }
}