﻿using OtomotivAbp.Domain.Departments.Dtos;

namespace OtomotivAbp.Domain.DeviceGroups.Dtos
{
    public class CreateDeviceGroupInput
    {
        public string Name { get; set; }
        public string IpAddress { get; set; }
        public DepartmentPartOutPut Department { get; set; }
    }
}