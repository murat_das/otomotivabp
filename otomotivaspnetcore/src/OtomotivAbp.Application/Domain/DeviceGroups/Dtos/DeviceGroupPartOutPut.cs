﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.DeviceGroups.Dtos
{
    public class DeviceGroupPartOutPut:EntityDto<int>
    {
        public string Name { get; set; }
        public string IpAddress { get; set; }
    }
}