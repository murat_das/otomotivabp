﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.Departments.Dtos;
using OtomotivAbp.Domain.Devices.Dtos;

namespace OtomotivAbp.Domain.DeviceGroups.Dtos
{
    public class DeviceGroupFullOutPut:EntityDto<int>
    {
        public string Name { get; set; }
        public string IpAddress { get; set; }
        public DepartmentPartOutPut Department { get; set; }

        public List<DevicePartOutPut> Devices { get; set; }
    }
}