﻿using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.Departments.Dtos;

namespace OtomotivAbp.Domain.DeviceGroups.Dtos
{
    public class UpdateDeviceGroupInput:EntityDto<int>
    {
        public string Name { get; set; }
        public string IpAddress { get; set; }
        public DepartmentPartOutPut Department { get; set; }
    }
}