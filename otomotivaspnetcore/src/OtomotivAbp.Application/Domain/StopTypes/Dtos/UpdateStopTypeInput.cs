﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.StopTypes.Dtos
{
    public class UpdateStopTypeInput : EntityDto<int>
    {
        public string Name { get; set; }

    }
}