﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.StopTypes.Dtos
{
    public class StopTypeFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }

    }
}