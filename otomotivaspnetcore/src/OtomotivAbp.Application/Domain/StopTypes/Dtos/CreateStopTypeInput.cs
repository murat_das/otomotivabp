﻿namespace OtomotivAbp.Domain.StopTypes.Dtos
{
    public class CreateStopTypeInput
    {
        public string Name { get; set; }
    }
}