﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.StopTypes.Dtos;

namespace OtomotivAbp.Domain.StopTypes
{
    public class StopTypeAppService : OtomotivAbpAppServiceBase, IStopTypeAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IStopTypeRepository _stopTypeRepository;

        public StopTypeAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IStopTypeRepository stopTypeRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _stopTypeRepository = stopTypeRepository;
        }

        [HttpPost]
        public async Task<StopTypeFullOutPut> CreateAsync(CreateStopTypeInput input)
        {
            var stopType = new StopType()
            {
                Name = input.Name
            };

            await _stopTypeRepository.InsertAsync(stopType);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<StopTypeFullOutPut>(stopType);
        }

        [HttpPost]
        public async Task<StopTypeFullOutPut> GetAsync(GetStopTypeInput input)
        {
            var stopType = await _entityManager.GetStopTypeAsync(input.Id);

            return ObjectMapper.Map<StopTypeFullOutPut>(stopType);
        }

        [HttpPost]
        public async Task<List<StopTypeFullOutPut>> GetListAsync()
        {
            var stopTypeList = await _stopTypeRepository.GetAllListAsync();

            return ObjectMapper.Map<List<StopTypeFullOutPut>>(stopTypeList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteStopTypeInput input)
        {
            var stopType = await _entityManager.GetStopTypeAsync(input.Id);

            await _stopTypeRepository.DeleteAsync(stopType.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<StopTypeFullOutPut> UpdateAsync(UpdateStopTypeInput input)
        {
            var stopType = await _entityManager.GetStopTypeAsync(input.Id);

            stopType.Name = input.Name;

            await _stopTypeRepository.UpdateAsync(stopType);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<StopTypeFullOutPut>(stopType);
        }
    }
}