﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.StopTypes.Dtos;

namespace OtomotivAbp.Domain.StopTypes
{
    public interface IStopTypeAppService : IApplicationService
    {
        #region Async Methods
        Task<StopTypeFullOutPut> CreateAsync(CreateStopTypeInput input);
        Task<StopTypeFullOutPut> GetAsync(GetStopTypeInput input);
        Task<List<StopTypeFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteStopTypeInput input);
        Task<StopTypeFullOutPut> UpdateAsync(UpdateStopTypeInput input);
        #endregion Async Methods
    }
}