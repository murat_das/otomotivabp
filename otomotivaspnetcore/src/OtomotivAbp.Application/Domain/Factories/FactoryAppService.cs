﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.Factories.Dtos;

namespace OtomotivAbp.Domain.Factories
{
    public class FactoryAppService : OtomotivAbpAppServiceBase, IFactoryAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IFactoryRepository _factoryRepository;

        public FactoryAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IFactoryRepository factoryRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _factoryRepository = factoryRepository;
        }

        [HttpPost]
        public async Task<FactoryFullOutPut> CreateAsync(CreateFactoryInput input)
        {
            var factory = new Factory()
            {
                Name = input.Name
            };

            await _factoryRepository.InsertAsync(factory);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<FactoryFullOutPut>(factory);
        }

        [HttpPost]
        public async Task<FactoryFullOutPut> GetAsync(GetFactoryInput input)
        {
            var factory = await _entityManager.GetFactoryAsync(input.Id);

            return ObjectMapper.Map<FactoryFullOutPut>(factory);
        }

        [HttpPost]
        public async Task<List<FactoryFullOutPut>> GetListAsync()
        {
            var factoryList = await _factoryRepository.GetAllListAsync();

            return ObjectMapper.Map<List<FactoryFullOutPut>>(factoryList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteFactoryInput input)
        {
            var factory = await _entityManager.GetFactoryAsync(input.Id);

            await _factoryRepository.DeleteAsync(factory.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<FactoryFullOutPut> UpdateAsync(UpdateFactoryInput input)
        {
            var factory = await _entityManager.GetFactoryAsync(input.Id);

            factory.Name = input.Name;

            await _factoryRepository.UpdateAsync(factory);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<FactoryFullOutPut>(factory);
        }
    }
}