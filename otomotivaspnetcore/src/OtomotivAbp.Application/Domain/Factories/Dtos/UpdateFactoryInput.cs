﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Domain.Factories.Dtos
{
    public class UpdateFactoryInput : EntityDto<int>
    {
        public string Name { get; set; }
    }
}