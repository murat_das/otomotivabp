﻿namespace OtomotivAbp.Domain.Factories.Dtos
{
    public class CreateFactoryInput
    {
        public string Name { get; set; }
    }
}