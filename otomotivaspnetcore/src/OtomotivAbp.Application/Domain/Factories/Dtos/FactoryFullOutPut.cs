﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.Departments.Dtos;

namespace OtomotivAbp.Domain.Factories.Dtos
{
    public class FactoryFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }

        public List<DepartmentPartOutPut> Departments { get; set; }

    }
}