﻿using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.StopTypes.Dtos;

namespace OtomotivAbp.Domain.Stops.Dtos
{
    public class StopFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public StopPartOutPut Stop { get; set; }
        public StopTypePartOutPut StopType { get; set; }
    }
}