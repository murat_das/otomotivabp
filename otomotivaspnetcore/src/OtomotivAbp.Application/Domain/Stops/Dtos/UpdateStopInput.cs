﻿using Abp.Application.Services.Dto;
using OtomotivAbp.Domain.StopTypes.Dtos;

namespace OtomotivAbp.Domain.Stops.Dtos
{
    public class UpdateStopInput : EntityDto<int>
    {
        public string Name { get; set; }
        public StopTypePartOutPut StopType { get; set; }
    }
}