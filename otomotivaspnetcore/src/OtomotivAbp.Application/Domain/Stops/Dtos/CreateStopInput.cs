﻿using OtomotivAbp.Domain.StopTypes.Dtos;

namespace OtomotivAbp.Domain.Stops.Dtos
{
    public class CreateStopInput
    {
        public string Name { get; set; }
        public StopTypePartOutPut StopType { get; set; }
    }
}