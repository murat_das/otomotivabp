﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Manager;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.Domain.Stops.Dtos;

namespace OtomotivAbp.Domain.Stops
{
    public class StopAppService : OtomotivAbpAppServiceBase, IStopAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IStopRepository _stopRepository;

        public StopAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IStopRepository stopRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _stopRepository = stopRepository;
        }

        [HttpPost]
        public async Task<StopFullOutPut> CreateAsync(CreateStopInput input)
        {
            var stop = new Stop()
            {
                Name = input.Name,
                StopTypeId = input.StopType.Id
            };

            await _stopRepository.InsertAsync(stop);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<StopFullOutPut>(stop);
        }

        [HttpPost]
        public async Task<StopFullOutPut> GetAsync(GetStopInput input)
        {
            var stop = await _entityManager.GetStopAsync(input.Id);

            return ObjectMapper.Map<StopFullOutPut>(stop);
        }

        [HttpPost]
        public async Task<List<StopFullOutPut>> GetListAsync()
        {
            var stopList = await _stopRepository.GetAllListAsync();

            return ObjectMapper.Map<List<StopFullOutPut>>(stopList);
        }

        [HttpPost]
        public async Task DeleteAsync(DeleteStopInput input)
        {
            var stop = await _entityManager.GetStopAsync(input.Id);

            await _stopRepository.DeleteAsync(stop.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<StopFullOutPut> UpdateAsync(UpdateStopInput input)
        {
            var stop = await _entityManager.GetStopAsync(input.Id);

            stop.Name = input.Name;
            stop.StopTypeId = input.StopType.Id;

            await _stopRepository.UpdateAsync(stop);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<StopFullOutPut>(stop);
        }
    }
}