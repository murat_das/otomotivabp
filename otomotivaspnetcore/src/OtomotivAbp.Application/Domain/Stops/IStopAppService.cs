﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Domain.Stops.Dtos;

namespace OtomotivAbp.Domain.Stops
{
    public interface IStopAppService : IApplicationService
    {
        #region Async Methods
        Task<StopFullOutPut> CreateAsync(CreateStopInput input);
        Task<StopFullOutPut> GetAsync(GetStopInput input);
        Task<List<StopFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteStopInput input);
        Task<StopFullOutPut> UpdateAsync(UpdateStopInput input);
        #endregion Async Methods
    }
}