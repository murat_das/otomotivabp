﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Users.Dto
{
    public class UserPartOutPut:EntityDto<int>
    {
        public string Name { get; set; }
        public string SurName { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
    }
}