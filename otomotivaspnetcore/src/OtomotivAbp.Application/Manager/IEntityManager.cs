﻿using System.Threading.Tasks;
using Abp.Dependency;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Manager
{
    public interface IEntityManager:ITransientDependency
    {
        Task<DailyShift> GetDailyShiftAsync(int dailyShiftId);
        Task<DailyShiftEmployee> GetDailyShiftEmployeeAsync(int dailyShiftEmployeeId);
        Task<DailyShiftWorkOrder> GetDailyShiftWorkOrderAsync(int dailyShiftWorkOrderId);
        Task<DailyShiftWorkOrderScrap> GetDailyShiftWorkOrderScrapAsync(int dailyShiftWorkOrderScrapId);
        Task<DailyShiftWorkOrderStop> GetDailyShiftWorkOrderStopAsync(int dailyShiftWorkOrderStopId);
        Task<Department> GetDepartmentAsync(int departmentId);
        Task<Device> GetDeviceAsync(int deviceId);
        Task<DeviceGroup> GetDeviceGroupAsync(int deviceGroupId);
        Task<DeviceMold> GetDeviceMoldAsync(int deviceMoldId);
        Task<DeviceStop> GetDeviceStopAsync(int deviceStopId);
        Task<Factory> GetFactoryAsync(int factoryId);
        Task<Mold> GetMoldAsync(int moldId);
        Task<Product> GetProductAsync(int productId);
        Task<ProductRawMaterial> GetProductRawMaterialAsync(int productRawMaterialId);
        Task<ProductScrap> GetProductScrapAsync(int productScrapId);
        Task<RawMaterial> GetRawMaterialAsync(int rawMaterialId);
        Task<Scrap> GetScrapAsync(int scrapId);
        Task<ScrapType> GetScrapTypeAsync(int scrapTypeId);
        Task<Shift> GetShiftAsync(int shiftId);
        Task<Stop> GetStopAsync(int stopId);
        Task<StopType> GetStopTypeAsync(int stopTypeId);
        Task<WorkOrder> GetWorkOrderAsync(int workOrderId);
    }
}