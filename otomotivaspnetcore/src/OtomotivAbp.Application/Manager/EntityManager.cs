﻿using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.ObjectMapping;
using Abp.Runtime.Session;
using Castle.Core.Logging;
using OtomotivAbp.Domain.Entities;
using OtomotivAbp.EntityFrameworkCore;
using OtomotivAbp.EntityFrameworkCore.Repositories;
using OtomotivAbp.Services;

namespace OtomotivAbp.Manager
{
    public class EntityManager : AbpAppServiceBase, IEntityManager
    {
        private readonly IDailyShiftRepository _dailyShiftRepository;
        private readonly IDailyShiftEmployeeRepository _dailyShiftEmployeeRepository;
        private readonly IDailyShiftWorkOrderRepository _dailyShiftWorkOrderRepository;
        private readonly IDailyShiftWorkOrderScrapRepository _dailyShiftWorkOrderScrapRepository;
        private readonly IDailyShiftWorkOrderStopRepository _dailyShiftWorkOrderStopRepository;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IDeviceRepository _deviceRepository;
        private readonly IDeviceGroupRepository _deviceGroupRepository;
        private readonly IDeviceMoldRepository _deviceMoldRepository;
        private readonly IDeviceStopRepository _deviceStopRepository;
        private readonly IFactoryRepository _factoryRepository;
        private readonly IMoldRepository _moldRepository;
        private readonly IProductRepository _productRepository;
        private readonly IProductRawMaterialRepository _productRawMaterialRepository;
        private readonly IProductScrapRepository _productScrapRepository;
        private readonly IRawMaterialRepository _rawMaterialRepository;
        private readonly IScrapRepository _scrapRepository;
        private readonly IScrapTypeRepository _scrapTypeRepository;
        private readonly IShiftRepository _shiftRepository;
        private readonly IStopRepository _stopRepository;
        private readonly IStopTypeRepository _stopTypeRepository;
        private readonly IWorkOrderRepository _workOrderRepository;

        public EntityManager(
            IAbpSession abpSession,
            ILocalizationManager localizationManager,
            ILogger logger,
            IUnitOfWorkManager unitOfWorkManager,
            IObjectMapper objectMapper,
            IDailyShiftRepository dailyShiftRepository,
            IDailyShiftEmployeeRepository dailyShiftEmployeeRepository,
            IDailyShiftWorkOrderRepository dailyShiftWorkOrderRepository,
            IDailyShiftWorkOrderScrapRepository dailyShiftWorkOrderScrapRepository,
            IDailyShiftWorkOrderStopRepository dailyShiftWorkOrderStopRepository,
            IDepartmentRepository departmentRepository,
            IDeviceRepository deviceRepository,
            IDeviceGroupRepository deviceGroupRepository,
            IDeviceMoldRepository deviceMoldRepository,
            IDeviceStopRepository deviceStopRepository,
            IFactoryRepository factoryRepository,
            IMoldRepository moldRepository,
            IProductRepository productRepository,
            IProductRawMaterialRepository productRawMaterialRepository,
            IProductScrapRepository productScrapRepository,
            IRawMaterialRepository rawMaterialRepository,
            IScrapRepository scrapRepository,
            IScrapTypeRepository scrapTypeRepository,
            IShiftRepository shiftRepository,
            IStopRepository stopRepository,
            IStopTypeRepository stopTypeRepository,
            IWorkOrderRepository workOrderRepository)
            : base(abpSession, localizationManager, logger, unitOfWorkManager, objectMapper)
        {
            _dailyShiftRepository = dailyShiftRepository;
            _dailyShiftEmployeeRepository = dailyShiftEmployeeRepository;
            _dailyShiftWorkOrderRepository = dailyShiftWorkOrderRepository;
            _dailyShiftWorkOrderScrapRepository = dailyShiftWorkOrderScrapRepository;
            _dailyShiftWorkOrderStopRepository = dailyShiftWorkOrderStopRepository;
            _departmentRepository = departmentRepository;
            _deviceRepository = deviceRepository;
            _deviceGroupRepository = deviceGroupRepository;
            _deviceMoldRepository = deviceMoldRepository;
            _deviceStopRepository = deviceStopRepository;
            _factoryRepository = factoryRepository;
            _moldRepository = moldRepository;
            _productRepository = productRepository;
            _productRawMaterialRepository = productRawMaterialRepository;
            _productScrapRepository = productScrapRepository;
            _rawMaterialRepository = rawMaterialRepository;
            _scrapRepository = scrapRepository;
            _scrapTypeRepository = scrapTypeRepository;
            _shiftRepository = shiftRepository;
            _stopRepository = stopRepository;
            _stopTypeRepository = stopTypeRepository;
            _workOrderRepository = workOrderRepository;
        }

        public async Task<DailyShift> GetDailyShiftAsync(int dailyShiftId)
        {
            var dailyShift = await _dailyShiftRepository.FirstOrDefaultAsync(x =>
                x.Id == dailyShiftId
            );
            if (dailyShift == null)
            {
                throw new EntityNotFoundException(typeof(DailyShift), dailyShiftId);
            }

            return dailyShift;
        }

        public async Task<DailyShiftEmployee> GetDailyShiftEmployeeAsync(int dailyShiftEmployeeId)
        {
            var dailyShiftEmployee = await _dailyShiftEmployeeRepository.FirstOrDefaultAsync(x =>
                x.Id == dailyShiftEmployeeId
            );
            if (dailyShiftEmployee == null)
            {
                throw new EntityNotFoundException(typeof(DailyShiftEmployee), dailyShiftEmployeeId);
            }

            return dailyShiftEmployee;
        }

        public async Task<DailyShiftWorkOrder> GetDailyShiftWorkOrderAsync(int dailyShiftWorkOrderId)
        {
            var dailyShiftWorkOrder = await _dailyShiftWorkOrderRepository.FirstOrDefaultAsync(x =>
                x.Id == dailyShiftWorkOrderId
            );
            if (dailyShiftWorkOrder == null)
            {
                throw new EntityNotFoundException(typeof(DailyShiftWorkOrder), dailyShiftWorkOrderId);
            }

            return dailyShiftWorkOrder;
        }

        public async Task<DailyShiftWorkOrderScrap> GetDailyShiftWorkOrderScrapAsync(int dailyShiftWorkOrderScrapId)
        {
            var dailyShiftWorkOrderScrap = await _dailyShiftWorkOrderScrapRepository.FirstOrDefaultAsync(x =>
                x.Id == dailyShiftWorkOrderScrapId
            );
            if (dailyShiftWorkOrderScrap == null)
            {
                throw new EntityNotFoundException(typeof(DailyShiftWorkOrderScrap), dailyShiftWorkOrderScrapId);
            }

            return dailyShiftWorkOrderScrap;
        }

        public async Task<DailyShiftWorkOrderStop> GetDailyShiftWorkOrderStopAsync(int dailyShiftWorkOrderStopId)
        {
            var dailyShiftWorkOrderStop = await _dailyShiftWorkOrderStopRepository.FirstOrDefaultAsync(x =>
                x.Id == dailyShiftWorkOrderStopId
            );
            if (dailyShiftWorkOrderStop == null)
            {
                throw new EntityNotFoundException(typeof(DailyShiftWorkOrderStop), dailyShiftWorkOrderStopId);
            }

            return dailyShiftWorkOrderStop;
        }

        public async Task<Department> GetDepartmentAsync(int departmentId)
        {
            var department = await _departmentRepository.FirstOrDefaultAsync(x =>
                x.Id == departmentId
            );
            if (department == null)
            {
                throw new EntityNotFoundException(typeof(Department), departmentId);
            }

            return department;
        }

        public async Task<Device> GetDeviceAsync(int deviceId)
        {
            var device = await _deviceRepository.FirstOrDefaultAsync(x =>
                x.Id == deviceId
            );
            if (device == null)
            {
                throw new EntityNotFoundException(typeof(Device), deviceId);
            }

            return device;
        }

        public async Task<DeviceGroup> GetDeviceGroupAsync(int deviceGroupId)
        {
            var deviceGroup = await _deviceGroupRepository.FirstOrDefaultAsync(x =>
                x.Id == deviceGroupId
            );
            if (deviceGroup == null)
            {
                throw new EntityNotFoundException(typeof(DeviceGroup), deviceGroupId);
            }

            return deviceGroup;
        }

        public async Task<DeviceMold> GetDeviceMoldAsync(int deviceMoldId)
        {
            var deviceMold = await _deviceMoldRepository.FirstOrDefaultAsync(x =>
                x.Id == deviceMoldId
            );
            if (deviceMold == null)
            {
                throw new EntityNotFoundException(typeof(DeviceMold), deviceMoldId);
            }

            return deviceMold;
        }

        public async Task<DeviceStop> GetDeviceStopAsync(int deviceStopId)
        {
            var deviceStop = await _deviceStopRepository.FirstOrDefaultAsync(x =>
                x.Id == deviceStopId
            );
            if (deviceStop == null)
            {
                throw new EntityNotFoundException(typeof(DeviceStop), deviceStopId);
            }

            return deviceStop;
        }

        public async Task<Factory> GetFactoryAsync(int factoryId)
        {
            var factory = await _factoryRepository.FirstOrDefaultAsync(x =>
                x.Id == factoryId
            );
            if (factory == null)
            {
                throw new EntityNotFoundException(typeof(Factory), factoryId);
            }

            return factory;
        }

        public async Task<Mold> GetMoldAsync(int moldId)
        {
            var mold = await _moldRepository.FirstOrDefaultAsync(x =>
                x.Id == moldId
            );
            if (mold == null)
            {
                throw new EntityNotFoundException(typeof(Mold), moldId);
            }

            return mold;
        }

        public async Task<Product> GetProductAsync(int productId)
        {
            var product = await _productRepository.FirstOrDefaultAsync(x =>
                x.Id == productId
            );
            if (product == null)
            {
                throw new EntityNotFoundException(typeof(Product), productId);
            }

            return product;
        }

        public async Task<ProductRawMaterial> GetProductRawMaterialAsync(int productRawMaterialId)
        {
            var productRawMaterial = await _productRawMaterialRepository.FirstOrDefaultAsync(x =>
                x.Id == productRawMaterialId
            );
            if (productRawMaterial == null)
            {
                throw new EntityNotFoundException(typeof(ProductRawMaterial), productRawMaterialId);
            }

            return productRawMaterial;
        }

        public async Task<ProductScrap> GetProductScrapAsync(int productScrapId)
        {
            var productScrap = await _productScrapRepository.FirstOrDefaultAsync(x =>
                x.Id == productScrapId
            );
            if (productScrap == null)
            {
                throw new EntityNotFoundException(typeof(ProductScrap), productScrapId);
            }

            return productScrap;
        }

        public async Task<RawMaterial> GetRawMaterialAsync(int rawMaterialId)
        {
            var rawMaterial = await _rawMaterialRepository.FirstOrDefaultAsync(x =>
                x.Id == rawMaterialId
            );
            if (rawMaterial == null)
            {
                throw new EntityNotFoundException(typeof(RawMaterial), rawMaterialId);
            }

            return rawMaterial;
        }

        public async Task<Scrap> GetScrapAsync(int scrapId)
        {
            var scrap = await _scrapRepository.FirstOrDefaultAsync(x =>
                x.Id == scrapId
            );
            if (scrap == null)
            {
                throw new EntityNotFoundException(typeof(Scrap), scrapId);
            }

            return scrap;
        }

        public async Task<ScrapType> GetScrapTypeAsync(int scrapTypeId)
        {
            var scrapType = await _scrapTypeRepository.FirstOrDefaultAsync(x =>
                x.Id == scrapTypeId
            );
            if (scrapType == null)
            {
                throw new EntityNotFoundException(typeof(ScrapType), scrapTypeId);
            }

            return scrapType;
        }

        public async Task<Shift> GetShiftAsync(int shiftId)
        {
            var shift = await _shiftRepository.FirstOrDefaultAsync(x =>
                x.Id == shiftId
            );
            if (shift == null)
            {
                throw new EntityNotFoundException(typeof(Shift), shiftId);
            }

            return shift;
        }

        public async Task<Stop> GetStopAsync(int stopId)
        {
            var stop = await _stopRepository.FirstOrDefaultAsync(x =>
                x.Id == stopId
            );
            if (stop == null)
            {
                throw new EntityNotFoundException(typeof(Stop), stopId);
            }

            return stop;
        }

        public async Task<StopType> GetStopTypeAsync(int stopTypeId)
        {
            var stopType = await _stopTypeRepository.FirstOrDefaultAsync(x =>
                x.Id == stopTypeId
            );
            if (stopType == null)
            {
                throw new EntityNotFoundException(typeof(StopType), stopTypeId);
            }

            return stopType;
        }

        public async Task<WorkOrder> GetWorkOrderAsync(int workOrderId)
        {
            var workOrder = await _workOrderRepository.FirstOrDefaultAsync(x =>
                x.Id == workOrderId
            );
            if (workOrder == null)
            {
                throw new EntityNotFoundException(typeof(WorkOrder), workOrderId);
            }

            return workOrder;
        }
    }
}