﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using OtomotivAbp.Authorization;
using OtomotivAbp.Manager;

namespace OtomotivAbp
{
    [DependsOn(
        typeof(OtomotivAbpCoreModule),
        typeof(AbpAutoMapperModule))]
    public class OtomotivAbpApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<OtomotivAbpAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(OtomotivAbpApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(cfg =>
            {
                MapperManager.DtosToDomain(cfg);
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg.AddMaps(thisAssembly);
            });
        }
    }
}
