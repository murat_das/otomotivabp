﻿using Abp.Application.Services;
using OtomotivAbp.MultiTenancy.Dto;

namespace OtomotivAbp.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

