﻿using System.Threading.Tasks;
using Abp.Application.Services;
using OtomotivAbp.Authorization.Accounts.Dto;

namespace OtomotivAbp.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
