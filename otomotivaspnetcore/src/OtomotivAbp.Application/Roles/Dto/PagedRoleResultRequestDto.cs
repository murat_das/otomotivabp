﻿using Abp.Application.Services.Dto;

namespace OtomotivAbp.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

