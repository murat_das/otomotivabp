using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace OtomotivAbp.Controllers
{
    public abstract class OtomotivAbpControllerBase: AbpController
    {
        protected OtomotivAbpControllerBase()
        {
            LocalizationSourceName = OtomotivAbpConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
