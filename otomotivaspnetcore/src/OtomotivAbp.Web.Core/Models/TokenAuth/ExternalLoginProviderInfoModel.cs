﻿using Abp.AutoMapper;
using OtomotivAbp.Authentication.External;

namespace OtomotivAbp.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
