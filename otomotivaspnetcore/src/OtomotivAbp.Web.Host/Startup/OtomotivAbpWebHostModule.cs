﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using OtomotivAbp.Configuration;

namespace OtomotivAbp.Web.Host.Startup
{
    [DependsOn(
       typeof(OtomotivAbpWebCoreModule))]
    public class OtomotivAbpWebHostModule: AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public OtomotivAbpWebHostModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(OtomotivAbpWebHostModule).GetAssembly());
        }
    }
}
