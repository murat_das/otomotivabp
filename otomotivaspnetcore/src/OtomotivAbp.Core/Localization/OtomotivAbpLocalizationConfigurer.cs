﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace OtomotivAbp.Localization
{
    public static class OtomotivAbpLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(OtomotivAbpConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(OtomotivAbpLocalizationConfigurer).GetAssembly(),
                        "OtomotivAbp.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
