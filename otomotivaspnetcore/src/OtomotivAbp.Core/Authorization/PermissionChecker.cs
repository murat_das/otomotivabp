﻿using Abp.Authorization;
using OtomotivAbp.Authorization.Roles;
using OtomotivAbp.Authorization.Users;

namespace OtomotivAbp.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
