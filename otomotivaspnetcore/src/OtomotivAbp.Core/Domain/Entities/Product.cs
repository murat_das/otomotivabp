﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using OtomotivAbp.MultiTenancy;

namespace OtomotivAbp.Domain.Entities
{
    public class Product : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public Product()
        {
            ProductRawMaterials=new HashSet<ProductRawMaterial>();
            ProductScraps=new HashSet<ProductScrap>();
            WorkOrders = new HashSet<WorkOrder>();
            Molds=new HashSet<Mold>();
        }
        #endregion Constructor

        #region Properties

        public string Name { get; set; }
        public string Barcode { get; set; }
        public string StockCode { get; set; }

        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }

      
        #endregion One To One Relations

        #region One To Many Relations

        public virtual ICollection<ProductRawMaterial> ProductRawMaterials { get; set; }
        public virtual ICollection<ProductScrap> ProductScraps { get; set; }
        public virtual ICollection<WorkOrder> WorkOrders { get; set; }
        public virtual ICollection<Mold> Molds { get; set; }


        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}