﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using OtomotivAbp.Authorization.Users;
using OtomotivAbp.MultiTenancy;

namespace OtomotivAbp.Domain.Entities
{
    public class DailyShiftEmployee : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public DailyShiftEmployee()
        {

        }
        #endregion Constructor

        #region Properties

        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public int DailyShiftId { get; set; }
        public virtual DailyShift DailyShift { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }

        #endregion One To One Relations

        #region One To Many Relations

        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}