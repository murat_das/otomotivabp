﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using OtomotivAbp.MultiTenancy;

namespace OtomotivAbp.Domain.Entities
{
    public class DailyShiftWorkOrder : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public DailyShiftWorkOrder()
        {
            DailyShiftWorkOrderStops=new HashSet<DailyShiftWorkOrderStop>();
            DailyShiftWorkOrderScraps=new HashSet<DailyShiftWorkOrderScrap>();
        }
        #endregion Constructor

        #region Properties

        public decimal Amount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }


        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public int DailyShiftId { get; set; }
        public virtual DailyShift DailyShift { get; set; }
        public int WorkOrderId { get; set; }
        public virtual WorkOrder WorkOrder { get; set; }

        #endregion One To One Relations

        #region One To Many Relations

        public virtual ICollection<DailyShiftWorkOrderStop> DailyShiftWorkOrderStops { get; set; }
        public virtual ICollection<DailyShiftWorkOrderScrap> DailyShiftWorkOrderScraps { get; set; }


        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}