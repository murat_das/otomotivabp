﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using OtomotivAbp.MultiTenancy;

namespace OtomotivAbp.Domain.Entities
{
    public class Department : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public Department()
        {
            DeviceGroups = new HashSet<DeviceGroup>();
        }
        #endregion Constructor

        #region Properties

        public string Name { get; set; }

        #endregion Properties   

        #region Relations
        #region One To One Relations

        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public int FactoryId { get; set; }
        public virtual Factory Factory { get; set; }

        //public int FactoryId { get; set; }
        //public virtual Factory Factory { get; set; }

        #endregion One To One Relations

        #region One To Many Relations

        public virtual ICollection<DeviceGroup> DeviceGroups { get; set; }

        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}