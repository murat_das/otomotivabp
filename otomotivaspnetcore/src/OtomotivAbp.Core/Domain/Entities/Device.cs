﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using OtomotivAbp.MultiTenancy;

namespace OtomotivAbp.Domain.Entities
{
    public class Device : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public Device()
        {
            DeviceMolds=new HashSet<DeviceMold>();
            DeviceStops=new HashSet<DeviceStop>();
            DailyShifts=new HashSet<DailyShift>();
            WorkOrders=new HashSet<WorkOrder>();
        }
        #endregion Constructor

        #region Properties

        public string Name { get; set; }
        public string No { get; set; }
        public bool Status { get; set; }
        public string IpAddress { get; set; }

        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public int DeviceGroupId { get; set; }
        public virtual DeviceGroup DeviceGroup { get; set; }

        #endregion One To One Relations

        #region One To Many Relations

        public virtual ICollection<DeviceMold> DeviceMolds { get; set; }
        public virtual ICollection<DeviceStop> DeviceStops { get; set; }
        public virtual ICollection<DailyShift> DailyShifts { get; set; }
        public virtual ICollection<WorkOrder> WorkOrders { get; set; }

        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}