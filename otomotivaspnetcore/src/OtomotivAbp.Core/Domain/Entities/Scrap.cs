﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using OtomotivAbp.MultiTenancy;

namespace OtomotivAbp.Domain.Entities
{
    public class Scrap : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public Scrap()
        {
            DailyShiftWorkOrderScraps=new HashSet<DailyShiftWorkOrderScrap>();
            ProductScraps=new HashSet<ProductScrap>();
        }
        #endregion Constructor

        #region Properties

        public string Name { get; set; }

        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public int ScrapTypeId { get; set; }
        public virtual ScrapType ScrapType{ get; set; }
        #endregion One To One Relations

        #region One To Many Relations

        public virtual ICollection<DailyShiftWorkOrderScrap>  DailyShiftWorkOrderScraps { get; set; }
        public virtual ICollection<ProductScrap>  ProductScraps { get; set; }

        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}