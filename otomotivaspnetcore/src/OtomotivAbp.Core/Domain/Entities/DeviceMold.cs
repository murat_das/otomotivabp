﻿using System;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using OtomotivAbp.MultiTenancy;

namespace OtomotivAbp.Domain.Entities
{
    public class DeviceMold : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public DeviceMold()
        {

        }
        #endregion Constructor

        #region Properties

        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public int MoldId { get; set; }
        public virtual Mold Mold { get; set; }
        public int DeviceId { get; set; }
        public virtual Device Device { get; set; }

        #endregion One To One Relations

        #region One To Many Relations

        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}