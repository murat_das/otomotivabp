﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using OtomotivAbp.MultiTenancy;

namespace OtomotivAbp.Domain.Entities
{
    public class DailyShift : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public DailyShift()
        {
            DailyShiftEmployees = new HashSet<DailyShiftEmployee>();
            DailyShiftWorkOrders=new List<DailyShiftWorkOrder>();
        }
        #endregion Constructor

        #region Properties

        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public int ShiftId { get; set; }
        public virtual Shift Shift { get; set; }
        public int DeviceId { get; set; }
        public virtual Device Device { get; set; }


        #endregion One To One Relations

        #region One To Many Relations

        public virtual ICollection<DailyShiftEmployee> DailyShiftEmployees { get; set; }
        public virtual ICollection<DailyShiftWorkOrder> DailyShiftWorkOrders { get; set; }


        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}