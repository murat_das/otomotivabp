﻿using System;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using OtomotivAbp.MultiTenancy;

namespace OtomotivAbp.Domain.Entities
{
    public class ProductRawMaterial : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public ProductRawMaterial()
        {

        }
        #endregion Constructor

        #region Properties

        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int RawMaterialId { get; set; }
        public virtual RawMaterial RawMaterial { get; set; }

        #endregion One To One Relations

        #region One To Many Relations

        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}