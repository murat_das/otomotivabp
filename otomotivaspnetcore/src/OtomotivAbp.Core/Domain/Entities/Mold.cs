﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using OtomotivAbp.MultiTenancy;

namespace OtomotivAbp.Domain.Entities
{
    public class Mold : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public Mold()
        {
            DeviceMolds=new HashSet<DeviceMold>();
            WorkOrders=new HashSet<WorkOrder>();
        }
        #endregion Constructor

        #region Properties

        public string Name { get; set; }
        public string Barcode { get; set; }
        public int Cavity { get; set; } // kalıp göz sayısı

        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        #endregion One To One Relations

        #region One To Many Relations

        public virtual ICollection<DeviceMold> DeviceMolds { get; set; }
        public virtual ICollection<WorkOrder> WorkOrders { get; set; }


        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}