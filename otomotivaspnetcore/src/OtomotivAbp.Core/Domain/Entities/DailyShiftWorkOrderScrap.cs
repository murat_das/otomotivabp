﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using OtomotivAbp.MultiTenancy;

namespace OtomotivAbp.Domain.Entities
{
    public class DailyShiftWorkOrderScrap : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public DailyShiftWorkOrderScrap()
        {

        }
        #endregion Constructor

        #region Properties

        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public int DailyShiftWorkOrderId { get; set; }
        public virtual DailyShiftWorkOrder DailyShiftWorkOrder { get; set; }
        public int ScrapId { get; set; }
        public virtual Scrap Scrap { get; set; }
        #endregion One To One Relations

        #region One To Many Relations

        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}