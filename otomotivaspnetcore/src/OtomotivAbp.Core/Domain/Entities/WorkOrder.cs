﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using OtomotivAbp.MultiTenancy;

namespace OtomotivAbp.Domain.Entities
{
    public class WorkOrder : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public WorkOrder()
        {
            DailyShiftWorkOrders = new HashSet<DailyShiftWorkOrder>();
        }
        #endregion Constructor

        #region Properties

        public string No { get; set; }
        public int Status { get; set; } // iş emri başlamamış, devam ediyor, bitmiş
        public decimal TargetAmount { get; set; }
        public int Type { get; set; } // iş emri erp, manuel, otomatik

        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }

        public int DeviceId { get; set; }
        public virtual Device Device { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int MoldId { get; set; }
        public virtual Mold Mold { get; set; }
        #endregion One To One Relations

        #region One To Many Relations

        public virtual ICollection<DailyShiftWorkOrder> DailyShiftWorkOrders { get; set; }

        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}