﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using OtomotivAbp.MultiTenancy;

namespace OtomotivAbp.Domain.Entities
{
    public class DailyShiftWorkOrderStop : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public DailyShiftWorkOrderStop()
        {

        }
        #endregion Constructor

        #region Properties

        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public int DailyShiftWorkOrderId { get; set; }
        public virtual DailyShiftWorkOrder DailyShiftWorkOrder { get; set; }
        public int StopId { get; set; }
        public virtual Stop Stop { get; set; }

        #endregion One To One Relations

        #region One To Many Relations

        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}