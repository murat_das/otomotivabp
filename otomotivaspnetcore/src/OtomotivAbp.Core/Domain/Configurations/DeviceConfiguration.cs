﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class DeviceConfiguration : IEntityTypeConfiguration<Device>
    {
        public void Configure(EntityTypeBuilder<Device> builder)
        {
            #region Properties

            builder.ToTable("Device");

            builder.HasKey(product => product.Id);

            builder.Property(product => product.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);
            builder.Property(product => product.IpAddress)
                .HasColumnName("IpAddress")
                .HasMaxLength(150);
            builder.Property(product => product.No)
                .HasColumnName("No")
                .HasMaxLength(150);
            builder.Property(product => product.Status)
                .HasColumnName("Status");

            
            #endregion Properties

            #region Relations
            builder.HasMany<DeviceMold>(device => device.DeviceMolds)
                .WithOne(deviceMold => deviceMold.Device)
                .HasForeignKey(deviceMold => deviceMold.DeviceId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<DeviceStop>(device => device.DeviceStops)
                .WithOne(deviceStop => deviceStop.Device)
                .HasForeignKey(deviceStop => deviceStop.DeviceId)
                .OnDelete(DeleteBehavior.ClientSetNull);
          
            builder.HasMany<DailyShift>(device => device.DailyShifts)
                .WithOne(dailyShift => dailyShift.Device)
                .HasForeignKey(dailyShift => dailyShift.DeviceId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<WorkOrder>(device => device.WorkOrders)
                .WithOne(workOrder => workOrder.Device)
                .HasForeignKey(workOrder => workOrder.DeviceId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}