﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class DailyShiftWorkOrderConfiguration : IEntityTypeConfiguration<DailyShiftWorkOrder>
    {
        public void Configure(EntityTypeBuilder<DailyShiftWorkOrder> builder)
        {
            #region Properties

            builder.ToTable("DailyShiftWorkOrder");

            builder.HasKey(product => product.Id);

            builder.Property(product => product.StartDate)
                .HasColumnName("StartDate");

            builder.Property(product => product.EndDate)
                .HasColumnName("EndDate");

            #endregion Properties

            #region Relations

            builder.HasMany<DailyShiftWorkOrderStop>(
                    dailyShiftWorkOrder => dailyShiftWorkOrder.DailyShiftWorkOrderStops)
                .WithOne(dailyShiftWorkOrderStop => dailyShiftWorkOrderStop.DailyShiftWorkOrder)
                .HasForeignKey(dailyShiftWorkOrderStop => dailyShiftWorkOrderStop.DailyShiftWorkOrderId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<DailyShiftWorkOrderScrap>(dailyShiftWorkOrder => dailyShiftWorkOrder.DailyShiftWorkOrderScraps)
                .WithOne(dailyShiftWorkOrderScrap => dailyShiftWorkOrderScrap.DailyShiftWorkOrder)
                .HasForeignKey(dailyShiftWorkOrderScrap => dailyShiftWorkOrderScrap.DailyShiftWorkOrderId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}