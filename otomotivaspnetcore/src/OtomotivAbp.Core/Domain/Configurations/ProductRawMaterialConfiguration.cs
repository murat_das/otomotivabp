﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class ProductRawMaterialConfiguration : IEntityTypeConfiguration<ProductRawMaterial>
    {
        public void Configure(EntityTypeBuilder<ProductRawMaterial> builder)
        {
            #region Properties

            builder.ToTable("ProductRawMaterial");

            builder.HasKey(product => product.Id);

            #endregion Properties

            #region Relations

            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}