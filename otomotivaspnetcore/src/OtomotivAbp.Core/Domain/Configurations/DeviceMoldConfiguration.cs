﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class DeviceMoldConfiguration : IEntityTypeConfiguration<DeviceMold>
    {
        public void Configure(EntityTypeBuilder<DeviceMold> builder)
        {
            #region Properties

            builder.ToTable("DeviceMold");

            builder.HasKey(product => product.Id);

            #endregion Properties

            #region Relations

            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}