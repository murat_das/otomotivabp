﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class DailyShiftWorkOrderStopConfiguration : IEntityTypeConfiguration<DailyShiftWorkOrderStop>
    {
        public void Configure(EntityTypeBuilder<DailyShiftWorkOrderStop> builder)
        {
            #region Properties

            builder.ToTable("DailyShiftWorkOrderStop");

            builder.HasKey(product => product.Id);

            #endregion Properties

            #region Relations
          
            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}