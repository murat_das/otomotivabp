﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class MoldConfiguration : IEntityTypeConfiguration<Mold>
    {
        public void Configure(EntityTypeBuilder<Mold> builder)
        {
            #region Properties

            builder.ToTable("Mold");

            builder.HasKey(product => product.Id);

            builder.Property(product => product.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);

            builder.Property(product => product.Barcode)
                .HasColumnName("Barcode")
                .HasMaxLength(150);

            builder.Property(product => product.Cavity)
                .HasColumnName("Cavity");


            #endregion Properties

            #region Relations
            builder.HasMany<DeviceMold>(mold => mold.DeviceMolds)
                .WithOne(deviceMold => deviceMold.Mold)
                .HasForeignKey(deviceMold => deviceMold.MoldId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<WorkOrder>(mold => mold.WorkOrders)
                .WithOne(mold => mold.Mold)
                .HasForeignKey(workOrder => workOrder.MoldId)
                .OnDelete(DeleteBehavior.ClientSetNull);

          

            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}