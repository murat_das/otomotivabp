﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class ShiftConfiguration : IEntityTypeConfiguration<Shift>
    {
        public void Configure(EntityTypeBuilder<Shift> builder)
        {
            #region Properties

            builder.ToTable("Shift");

            builder.HasKey(product => product.Id);

            builder.Property(product => product.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);
            builder.Property(product => product.StartTime)
                .HasColumnName("StartTime");
            builder.Property(product => product.EndTime)
                .HasColumnName("EndTime");


            #endregion Properties

            #region Relations
            builder.HasMany<DailyShift>(shift => shift.DailyShifts)
                .WithOne(dailyShift => dailyShift.Shift)
                .HasForeignKey(dailyShift => dailyShift.ShiftId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}