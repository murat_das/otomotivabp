﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class DailyShiftConfiguration : IEntityTypeConfiguration<DailyShift>
    {
        public void Configure(EntityTypeBuilder<DailyShift> builder)
        {
            #region Properties

            builder.ToTable("DailyShift");

            builder.HasKey(product => product.Id);


            #endregion Properties

            #region Relations
            builder.HasMany<DailyShiftEmployee>(dailyShift => dailyShift.DailyShiftEmployees)
                .WithOne(dailyShiftEmployee => dailyShiftEmployee.DailyShift)
                .HasForeignKey(dailyShiftEmployee => dailyShiftEmployee.DailyShiftId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<DailyShiftWorkOrder>(dailyShift => dailyShift.DailyShiftWorkOrders)
                .WithOne(dailyShiftWorkOrder => dailyShiftWorkOrder.DailyShift)
                .HasForeignKey(dailyShiftWorkOrder => dailyShiftWorkOrder.DailyShiftId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}