﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class DeviceGroupConfiguration : IEntityTypeConfiguration<DeviceGroup>
    {
        public void Configure(EntityTypeBuilder<DeviceGroup> builder)
        {
            #region Properties

            builder.ToTable("DeviceGroup");

            builder.HasKey(product => product.Id);

            builder.Property(product => product.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);
            builder.Property(product => product.IpAddress)
                .HasColumnName("IpAddress")
                .HasMaxLength(150);


            #endregion Properties

            #region Relations
            builder.HasMany<Device>(deviceGroup => deviceGroup.Devices)
                .WithOne(device => device.DeviceGroup)
                .HasForeignKey(device => device.DeviceGroupId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}