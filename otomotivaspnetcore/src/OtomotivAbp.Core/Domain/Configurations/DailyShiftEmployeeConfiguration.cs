﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class DailyShiftEmployeeConfiguration : IEntityTypeConfiguration<DailyShiftEmployee>
    {
        public void Configure(EntityTypeBuilder<DailyShiftEmployee> builder)
        {
            #region Properties

            builder.ToTable("DailyShiftEmployee");

            builder.HasKey(product => product.Id);

            #endregion Properties

            #region Relations

            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}