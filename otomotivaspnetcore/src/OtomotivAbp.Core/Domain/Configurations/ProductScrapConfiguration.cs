﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class ProductScrapConfiguration : IEntityTypeConfiguration<ProductScrap>
    {
        public void Configure(EntityTypeBuilder<ProductScrap> builder)
        {
            #region Properties

            builder.ToTable("ProductScrap");

            builder.HasKey(product => product.Id);

            #endregion Properties

            #region Relations

            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}