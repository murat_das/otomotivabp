﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class RawMaterialConfiguration : IEntityTypeConfiguration<RawMaterial>
    {
        public void Configure(EntityTypeBuilder<RawMaterial> builder)
        {
            #region Properties

            builder.ToTable("RawMaterial");

            builder.HasKey(product => product.Id);

            builder.Property(product => product.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);
            builder.Property(product => product.Barcode)
                .HasColumnName("Barcode")
                .HasMaxLength(150);


            #endregion Properties

            #region Relations
            builder.HasMany<ProductRawMaterial>(product => product.ProductRawMaterials)
                .WithOne(productRawMaterial => productRawMaterial.RawMaterial)
                .HasForeignKey(productRawMaterial => productRawMaterial.RawMaterialId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}