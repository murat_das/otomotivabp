﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class ScrapConfiguration : IEntityTypeConfiguration<Scrap>
    {
        public void Configure(EntityTypeBuilder<Scrap> builder)
        {
            #region Properties

            builder.ToTable("Scrap");

            builder.HasKey(product => product.Id);
            builder.Property(product => product.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);

            #endregion Properties

            #region Relations

            builder.HasMany<DailyShiftWorkOrderScrap>(scrap => scrap.DailyShiftWorkOrderScraps)
                .WithOne(dailyShiftWorkOrderScrap => dailyShiftWorkOrderScrap.Scrap)
                .HasForeignKey(dailyShiftWorkOrderScrap => dailyShiftWorkOrderScrap.ScrapId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<ProductScrap>(scrap => scrap.ProductScraps)
                .WithOne(productScrap => productScrap.Scrap)
                .HasForeignKey(productScrap => productScrap.ScrapId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}