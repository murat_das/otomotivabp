﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class DeviceStopConfiguration : IEntityTypeConfiguration<DeviceStop>
    {
        public void Configure(EntityTypeBuilder<DeviceStop> builder)
        {
            #region Properties

            builder.ToTable("DeviceStop");

            builder.HasKey(product => product.Id);

            #endregion Properties

            #region Relations

            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}