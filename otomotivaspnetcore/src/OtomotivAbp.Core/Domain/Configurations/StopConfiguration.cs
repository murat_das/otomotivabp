﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class StopConfiguration : IEntityTypeConfiguration<Stop>
    {
        public void Configure(EntityTypeBuilder<Stop> builder)
        {
            #region Properties

            builder.ToTable("Stop");

            builder.HasKey(product => product.Id);

            builder.Property(product => product.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);

            #endregion Properties

            #region Relations
            builder.HasMany<DeviceStop>(stop => stop.DeviceStops)
                .WithOne(deviceStop => deviceStop.Stop)
                .HasForeignKey(deviceStop => deviceStop.StopId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<DailyShiftWorkOrderStop>(stop => stop.DailyShiftWorkOrderStops)
                .WithOne(dailyShiftWorkOrderStop => dailyShiftWorkOrderStop.Stop)
                .HasForeignKey(dailyShiftWorkOrderStop => dailyShiftWorkOrderStop.StopId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}