﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            #region Properties

            builder.ToTable("Product");

            builder.HasKey(product => product.Id);

            builder.Property(product => product.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);
            builder.Property(product => product.Barcode)
                .HasColumnName("Barcode")
                .HasMaxLength(150);
            builder.Property(product => product.StockCode)
                .HasColumnName("StockCode")
                .HasMaxLength(150);
            #endregion Properties

            #region Relations
            builder.HasMany<ProductRawMaterial>(product => product.ProductRawMaterials)
                .WithOne(productRawMaterial => productRawMaterial.Product)
                .HasForeignKey(productRawMaterial => productRawMaterial.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<ProductScrap>(product => product.ProductScraps)
                .WithOne(productScrap => productScrap.Product)
                .HasForeignKey(productScrap => productScrap.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<WorkOrder>(product => product.WorkOrders)
                .WithOne(workOrder => workOrder.Product)
                .HasForeignKey(workOrder => workOrder.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull);


            builder.HasMany<Mold>(product => product.Molds)
                .WithOne(mold => mold.Product)
                .HasForeignKey(mold => mold.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}