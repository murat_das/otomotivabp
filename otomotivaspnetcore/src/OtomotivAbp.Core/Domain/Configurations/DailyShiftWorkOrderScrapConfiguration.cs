﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class DailyShiftWorkOrderScrapConfiguration : IEntityTypeConfiguration<DailyShiftWorkOrderScrap>
    {
        public void Configure(EntityTypeBuilder<DailyShiftWorkOrderScrap> builder)
        {
            #region Properties

            builder.ToTable("DailyShiftWorkOrderScrap");

            builder.HasKey(product => product.Id);

            #endregion Properties

            #region Relations
          
            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}