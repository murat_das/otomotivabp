﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class StopTypeConfiguration : IEntityTypeConfiguration<StopType>
    {
        public void Configure(EntityTypeBuilder<StopType> builder)
        {
            #region Properties

            builder.ToTable("StopType");

            builder.HasKey(product => product.Id);

            builder.Property(product => product.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);

            #endregion Properties

            #region Relations
            builder.HasMany<Stop>(stopType => stopType.Stops)
                .WithOne(stop => stop.StopType)
                .HasForeignKey(stop => stop.StopTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}