﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class ScrapTypeConfiguration : IEntityTypeConfiguration<ScrapType>
    {
        public void Configure(EntityTypeBuilder<ScrapType> builder)
        {
            #region Properties

            builder.ToTable("ScrapType");

            builder.HasKey(product => product.Id);
           
            builder.Property(product => product.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);

            #endregion Properties

            #region Relations
            builder.HasMany<Scrap>(scrapType => scrapType.Scraps)
                .WithOne(scrap => scrap.ScrapType)
                .HasForeignKey(scrap => scrap.ScrapTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull);

         

            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}