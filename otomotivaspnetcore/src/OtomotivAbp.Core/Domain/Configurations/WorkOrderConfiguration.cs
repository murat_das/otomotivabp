﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.Domain.Configurations
{
    public class WorkOrderConfiguration : IEntityTypeConfiguration<WorkOrder>
    {
        public void Configure(EntityTypeBuilder<WorkOrder> builder)
        {
            #region Properties

            builder.ToTable("WorkOrder");

            builder.HasKey(product => product.Id);

            builder.Property(product => product.No)
                .HasColumnName("No")
                .HasMaxLength(150);

            builder.Property(product => product.Status)
                .HasColumnName("Status");

            builder.Property(product => product.TargetAmount)
                .HasColumnName("TargetAmount");

            builder.Property(product => product.Type)
                .HasColumnName("Type");

            #endregion Properties

            #region Relations
            builder.HasMany<DailyShiftWorkOrder>(workOrder => workOrder.DailyShiftWorkOrders)
                .WithOne(dailyShiftWorkOrder => dailyShiftWorkOrder.WorkOrder)
                .HasForeignKey(dailyShiftWorkOrder => dailyShiftWorkOrder.WorkOrderId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            
            #endregion Relations

            #region OptimisticLockField
            builder.Property(product => product.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}