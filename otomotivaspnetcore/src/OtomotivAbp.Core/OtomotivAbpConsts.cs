﻿namespace OtomotivAbp
{
    public class OtomotivAbpConsts
    {
        public const string LocalizationSourceName = "OtomotivAbp";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
