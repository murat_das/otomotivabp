﻿using System.Collections.Generic;
using Abp.MultiTenancy;
using OtomotivAbp.Authorization.Users;
using OtomotivAbp.Domain.Entities;

namespace OtomotivAbp.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {
            DailyShifts = new HashSet<DailyShift>();
            DailyShiftEmployees = new HashSet<DailyShiftEmployee>();
            DailyShiftWorkOrders = new HashSet<DailyShiftWorkOrder>();
            DailyShiftWorkOrderScraps = new HashSet<DailyShiftWorkOrderScrap>();
            DailyShiftWorkOrderStops = new HashSet<DailyShiftWorkOrderStop>();
            Departments = new HashSet<Department>();
            Devices = new HashSet<Device>();
            DeviceGroups = new HashSet<DeviceGroup>();
            DeviceMolds = new HashSet<DeviceMold>();
            DeviceStops = new HashSet<DeviceStop>();
            Factories = new HashSet<Factory>();
            Molds = new HashSet<Mold>();
            Products = new HashSet<Product>();
            ProductRawMaterials = new HashSet<ProductRawMaterial>();
            ProductScraps = new HashSet<ProductScrap>();
            RawMaterials = new HashSet<RawMaterial>();
            Scraps = new HashSet<Scrap>();
            ScrapTypes = new HashSet<ScrapType>();
            Shifts = new HashSet<Shift>();
            Stops = new HashSet<Stop>();
            StopTypes = new HashSet<StopType>();
            WorkOrders = new HashSet<WorkOrder>();
        }

        public virtual ICollection<DailyShift> DailyShifts { get; set; }
        public virtual ICollection<DailyShiftEmployee> DailyShiftEmployees { get; set; }
        public virtual ICollection<DailyShiftWorkOrder> DailyShiftWorkOrders { get; set; }
        public virtual ICollection<DailyShiftWorkOrderScrap> DailyShiftWorkOrderScraps { get; set; }
        public virtual ICollection<DailyShiftWorkOrderStop> DailyShiftWorkOrderStops { get; set; }
        public virtual ICollection<Department> Departments { get; set; }
        public virtual ICollection<Device> Devices { get; set; }
        public virtual ICollection<DeviceGroup> DeviceGroups { get; set; }
        public virtual ICollection<DeviceMold> DeviceMolds { get; set; }
        public virtual ICollection<DeviceStop> DeviceStops { get; set; }
        public virtual ICollection<Factory> Factories { get; set; }
        public virtual ICollection<Mold> Molds { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<ProductRawMaterial> ProductRawMaterials { get; set; }
        public virtual ICollection<ProductScrap> ProductScraps { get; set; }
        public virtual ICollection<RawMaterial> RawMaterials { get; set; }
        public virtual ICollection<Scrap> Scraps { get; set; }
        public virtual ICollection<ScrapType> ScrapTypes { get; set; }
        public virtual ICollection<Shift> Shifts { get; set; }
        public virtual ICollection<Stop> Stops { get; set; }
        public virtual ICollection<StopType> StopTypes { get; set; }
        public virtual ICollection<WorkOrder> WorkOrders { get; set; }


        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
